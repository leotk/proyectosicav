﻿//FUNCION PARA TRANSFORMAR EL DIA
function convertDate(inputFormat) {
    if (inputFormat == '' || inputFormat == null) {
        return '';
    } else {
       var d = new Date(inputFormat)
        function pad(s) { return (s < 10) ? '0' + s : s; }
        return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/')
    }
}

//FUNCION PARA VALIDAR EL INPUT TYPE FILE Y EL VALOR BOOLEAN OCULTO PARA REGISTRAR/ACT
function ValidadorFormulario(form) {
    var file = form.querySelectorAll('input[type=file]')[0].value.length;
    var check = form.elements["chek"];
    var validador = form.validador.value;
    var rpta = true;
    var msag = "";

    if (check) {
        if (check.checked == true) {
            if (file != 0 && validador == "false") {
                msag = "El archivo debe ser validado";
                rpta = false;
            }
            else if (file == 0 && validador == "false") {
                msag = "No puedes remplazar un archivo existente con uno vacio";
                rpta = false;
            }
        }
        if (rpta) { msag = 'Quieres actualizar el registro ?'; }
    } else {
        if (file != 0 && validador == "false") {
            msag = "El archivo cargado debe ser validado";
            rpta = false;
        }
        else if (file == 0 && validador == "false") {
            msag = "No se puede guardar un registro con un archivo vacio";
            rpta = false;
        }
        //if (rpta) { msag = 'Deseas registar esta entrada ?'; }
    }

    return {
        rpta: rpta,
        msag: msag
    };
}

//FUNCION PARA VALIDAR EL CHEQUEO DE LOS CHECK EN CADA FORM EDITAR FILE
function ChangeCheck(element) { 
    var form = $(element).closest("form")[0];

    var check = form.elements["chek"];
    var file = form.querySelectorAll('input[type=file]')[0];
    var namefile = form.elements["namefile"];

    if (check.checked == true) {
        file.style.display = "block";
        namefile.style.display = "none";
    } else {
        file.style.display = "none";
        namefile.style.display = "block";
    }

}

function IsNull(data) { //FUNCION PARA VALIDAR SI EL DATO A LLAMAR ESTA VACIO O NO
    var value;
    data != null ? value = data : value = '';
    return value;
}

function IsNullM(data) { //FUNCION PARA VALIDAR SI EL DATO A LLAMAR ESTA VACIO O NO
    var value;
    data != null ? value = (data).toFixed(2) : value = (0).toFixed(2);
    return value;
}

function CancelarFile() {
    $(".lida").addClass("active");
    $("#NavDato").addClass("active");
    $("#NombreEstado").val('Cancelado');

    $("#Actsoli :input").prop("disabled", true);
    $("#NuevoPasajero :input").prop("disabled", true);
    $("#NuevaCotiza :input").prop("disabled", true);
    $("#NuevaDreserva :input").prop("disabled", true);
    $("#NuevaLCliente :input").prop("disabled", true);
    $("#NuevaLProveedor :input").prop("disabled", true);
    $("#NuevaFac :input").prop("disabled", true);

    $("#tbl-detalle-pasajero a").css({ cursor: "not-allowed" }).click(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    });
    $("#tbl-detalle-coti a").css({ cursor: "not-allowed" }).click(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    });
    $("#tbl-detalle-reser a").css({ cursor: "not-allowed" }).click(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    });
    $("#tbl-detalle-licli a").css({ cursor: "not-allowed" }).click(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    });
    $("#tbl-detalle-lipro a").css({ cursor: "not-allowed" }).click(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    });
    $("#tbl-detalle-fact a").css({ cursor: "not-allowed" }).click(function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
    });
}

function PosicionamientoPestania(estatus) {

    switch (parseInt(estatus)) {
        case 0:

            $("ul.flex-wrap li").addClass("disabled");
            
            $(".lida").removeClass("disabled").addClass("active");
            $("#NavDato").addClass("active");
            $("#NombreEstado").val('Fase Solicitud');

            $("#NuevoPasajero :input").prop("disabled", true);
            $("#NuevaCotiza :input").prop("disabled", true);
            $("#NuevaDreserva :input").prop("disabled", true);
            $("#NuevaLCliente :input").prop("disabled", true);
            $("#NuevaLProveedor :input").prop("disabled", true);
            $("#NuevaFac :input").prop("disabled", true);

            //OTRA FORMA DE HACERLO
            //$("#btns-file a").css({ cursor: "not-allowed" }).click(function (e) {
            //    e.preventDefault();
            //    e.stopImmediatePropagation();
            //});

            break;
        case 1:
            $(".lida").addClass("active");
            $("#NavDato").addClass("active");
            $("#NombreEstado").val('Fase Solicitud');

            $("#NuevoPasajero :input").prop("disabled", true);
            $("#NuevaCotiza :input").prop("disabled", true);
            $("#NuevaDreserva :input").prop("disabled", true);
            $("#NuevaLCliente :input").prop("disabled", true);
            $("#NuevaLProveedor :input").prop("disabled", true);
            $("#NuevaFac :input").prop("disabled", true);

            $("#tbl-detalle-pasajero a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-coti a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-reser a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-licli a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-lipro a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-fact a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });

            break;
        case 2:
            $(".lipa").addClass("active");
            $("#NavPasa").addClass("active");
            $("#NombreEstado").val('Fase Pasajeros');

            $("#Actsoli :input").prop("disabled", true);
            $("#NuevaCotiza :input").prop("disabled", true);
            $("#NuevaDreserva :input").prop("disabled", true);
            $("#NuevaLCliente :input").prop("disabled", true);
            $("#NuevaLProveedor :input").prop("disabled", true);
            $("#NuevaFac :input").prop("disabled", true);

            $("#tbl-detalle-coti a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-reser a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-licli a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-lipro a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-fact a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });

            break;
        case 3:
            $(".lico").addClass("active");
            $("#NavCoti").addClass("active");
            $("#NombreEstado").val('Fase Cotización');

            $("#Actsoli :input").prop("disabled", true);
            $("#NuevoPasajero :input").prop("disabled", true);
            $("#NuevaDreserva :input").prop("disabled", true);
            $("#NuevaLCliente :input").prop("disabled", true);
            $("#NuevaLProveedor :input").prop("disabled", true);
            $("#NuevaFac :input").prop("disabled", true);

            $("#tbl-detalle-pasajero a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-reser a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-licli a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-lipro a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-fact a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });

            break;
        case 4:
            $(".lire").addClass("active");
            $("#NavRese").addClass("active");
            $("#NombreEstado").val('Fase Reservas');

            $("#Actsoli :input").prop("disabled", true);
            $("#NuevoPasajero :input").prop("disabled", true);
            $("#NuevaCotiza :input").prop("disabled", true);
            $("#NuevaLCliente :input").prop("disabled", true);
            $("#NuevaLProveedor :input").prop("disabled", true);
            $("#NuevaFac :input").prop("disabled", true);

            $("#tbl-detalle-pasajero a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-coti a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-licli a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-lipro a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-fact a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });

            break;
        case 5:
            $(".licl").addClass("active");
            $("#NavLiCli").addClass("active");
            $("#NombreEstado").val('Fase Liquidación cliente');

            $("#Actsoli :input").prop("disabled", true);
            $("#NuevoPasajero :input").prop("disabled", true);
            $("#NuevaCotiza :input").prop("disabled", true);
            $("#NuevaDreserva :input").prop("disabled", true);
            $("#NuevaLProveedor :input").prop("disabled", true);
            $("#NuevaFac :input").prop("disabled", true);

            $("#tbl-detalle-pasajero a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-coti a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-reser a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-lipro a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-fact a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });

            break;
        case 6:
            $(".lipr").addClass("active");
            $("#NavLiPro").addClass("active");
            $("#NombreEstado").val('Fase Liquidación proveedor');

            $("#Actsoli :input").prop("disabled", true);
            $("#NuevoPasajero :input").prop("disabled", true);
            $("#NuevaCotiza :input").prop("disabled", true);
            $("#NuevaDreserva :input").prop("disabled", true);
            $("#NuevaLCliente :input").prop("disabled", true);
            $("#NuevaFac :input").prop("disabled", true);

            $("#tbl-detalle-pasajero a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-coti a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-reser a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-licli a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-fact a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });

            break;
        case 7:
            $(".lifa").addClass("active");
            $("#NavFac").addClass("active");
            $("#NombreEstado").val('Fase Facturación');

            $("#Actsoli :input").prop("disabled", true);
            $("#NuevoPasajero :input").prop("disabled", true);
            $("#NuevaCotiza :input").prop("disabled", true);
            $("#NuevaDreserva :input").prop("disabled", true);
            $("#NuevaLCliente :input").prop("disabled", true);
            $("#NuevaLProveedor :input").prop("disabled", true);

            $("#tbl-detalle-pasajero a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-coti a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-reser a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-licli a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });
            $("#tbl-detalle-lipro a").css({ cursor: "not-allowed" }).click(function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            });

            break;
        default:
            console.log('No se ejecuto nah')
    }

}