using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using ProyectoWsicav.Controllers;
using ProyectoWsicav.Demo;
using ProyectoWsicav.Models;

namespace ProyectoWsicav
{
    public class Startup
    {
      
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddHangfire(x => x.UseSqlServerStorage("Server=localhost\\sql2012;Database=dbwsicavprueba;User Id=sa;Password=Ws1c4v321"));
            //services.AddHangfire(x => x.UseSqlServerStorage("Data Source=LCONDORI;Initial Catalog=dbwsicavprueba;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"));
            //services.AddHangfire(x => x.UseSqlServerStorage("Server=localhost,1440;Database=dbwsicavprueba;User Id=sa;Password=proY3ct0#Camara"));
            services.AddHangfire(x => x.UseSqlServerStorage("Data Source=DESKTOP-9V5T2DM;Initial Catalog=dbwsicavprueba;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"));

            services.AddHangfireServer();
           // services.AddScoped<IDemoService, DemoService>();

            //services.AddControllers;
            //var adminBO =sp.GetService<IDemoService>();
            //RecurringJob.AddOrUpdate("ConfirmReminder", () => adminBO.RunTask(), Cron.Minutely);      // every sunday at 05:00
                                                      
           // services.AddScoped<CorreoCampanaController,>
            //services.AddControllersWithViews();
            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            //IMPLEMENTACION SESION
            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession();
            //**
            services.AddDbContext<UsersContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("UsersContext")));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseHangfireServer();
            app.UseHangfireDashboard();
            DemoService obj = new DemoService();
            RecurringJob.AddOrUpdate(() =>obj.RunTask(), Cron.Minutely);
   



            //CAMBIAR EL DECIMAL CON "," POR "." 
            var cultureInfo = new CultureInfo("en-US");
            var dateformat = new DateTimeFormatInfo
            {
                ShortDatePattern = "dd/MM/yyyy",
                LongDatePattern = "dd/MM/yyyy hh:mm:ss tt"
            };
            cultureInfo.DateTimeFormat = dateformat;
            cultureInfo.NumberFormat.NumberGroupSeparator = ".";

            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
            //**

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/lib")),
                RequestPath = new PathString("/lib")
            });

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/images")),
                RequestPath = new PathString("/images")
            });

            app.UseRouting();

            app.UseAuthorization();

            //implementacion SESION
            app.UseSession();
            //**

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Acceso}/{action=Login}/{id?}");
            });
           // Rotativa.AspNetCore.RotativaConfiguration.Setup(env.WebRootPath.ToString(),"Rotativa");
        }
    }
}
