﻿using Microsoft.AspNetCore.Http;
using ProyectoWsicav.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.ViewObject
{
    public class VFactura
    {
        public VFactura()
        {
        }

        public VFactura(TblFactura fac) //Para Editar
        {
            Id = fac.Id;
            SolicitudId = fac.SolicitudId;
            Fecha = fac.FechaFactu;
            NumeFactu = fac.NumeFactu;
            Monto = fac.Monto;
            Documentonombre = fac.Documentonombre;
        }

        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [DataType(DataType.Date),Required(ErrorMessage = "La fecha es requerida")]
        public DateTime? Fecha { get; set; }
        [Required(ErrorMessage = "N° de factura es requerido")]
        public string NumeFactu { get; set; }
        [Required(ErrorMessage = "Monto requerido"), RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Monto invalido")]
        public decimal? Monto { get; set; }
        public string Documentonumero { get; set; }
        public string Documentonombre { get; set; }
        public IFormFile DocFact { get; set; }
    }
}
