﻿using Microsoft.AspNetCore.Http;
using ProyectoWsicav.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.ViewObject
{
    public class VCotizacion
    {
        public VCotizacion()
        {
        }
        public VCotizacion(TblCotizacion coti) //Empleado Para editar
        {
            Id = coti.Id;
            SolicitudId = coti.SolicitudId;
            Fecha = coti.FechaCotizacion;
            NumeCotizacion = coti.NumeCotizacion;
            Monto = coti.Monto;
            Documentonombre = coti.Documentonombre;
        }
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [Required(ErrorMessage = "La fecha es requerida")]
        [DataType(DataType.Date)]
        public DateTime? Fecha { get; set; }
        [Required(ErrorMessage = "N° de cotización es requerido")]
        public string NumeCotizacion { get; set; }
        [Required(ErrorMessage = "Monto requerido"), RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Monto invalido")]
        public decimal? Monto { get; set; }
        public string Documentonumero { get; set; }
        public string Documentonombre { get; set; }
        public IFormFile DocCoti { get; set; }
    }
}
