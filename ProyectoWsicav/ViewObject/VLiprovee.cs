﻿using Microsoft.AspNetCore.Http;
using ProyectoWsicav.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.ViewObject
{
    public class VLiprovee
    {
        public VLiprovee()
        {
        }
        public VLiprovee(TblLiquiProve lp) //Empleado para editar actualizar
        {
            Id = lp.Id;
            SolicitudId = lp.SolicitudId;
            ProveedorId = lp.ProveedorId;
            FechaLprove = lp.FechaLprove;
            FormaPago = lp.FormaPago;
            Banco = lp.Banco;
            Titular = lp.Titular;
            NumeVaucher = lp.NumeVaucher;
            NumeTarjeta = lp.NumeTarjeta;
            PagoCliente = lp.PagoCliente;
            Monto = lp.Monto;
            Documentonombre = lp.Documentonombre;
        }

        public int Id { get; set; }
        public int SolicitudId { get; set; }
        public int? ProveedorId { get; set; }
        [DataType(DataType.Date), Required(ErrorMessage = "Fecha requerida")]
        public DateTime? FechaLprove { get; set; }
        public string FormaPago { get; set; }
        public string Banco { get; set; }
        [Required(ErrorMessage = "Titular requerido")]
        public string Titular { get; set; }
        [Required(ErrorMessage = "Voucher requerido")]
        public string NumeVaucher { get; set; }
        [Required(ErrorMessage = "N° tarjeta requerido")]
        public string NumeTarjeta { get; set; }
        public bool PagoCliente { get; set; }
        [Required(ErrorMessage = "Monto requerido")]
        [RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Monto invalido")]
        public decimal? Monto { get; set; }
        public string Documentonombre { get; set; }
        public IFormFile Doc { get; set; }
    }
}
