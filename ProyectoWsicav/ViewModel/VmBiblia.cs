﻿using System;
using System.Collections.Generic;

namespace ProyectoWsicav.ViewModel
{
    public partial class VmBiblia
    {
        public int Id { get; set; }
        public string NombresPasajeros { get; set; }
        public string Files { get; set; }
        public int nPasajeros { get; set; }
        public string idioma { get; set; }
        public string Agencia { get; set; }
        public string hora { get; set; }
        public string Servicios { get; set; }
        public string TipoServicio { get; set; }
        public string Hotel { get; set; }
        public int GuiaId { get; set; }
        public int VehiculoId { get; set; }
        public string Observaciones { get; set; }
        public DateTime FechaIn { get; set; }
        public string Guia { get; set; }
        public string Movilidad { get; set; }
        public int ReservaDetalleId { get; set; }




    }
}
