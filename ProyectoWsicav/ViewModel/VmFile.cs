﻿using ProyectoWsicav.Models;
using ProyectoWsicav.ViewObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.ViewModel
{
    public class VmFile
    {
        public TblSolicitud solicitud { get; set; }
        public TblPasajeros pasajero { get; set; }
        public VCotizacion coti { get; set; }
        public TblReservasDetalle Reser { get; set; }
        public VLicliente Lcliente { get; set; }
        public VLiprovee Lprove { get; set; }
        public VFactura Fact { get; set; }
    }
}
