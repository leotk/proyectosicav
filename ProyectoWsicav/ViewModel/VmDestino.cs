﻿using System;
using System.Collections.Generic;

namespace ProyectoWsicav.ViewModel
{
    public class VmDestino
    {


        public int Id { get; set; }
        public int CiudadId { get; set; }
        public string Nombre { get; set; }

        public string NombreCiudad { get; set; }

    }
}
