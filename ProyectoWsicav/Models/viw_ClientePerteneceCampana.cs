﻿using System;
using System.Collections.Generic;

namespace ProyectoWsicav.Models
{
    public partial class viw_ClientePerteneceCampana
    {
        public int Id { get; set; }
        public int ClienteID { get; set; }
        public int CampanaID { get; set; }
        public string nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        //public int ClienteProvieneID { get; set; }
       

    }
}
