﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Vendedor")]
    public partial class TblVendedor
    {
        public TblVendedor()
        {
            TblSolicitud = new HashSet<TblSolicitud>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(100)]
        public string Nombres { get; set; }

        [InverseProperty("Vendedor")]
        public virtual ICollection<TblSolicitud> TblSolicitud { get; set; }
    }
}
