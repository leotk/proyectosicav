﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.Models
{
    public class Metodos
    {
        private static string[] AdmitedExtens = { ".pdf", ".docx", ".doc", ".xlsx", ".png", ".jpg"};

        public static TblCiudad ObtenerDatos(String codigo) {

            TblCiudad ciudad = null;
            ciudad.NombreCiudad = "Pedro";
            return ciudad;
        }

        public static List<TblCiudad> Listaciudades() {

            List<TblCiudad> lista = new List<TblCiudad>();

            return lista;
        }

        private static Boolean ValidarNuevaSolicitud(TblSolicitud soli) {
            bool rpta=false;

            if (soli.Solicitadode is null || soli.Idioma is null || soli.Mediollegada is null || soli.DestinoId is null || soli.FechaIn is null || soli.FechaOut is null /*|| soli.Cntadulto is null || soli.Cntnino is null || soli.Cntinfa is null*/ )
            {
                rpta = true;
            }

            return rpta;
        }

        public static void VistasInicio(String tipo, TblSolicitud so, out String Vistahtml,out String Boton)
        {
            Boton = "";
            Vistahtml = "";
            if (so.Cancelaestado is true)
            {

                Boton += "<div class=\"btn-group\"><a href=\"/Solicitudes/CancelarRecuperara/" + so.Id + "\" class=\"btn btn-success\"><i class=\"fa fa-plus-circle\"></i>&nbsp;Recuperar</a></div>";
                Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";
            } 
            else if (so.Cancelaestado is false)
            {
                if (!(ValidarNuevaSolicitud(so)))
                {
                    Boton += "<div class=\"btn-group\"><a href=\"/Solicitudes/VerDetalle/" + so.Id + "\" class=\"btn btn-warning\"><i class=\"fa fa-chevron-left\"></i>&nbsp;Volver</a></div>";
                    Boton += "<div class=\"btn-group\"><a href=\"/Solicitudes/CamEstado/" + so.Id + "\" class=\"btn btn-success\"><i class=\"fa fa-refresh\"></i>&nbsp;Cambiar estado</a></div>";
                    Boton += "<div class=\"btn-group\"><a href=\"/Solicitudes/CancelarRecuperara/" + so.Id + "\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i>&nbsp;Cancelar</a></div>";
                    if (tipo.Equals("D"))
                    {
                        Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";

                    }
                    else if (tipo.Equals("P"))
                    {
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";
                    }
                    else if (tipo.Equals("Cti"))
                    {
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";
                    }
                    else if (tipo.Equals("R"))
                    {
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                        Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";
                    }
                    else if (tipo.Equals("LC"))
                    {
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                        Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";
                    }
                    else if (tipo.Equals("LP"))
                    {
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";
                    }
                    else if (tipo.Equals("Fc"))
                    {
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu1\" class=\"a a2\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu3\" class=\"a a2\">Cotización</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu2\" class=\"a a2\">Reservas</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu4\" class=\"a a2\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"\"><a data-toggle=\"tab\" href=\"#menu5\" class=\"a a2\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#menu6\" class=\"a a2\">Facturación</a></li>";
                    }
                    else
                    {
                        Vistahtml += "<li class=\"disabled\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                        Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Pasajeros</a></li>";
                        Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Cotización</a></li>";
                        Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Reservas</a></li>";
                        Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Liquidación Cliente</a></li>";
                        Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Liquidación Proveedor</a></li>";
                        Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Facturación</a></li>";
                    }
                }
                else
                {
                    Vistahtml += "<li class=\"active\"><a data-toggle=\"tab\" href=\"#home\" class=\"a a2\">Datos</a></li>";
                    Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Pasajeros</a></li>";
                    Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Cotización</a></li>";
                    Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Reservas</a></li>";
                    Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Liquidación Cliente</a></li>";
                    Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Liquidación Proveedor</a></li>";
                    Vistahtml += "<li class=\"disabled\"><a disabled class=\"a a2 disabled\">Facturación</a></li>";
                }
            }

            //return Vistahtml;
        }

        public static String VistaPasajero(ref String dato, List<TblPasajeros> detalle)
        {
            dato += "<div class=\"col-md-12\">";
            dato += "<div class=\"form-row\">";
            dato += "<div class=\"form-group col-md-4\">";
            dato += "<label for=\"inputEmail4\">Nombres y Apellidos</label>";
            dato += "<input disabled class=\"form-control\" placeholder=\"Digite los nombres y apellidos\" type=\"text\" data-val=\"true\" data-val-length=\"The field NombresPasajeros must be a string with a maximum length of 50.\" data-val-length-max=\"50\" id=\"Item2_NombresPasajeros\" maxlength=\"50\" name=\"Item2.NombresPasajeros\" value=\"\">";
            dato += "</div>";
            dato += "<div class=\"form-group col-md-2\">";
            dato += "<label>Tipo de Pasajero</label>";
            dato += "<select disabled class=\"form-control\" id=\"Item2_TipoPasajeroId\" name=\"Item2.TipoPasajeroId\">";
            dato += "<option value=\"1\">Infante</option>";
            dato += "</select>";
            dato += "</div>";
            dato += "<div class=\"form-group col-md-3\"> <label>Documento de identidad</label> <select disabled class=\"form-control\" id=\"Item2_TipoDocumentoIdentidadId\" name=\"Item2.TipoDocumentoIdentidadId\"> <option value=\"1\">DNI</option> </select> </div>";
            dato += "<div class=\"form-group col-md-2\"> <label>Nacionalidad</label> <select disabled class=\"form-control\" data-val=\"true\" data-val-length=\"The field Nacionalidad must be a string with a maximum length of 50.\" data-val-length-max=\"50\" id=\"Item2_Nacionalidad\" name=\"Item2.Nacionalidad\"> <option value=\"EE.UU\" selected=\"\">EE.UU</option> </select> </div>";
            dato += "<div class=\"form-group col-md-2\"> <label>Numero de Documento</label> <input disabled type=\"text\" class=\"form-control\" data-val=\"true\" data-val-length=\"The field NumeroDucumento must be a string with a maximum length of 50.\" data-val-length-max=\"50\" id=\"Item2_NumeroDucumento\" maxlength=\"50\" name=\"Item2.NumeroDucumento\" value=\"\"> </div>";
            dato += "<div class=\"form-group col-md-2\"> <label>Edad</label> <input disabled type=\"text\" class=\"form-control\" placeholder=\"Edad\" id=\"Item2_Edad\" name=\"Item2.Edad\" value=\"\"> </div>";
            dato += "<div class=\"form-group col-md-2\"> <label>Pasajero Principal</label> <div class=\"checkbox\"> <label><input disabled type=\"checkbox\" data-val=\"true\" data-val-required=\"The Principal field is required.\" id=\"Item2_Principal\" name=\"Item2.Principal\" value=\"true\">Principal</label> </div> </div>";
            dato += "</div> <div class=\"form-group col-md-2\"> <label for=\"inputAddress\"></label> <br> <button class=\"btn btn-primary disabled\"> <i class=\"fa fa-save\"></i> Registrar </button> </div></div>";
            dato += "<section class=\"content\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"box box-success\"> <div class=\"box-header\"> <h3 class=\"box-title\">Listado</h3> </div> <div class=\"box-body\"> <table id=\"tbl-pasajero\" class=\"table table-bordered table-striped\"> <thead> <tr> <th>Nombre y Apellidos</th> <th>Edad</th> <th>Nacionalidad</th> <th>Tipo Pasajero</th> <th>Documento de identidad</th> <th>Numero Documento</th> <th>Principal</th> <th>Acciones</th> </tr> </thead> <tbody>";
            foreach (TblPasajeros p in detalle)
            {
                dato += "<tr>";
                dato += "<td>" + p.NombresPasajeros + "</td>";
                dato += "<td>"+p.Edad+"</td>";
                dato += "<td>"+p.Nacionalidad+"</td>";
                dato += "<td>"+p.TipoPasajero.Descripcion+"</td>";
                dato += "<td>"+p.TipoDocumentoIdentidad.Descripcion+"</td>";
                dato += "<td>"+p.NumeroDucumento+"</td>";
                dato += "<td>";
                if (p.Principal == true)
                {
                    dato += "<small class=\"label bg-blue\">Principal</small>";
                }
                dato += "</td>";
                dato += "<td> <div class=\"btn-group\"> <button class=\"btn btn-block btn-warning btn-xs disabled\"><i class=\"fa fa-pencil\"></i></button> </div> <div class=\"btn-group\"> <button class=\"btn btn-block btn-danger btn-xs disabled\"><i class=\"fa fa-ban\"></i></button> </div></td></tr>";
            }
            dato += "</tbody> </table> </div> </div> </div> </div> </section>";
            return dato;
        }

        public static String VistaCotizacion(ref String dato, List<TblCotizacion> detalle)
        {
            dato += "<div class=\"col-md-12\"> <div class=\"form-row\"> <div class=\"form-group col-md-3\"> <label>Fecha Cotización</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div> <input disabled type=\"text\" class=\"form-control pull-right datepicker\" id=\"Item4_Fecha\" name=\"Item4.Fecha\" value=\"\"> </div> </div> <div class=\"form-group col-md-3\"> <label>Numero Cotización</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item4_NumeCotizacion\" name=\"Item4.NumeCotizacion\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label>Monto Total</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-usd\"></i> </div> <input disabled class=\"form-control pull-right\" type=\"text\" data-val=\"true\" data-val-number=\"The field Monto must be a number.\" id=\"Item4_Monto\" name=\"Item4.Monto\" value=\"\"> </div> </div> </div> <div class=\"form-row\"> <div class=\"form-group col-md-4\"> <label>Archivo de Cotización</label> <input disabled onchange=\"ChangeCoti()\" type=\"file\" id=\"Item4_DocCoti\" name=\"Item4.DocCoti\"> </div> <div class=\"form-group col-md-2\"> <br> <button disabled type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i>Registrar</button> </div> <div class=\"form-group col-md-2\"> <br> <a disabled class=\"btn btn-info\"><i class=\"fa fa-arrow-circle-o-up\"></i> Ir a cotizador</a> </div> </div></div>";
            dato += "<section class=\"content\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"box box-success\"> <div class=\"box-header\"> <h3 class=\"box-title\" style=\"font-weight: bold;\">Listado de Cotizaciones</h3> </div> <div class=\"box-body\"> <table class=\"table table-bordered table-striped tablas\"> <thead> <tr> <th>Fecha Cotización</th> <th>Numero Cotización</th> <th>Monto Total</th> <th>Archivo</th> <th>Acciones</th> </tr> </thead> <tbody>";

            foreach (TblCotizacion p in detalle)
            {
                dato += "<tr>";
                dato += "<td>" + String.Format("{0:dd/MM/yyyy}", p.FechaCotizacion) + "</td>";
                dato += "<td>" + p.NumeCotizacion + "</td>";
                dato += "<td>" + p.Monto + "</td>";
                dato += "<td>" + p.Documentonombre + "</td>";
                dato += "<td>";
                dato += "<div class=\"btn-group\"> <a class=\"btn btn-block btn-success btn-xs disabled\"><i class=\"fa fa-download disabled\"></i></a></div><div class=\"btn-group\"> <a class=\"btn btn-block btn-info disabled btn-xs disabled\"><i class=\"fa fa-envelope\"></i></a></div><div class=\"btn-group\"> <a class=\"btn btn-block btn-success btn-xs disabled\"><i class=\"fa fa-whatsapp\"></i></a></div><div class=\"btn-group\"> <a class=\"btn btn-block btn-warning btn-xs disabled\"><i class=\"fa fa-pencil\"></i></a></div><div class=\"btn-group\"> <a class=\"btn btn-block btn-danger btn-xs disabled\"><i class=\"fa fa-ban\"></i></a></div>";
                dato += "</td>";
                dato += "</tr>";
            }
            dato += "</tbody> </table> </div> </div> </div> </div>";

            return dato;
        }

        public static String VistaReserva(ref String dato, List<TblReservasDetalle> detalle, decimal suma)
        {
            dato += "<div class=\"col-md-12\"> <div class=\"form-row\"> <div class=\"form-group col-md-3\"> <label for=\"inputEmail4\">Tipo de reserva</label> <select disabled class=\"form-control\" data-val=\"true\" data-val-required=\"The TipoServicioId field is required.\" id=\"Item3_TipoServicioId\" name=\"Item3.TipoServicioId\"> <option value=\"1\">Servicio uno</option></select> </div> <div class=\"form-group col-md-3\"> <label for=\"inputPassword4\">Fecha Reservacion</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div> <input disabled type=\"text\" class=\"form-control pull-right datepicker\" id=\"Item3_FechaIn\" name=\"Item3.FechaIn\" value=\"\"> </div> </div> <div class=\"form-group col-md-2\"> <label for=\"inputPassword4\">Localizador</label> <input disabled class=\"form-control\" type=\"text\" data-val=\"true\" data-val-length=\"The field Localizador must be a string with a maximum length of 50.\" data-val-length-max=\"50\" id=\"Item3_Localizador\" maxlength=\"50\" name=\"Item3.Localizador\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label for=\"inputPassword4\">Fecha Limite</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div> <input disabled type=\"text\" class=\"form-control pull-right datepicker\" id=\"Item3_FechaLimite\" name=\"Item3.FechaLimite\" value=\"\"> </div> </div> <div class=\"form-group col-md-3\"> <label for=\"inputEmail4\">Proveedor</label> <select disabled class=\"form-control\" data-val=\"true\" data-val-required=\"The ProveedorId field is required.\" id=\"Item3_ProveedorId\" name=\"Item3.ProveedorId\"> <option value=\"1\">Prove01</option></select> </div> <div class=\"form-group col-md-3\"> <label>Operador</label> <select disabled class=\"form-control\"> <option >Wsicav Tours</option> </select> </div> <div class=\"form-group col-md-2\"> <label>Servicio Extra</label> <div class=\"checkbox\"> <label><input disabled type=\"checkbox\" data-val=\"true\" data-val-required=\"The Adicional field is required.\" id=\"Item3_Adicional\" name=\"Item3.Adicional\" value=\"true\">Extra</label> </div> </div> <div class=\"form-group col-md-2\"> <label for=\"inputPassword4\">Importe</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-usd\"></i> </div> <input disabled class=\"form-control pull-right\" type=\"text\" data-val=\"true\" data-val-number=\"The field Monto must be a number.\" id=\"Item3_Monto\" name=\"Item3.Monto\" value=\"\"> </div> </div> </div> <div class=\"form-group col-md-2\"> <label for=\"inputAddress\"></label> <br> <button type=\"submit\" class=\"btn btn-primary disabled\"><i class=\"fa fa-save\"></i>Registrar</button> </div></div>";
            dato += "<section class=\"content\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"box box-success\"> <div class=\"box-header\"> <h3 class=\"box-title\">Listado</h3> </div> <div class=\"box-body\"> <table id=\"tbl-detalle-reser\" class=\"table table-bordered table-striped\"> <thead> <tr> <th>Tipo Reserva</th> <th>Localizador</th> <th>Fecha de Reservación</th> <th>Fecha Limite</th> <th>Proveedor</th> <th>Operador</th> <th>Servicio Extra</th> <th>Importe</th> <th>Acciones</th> </tr> </thead> <tbody>";
            foreach (TblReservasDetalle p in detalle)
            {
                dato += "<tr>";
                dato += "<td>" + p.TipoServicio.Descripcion + "</td>";
                dato += "<td>" + p.Localizador + "</td>";
                dato += "<td>" + String.Format("{0:dd/MM/yyyy}", p.FechaIn) + "</td>";
                dato += "<td>" + String.Format("{0:dd/MM/yyyy}", p.FechaLimite) + "</td>";
                dato += "<td>" + p.Proveedor.Nombre + "</td>";
                dato += "<td>Wsicav Tours</td>";
                dato += "<td>";
                if (p.Adicional == true)
                {
                    dato += "<small class=\"label bg-red\">Extra</small>";
                }
                dato += "</td>";
                dato += "<td>" + p.Monto + "</td>";
                dato += "<td> <div class=\"btn-group\"> <button class=\"btn btn-block btn-warning btn-xs disabled\"><i class=\"fa fa-pencil\"></i></button> </div> <div class=\"btn-group\"> <button class=\"btn btn-block btn-danger btn-xs disabled\"><i class=\"fa fa-ban\"></i></button> </div></td>";
                dato += "</tr>";
            }
            dato += "</tbody> <tfoot> <tr> <th colspan=\"7\"> <label class=\"pull-right\">Total :</label> </th>";
            dato += "<td>" + suma + "</td>";
            dato += "</tr> </tfoot> </table> </div> </div> </div> </div> </section>";
            return dato;
        }

        public static String VistaLiCliente(ref String dato, List<TblLiquiCliente> detalle, decimal suma)
        {
            dato += "<div class=\"col-md-12\"> <div class=\"col-md-12\"> <div class=\"form-group col-md-3\"> <label>Fecha de Liquidación</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div> <input disabled type=\"text\" class=\"form-control pull-right datepicker\" id=\"Item5_FechaLcliente\" name=\"Item5.FechaLcliente\" value=\"\"> </div> </div> <div class=\"form-group col-md-3\"> <label>N° Voucher</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item5_NumeVouch\" name=\"Item5.NumeVouch\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label>Forma de Pago</label> <select disabled class=\"form-control\" id=\"Item5_FormaPago\" name=\"Item5.FormaPago\"> <option disabled value=\"Tarjeta de Credito\">Tarjeta de Credito</option> </select> </div> <div class=\"form-group col-md-2\"> <label>Banco</label> <select disabled class=\"form-control\" id=\"Item5_Banco\" name=\"Item5.Banco\"> <option disabled value=\"BCP\">BCP</option> </select> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group col-md-3\"> <label>Titular</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item5_Titular\" name=\"Item5.Titular\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label>Numero de Tarjeta</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item5_NumeTarjeta\" name=\"Item5.NumeTarjeta\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label>Importe</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-usd\"></i> </div> <input disabled class=\"form-control pull-right\" type=\"text\" data-val=\"true\" id=\"Item5_Monto\" name=\"Item5.Monto\" value=\"\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group col-md-3\"> <label>Archivo de Cotización</label> <input disabled type=\"file\" id=\"Item5_Doc\" name=\"Item5.Doc\"> </div> <div class=\"form-group col-md-2\"> <br> <button disabled type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i>Registrar</button> </div> </div></div>";
            dato += "<section class=\"content\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"box box-success\"> <div class=\"box-header\"> <h3 class=\"box-title\">Listado</h3> </div> <div class=\"box-body\"> <table id=\"tbl-detalle-reser\" class=\"table table-bordered table-striped\"> <thead> <tr> <th>Fecha Liquidacion</th> <th>Forma de Pago</th> <th>Banco</th> <th>Titular</th> <th>Número de Tarjeta</th> <th>N° Voucher</th> <th>Archivo </th> <th>Importe</th> <th>Acciones</th> </tr> </thead><tbody>";
            
            foreach (TblLiquiCliente p in detalle)
            {
                dato += "<tr>";
                dato += "<td>" + String.Format("{0:dd/MM/yyyy}", p.FechaLcliente) + "</td>";
                dato += "<td>" + p.FormaPago + "</td>";
                dato += "<td>" + p.Banco + "</td>";
                dato += "<td>" + p.Titular + "</td>";
                dato += "<td>" + p.NumeTarjeta + "</td>";
                dato += "<td>" + p.NumeVouch + "</td>";
                dato += "<td>" + p.Documentonombre + "</td>";
                dato += "<td>" + p.Monto + "</td>";
                dato += "<td> <div class=\"btn-group\"><a class=\"disabled btn btn-block btn-success btn-xs\" ><i class=\"fa fa-download\"></i></a></div> <div class=\"btn-group\"> <button disabled class=\"btn btn-block btn-warning btn-xs\"><i class=\"fa fa-pencil\"></i></a>";
                dato += "</div><div class=\"btn-group\"><button disabled class=\"btn btn-block btn-danger btn-xs\"><i class=\"fa fa-ban\"></i></a>";
                dato += "</div></td></tr>";
            }
            dato += "</tbody><tfoot><tr><th colspan=\"7\"><label class=\"pull-right\">Total :</label></th><td>" + suma + "</td></tr></tfoot></table>";
            dato += "</div></div></div></div></section>";
            return dato;
        }

        public static String VistaLiProveedor(ref String dato, List<TblLiquiProve> detalle, decimal suma)
        {
            dato += "<div class=\"col-md-12\"> <div class=\"col-md-12\"> <div class=\"form-group col-md-3\"> <label>Proveedor</label> <select disabled class=\"form-control\" id=\"Item6_ProveedorId\" name=\"Item6.ProveedorId\"> <option disabled value=\"1\">Vaneza chacaliaza Remu</option> </select> </div> <div class=\"form-group col-md-2\"> <label>Pago por Cliente</label> <div class=\"checkbox\"> <label><input disabled type=\"checkbox\" data-val=\"true\" value=\"true\">Pago</label> </div> </div> <div class=\"form-group col-md-2\"> <label>Fecha de Liquidación</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div> <input disabled type=\"text\" class=\"form-control pull-right datepicker\" value=\"\"> </div> </div> <div class=\"form-group col-md-3\"> <label>Forma de Pago</label> <select disabled class=\"form-control\" id=\"Item6_FormaPago\" name=\"Item6.FormaPago\"> <option disabled value=\"Tarjeta de Credito\">Tarjeta de Credito</option> </select> </div> <div class=\"form-group col-md-2\"> <label>Banco</label> <select disabled class=\"form-control\" id=\"Item6_Banco\" name=\"Item6.Banco\"> <option disabled value=\"BCP\">BCP</option> </select> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group col-md-3\"> <label>Titular</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item6_Titular\" name=\"Item6.Titular\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label>N° de Tarjeta</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item6_NumeTarjeta\" name=\"Item6.NumeTarjeta\" value=\"\"> </div> <div class=\"form-group col-md-2\"> <label>N° Voucher</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item6_NumeVaucher\" name=\"Item6.NumeVaucher\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label>Importe</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-usd\"></i> </div> <input disabled class=\"form-control pull-right\" type=\"text\" data-val=\"true\" data-val-number=\"The field Monto must be a number.\" id=\"Item6_Monto\" name=\"Item6.Monto\" value=\"\"> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"form-group col-md-3\"> <label>Archivo de Cotización</label> <input disabled type=\"file\" id=\"Item6_Doc\" name=\"Item6.Doc\"> </div> <div class=\"form-group col-md-4\"> <br> <button disabled type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i>Registrar</button> </div> </div></div>";
            dato += "<section class=\"content\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"box box-success\"> <div class=\"box-header\"> <h3 class=\"box-title\">Listado</h3> </div> <div class=\"box-body\"> <table id=\"tbl-detalle-reser\" class=\"table table-bordered table-striped\"> <thead> <tr> <th>Fecha Liquidacion</th> <th>Proveedor</th> <th>Forma de Pago</th> <th>Banco</th> <th>Titular</th> <th>Número de Tarjeta</th> <th>N° voucher</th> <th>Archivo </th> <th>Pago por Cliente</th> <th>Importe</th> <th>Acciones</th> </tr> </thead> <tbody>";
            foreach (TblLiquiProve p in detalle)
            {
                dato += "<tr>";
                dato += "<td>" + String.Format("{0:dd/MM/yyyy}", p.FechaLprove) + "</td>";
                dato += "<td>" + p.Proveedor.Nombre + "</td>";
                dato += "<td>" + p.FormaPago + "</td>";
                dato += "<td>" + p.Banco + "</td>";
                dato += "<td>" + p.Titular + "</td>";
                dato += "<td>" + p.NumeTarjeta + "</td>";
                dato += "<td>" + p.NumeVaucher + "</td>";
                dato += "<td>" + p.Documentonombre + "</td>";
                dato += "<td>";
                if (p.PagoCliente == true)
                {
                    dato += "<small class=\"label bg-red\">Comisión</small>";
                }
                dato += "</td>";
                dato += "<td>" + p.Monto + "</td>";
                dato += "<td> <div class=\"btn-group\"> <a disabled class=\"btn btn-block btn-success btn-xs disabled\"><i class=\"fa fa-download\"></i></a> </div> <div class=\"btn-group\"> <a disabled class=\"btn btn-block btn-warning disabled btn-xs\"><i class=\"fa fa-pencil\"></i></a> </div> <div class=\"btn-group\"> <a disabled class=\"btn btn-block btn-danger btn-xs disabled\"><i class=\"fa fa-ban\"></i></a> </div></td>";
                dato += "</tr>";
            }
            dato += "</tbody><tfoot><tr><th colspan=\"7\"><label class=\"pull-right\">Total :</label></th><td>" + suma + "</td></tr></tfoot></table>";
            dato += "</div></div></div></div></section>";
            return dato;
        }

        public static String VistaFacturacion(ref String dato, List<TblFactura> detalle)
        {
            dato += "<div class=\"col-md-12\"> <div class=\"form-row\"> <div class=\"form-group col-md-3\"> <label>Fecha de Facturación</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div> <input disabled type=\"text\" class=\"form-control pull-right datepicker\" id=\"Item7_Fecha\" name=\"Item7.Fecha\" value=\"\"> </div> </div> <div class=\"form-group col-md-3\"> <label>N° Factura</label> <input disabled class=\"form-control\" type=\"text\" id=\"Item7_NumeFactu\" name=\"Item7.NumeFactu\" value=\"\"> </div> <div class=\"form-group col-md-3\"> <label>Monto a Facturar</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-usd\"></i> </div> <input disabled class=\"form-control pull-right\" type=\"text\" data-val=\"true\" id=\"Item7_Monto\" name=\"Item7.Monto\" value=\"\"> </div> </div> <div class=\"form-group col-md-4\"> <label>Archivo de Cotización</label> <input disabled type=\"file\" id=\"Item7_DocFact\" name=\"Item7.DocFact\"> </div> </div> <div class=\"form-group col-md-2\"> <br> <button disabled type=\"submit\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i>Registrar</button> </div> <div class=\"form-group col-md-2\"> <br> <a disabled class=\"btn btn-info\"><i class=\"fa fa-arrow-circle-o-up\"></i> Ir a facturar</a> </div></div>";
            dato += "<section class=\"content\"> <div class=\"row\"> <div class=\"col-xs-12\"> <div class=\"box box-success\"> <div class=\"box-header\"> <h3 class=\"box-title\" style=\"font-weight: bold;\">Listado de Facturas</h3> </div> <div class=\"box-body\"> <table class=\"table table-bordered table-striped tablas\"> <thead> <tr> <th>Fecha Facturación</th> <th>N° de Factura</th> <th>Monto a Facturar</th> <th>Archivo</th> <th>Acciones</th> </tr> </thead> <tbody>";

            foreach (TblFactura p in detalle)
            {
                dato += "<tr>";
                dato += "<td>" + p.FechaFactu + "</td>";
                dato += "<td>" + p.NumeFactu + "</td>";
                dato += "<td>" + p.Monto + "</td>";
                dato += "<td>" + p.Documentonombre + "</td>";
                dato += "<td> <div class=\"btn-group\"> <a disabled class=\"btn btn-block btn-success btn-xs disabled\"><i class=\"fa fa-download\"></i></a> </div> <div class=\"btn-group\"> <a class=\"btn btn-block btn-info btn-xs disabled\"><i class=\"fa fa-envelope\"></i></a> </div> <div class=\"btn-group\"> <a disabled class=\"btn btn-block btn-success btn-xs\"><i class=\"fa fa-whatsapp\"></i></a> </div> <div class=\"btn-group\"> <a disabled class=\"btn btn-block btn-warning btn-xs disabled\"><i class=\"fa fa-pencil\"></i></a> </div> <div class=\"btn-group\"> <a disabled class=\"btn btn-block btn-danger btn-xs disabled\"><i class=\"fa fa-ban\"></i></a> </div></td></tr>";
            }

            dato += "</tbody> </table> </div> </div> </div> </div></section>";

            return dato;
        }

        public static String VistaSolicitud(ref String dato, TblSolicitud detalle)
        {
            var boleto = $"{(detalle.BoletoAereo is true ? "checked=\"checked\"" : "")}";
            var hotel = $"{(detalle.Hotel is true ? "checked=\"checked\"" : "")}";
            var trasla = $"{(detalle.Traslado is true ? "checked=\"checked\"" : "")}";
            var excur = $"{(detalle.Excursion is true ? "checked=\"checked\"" : "")}";
            var guias = $"{(detalle.Guias is true ? "checked=\"checked\"" : "")}";
            var segur = $"{(detalle.SeguroViajes is true ? "checked=\"checked\"" : "")}";
            var carro = $"{(detalle.AlquilerCarro is true ? "checked=\"checked\"" : "")}";

            var destino="";

            if (!(detalle.DestinoId is null))
            {
                destino = detalle.Destino.Nombre;
            }

            dato += "<div class=\"col-md-12\"> <div class=\"row form-group\"> <div class=\"col-md-3\"> <label for=\"inputEmail4\">Solicitado Desde</label> <select disabled class=\"form-control\" data-val=\"true\" id=\"Item1_Solicitadode\" name=\"Item1.Solicitadode\">";
            dato += "<option value=\"Pagina web\">"+detalle.Solicitadode+ "</option></select></div>";

            dato += "<div class=\"col-md-3\"> <label for=\"inputEmail4\">Idioma</label> <select disabled class=\"form-control\" data-val=\"true\" id=\"Item1_Idioma\" name=\"Item1.Idioma\">";
            dato += "<option value=\"\">"+detalle.Idioma+"</option></select>";
            dato += "</div> <div class=\"col-md-3\"> <label for=\"inputEmail4\">Llegada</label> <select disabled class=\"form-control\" data-val=\"true\" id=\"Item1_Mediollegada\" name=\"Item1.Mediollegada\">";
            dato += "<option value=\"\">"+detalle.Mediollegada+"</option></select>";
            dato += "</div> <div class=\"col-md-3\"> <label>Destino</label> <select disabled class=\"form-control\" id=\"Item1_DestinoId\" name=\"Item1.DestinoId\">";
            dato += "<option selected=\"selected\" value=\"\">"+destino+"</option></select></div></div>";

            dato += "<div class=\"row form-group\"> <div class=\"col-md-12\"> <label>Tipo de reserva</label> <div class=\"checkbox\">";
            dato += "<label><input disabled type=\"checkbox\""+ boleto + "data-val=\"true\"  id=\"Item1_BoletoAereo\" name=\"Item1.BoletoAereo\" value=\"true\">Boleto</label>";
            dato += "<label><input disabled type=\"checkbox\""+ hotel + "data-val=\"true\"  id=\"Item1_Hotel\" name=\"Item1.Hotel\" value=\"true\">Hotel</label>";
            dato += "<label><input disabled type=\"checkbox\""+ trasla + "data-val=\"true\" id=\"Item1_Traslado\" name=\"Item1.Traslado\" value=\"true\">Traslado</label>";
            dato += "<label><input disabled type=\"checkbox\""+ excur + "data-val=\"true\"  id=\"Item1_Excursion\" name=\"Item1.Excursion\" value=\"true\">Excursión</label>";
            dato += "<label><input disabled type=\"checkbox\""+ guias + "data-val=\"true\"  id=\"Item1_Guias\" name=\"Item1.Guias\" value=\"true\">Guias</label>";
            dato += "<label><input disabled type=\"checkbox\""+ segur + "data-val=\"true\"  id=\"Item1_SeguroViajes\" name=\"Item1.SeguroViajes\" value=\"true\">Seguro</label>";
            dato += "<label><input disabled type=\"checkbox\""+ carro + "data-val=\"true\"  id=\"Item1_AlquilerCarro\" name=\"Item1.AlquilerCarro\" value=\"true\">Alquier de Carro</label>";
            dato += "</div> </div> </div> <div class=\"row form-group col-md-12\"> <label>Cantidad de Personas</label> </div> <div class=\"row \"> <div class=\"col-md-12\"> <div class=\"form-group col-md-1\">";
            dato += "<label>Adultos</label><input disabled type=\"number\"  class=\"form-control\" id=\"Item1_Cntadulto\" name=\"Item1.Cntadulto\" value=\""+detalle.Cntadulto+"\"></div>";
            dato += "<div class=\"form-group col-md-1\"> <label>Niños</label><input disabled type=\"number\" class=\"form-control\" id=\"Item1_Cntnino\" name=\"Item1.Cntnino\" value=\""+detalle.Cntnino+"\"></div>";
            dato += "<div class=\"form-group col-md-1\"> <label>Infantes</label><input disabled type=\"number\" class=\"form-control\" id=\"Item1_Cntinfa\" name=\"Item1.Cntinfa\" value=\""+detalle.Cntinfa+"\"></div></div></div>";
            dato += "<div class=\"row form-group col-md-12\"> <label>Fecha y horarios</label> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group col-md-3\"> <label for=\"inputEmail4\">Fecha de Ida</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div>";
            dato += "<input disabled type=\"text\" class=\"form-control pull-right datepicker\"  id=\"Item1_FechaIn\" name=\"Item1.FechaIn\" value=\""+ String.Format("{0:dd/MM/yyyy}", detalle.FechaIn)+"\"></div></div>";
            dato += "<div class=\"form-group col-md-3\"> <label for=\"inputEmail4\">Fecha de retorno</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-calendar\"></i> </div>";
            dato += "<input disabled type=\"text\" class=\"form-control pull-right datepicker\"  id=\"Item1_FechaOut\" name=\"Item1.FechaOut\" value=\""+ String.Format("{0:dd/MM/yyyy}", detalle.FechaOut)+"\"></div></div></div></div>";
            dato += "<div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group col-md-3 bootstrap-timepicker\"><div class=\"bootstrap-timepicker-widget dropdown-menu\"><table><tbody><tr><td><a href=\"#\" data-action=\"incrementHour\"><i class=\"glyphicon glyphicon-chevron-up\"></i></a></td><td class=\"separator\">&nbsp;</td><td><a href=\"#\" data-action=\"incrementMinute\"><i class=\"glyphicon glyphicon-chevron-up\"></i></a></td><td class=\"separator\">&nbsp;</td><td class=\"meridian-column\"><a href=\"#\" data-action=\"toggleMeridian\"><i class=\"glyphicon glyphicon-chevron-up\"></i></a></td></tr><tr><td><span class=\"bootstrap-timepicker-hour\">04</span></td> <td class=\"separator\">:</td><td><span class=\"bootstrap-timepicker-minute\">45</span></td> <td class=\"separator\">&nbsp;</td><td><span class=\"bootstrap-timepicker-meridian\">PM</span></td></tr><tr><td><a href=\"#\" data-action=\"decrementHour\"><i class=\"glyphicon glyphicon-chevron-down\"></i></a></td><td class=\"separator\"></td><td><a href=\"#\" data-action=\"decrementMinute\"><i class=\"glyphicon glyphicon-chevron-down\"></i></a></td><td class=\"separator\">&nbsp;</td><td><a href=\"#\" data-action=\"toggleMeridian\"><i class=\"glyphicon glyphicon-chevron-down\"></i></a></td></tr></tbody></table></div> <label for=\"inputEmail4\">Horario de Ida</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-clock-o\"></i> </div>";
            dato += "<input disabled type=\"text\" class=\"form-control timepicker\" data-val=\"true\" id=\"Item1_HoraVueloIda\" maxlength=\"10\" name=\"Item1.HoraVueloIda\" value=\""+detalle.HoraVueloIda+"\"></div></div>";
            dato += "<div class=\"form-group col-md-3 bootstrap-timepicker\"><div class=\"bootstrap-timepicker-widget dropdown-menu\"><table><tbody><tr><td><a href=\"#\" data-action=\"incrementHour\"><i class=\"glyphicon glyphicon-chevron-up\"></i></a></td><td class=\"separator\">&nbsp;</td><td><a href=\"#\" data-action=\"incrementMinute\"><i class=\"glyphicon glyphicon-chevron-up\"></i></a></td><td class=\"separator\">&nbsp;</td><td class=\"meridian-column\"><a href=\"#\" data-action=\"toggleMeridian\"><i class=\"glyphicon glyphicon-chevron-up\"></i></a></td></tr><tr><td><span class=\"bootstrap-timepicker-hour\">03</span></td> <td class=\"separator\">:</td><td><span class=\"bootstrap-timepicker-minute\">05</span></td> <td class=\"separator\">&nbsp;</td><td><span class=\"bootstrap-timepicker-meridian\">AM</span></td></tr><tr><td><a href=\"#\" data-action=\"decrementHour\"><i class=\"glyphicon glyphicon-chevron-down\"></i></a></td><td class=\"separator\"></td><td><a href=\"#\" data-action=\"decrementMinute\"><i class=\"glyphicon glyphicon-chevron-down\"></i></a></td><td class=\"separator\">&nbsp;</td><td><a href=\"#\" data-action=\"toggleMeridian\"><i class=\"glyphicon glyphicon-chevron-down\"></i></a></td></tr></tbody></table></div> <label for=\"inputEmail4\">Horario de retorno</label> <div class=\"input-group date\"> <div class=\"input-group-addon\"> <i class=\"fa fa-clock-o\"></i> </div>";
            dato += "<input disabled type=\"text\" class=\"form-control timepicker\" data-val=\"true\" id=\"Item1_HoraVueloRegreso\" maxlength=\"10\" name=\"Item1.HoraVueloRegreso\" value=\""+detalle.HoraVueloRegreso+"\"></div></div></div></div>";
            dato += "<div class=\"form-group col-md-2\"> <label for=\"\"></label> <br> <button class=\"btn btn-primary disabled\"><i class=\"fa fa-save\"></i> Actualizar</button> </div></div>";
            return dato;
        }

        public static String TipoEstadoSolicitud(String dato,TblSolicitud statsolicitud)
        {
            if (statsolicitud.Cancelaestado is true)
            {
                dato = "Cancelado";
            }
            else if (statsolicitud.Cancelaestado is false)
            {
                if (statsolicitud.Estadofile == 1)
                {
                    dato = "Fase Solicitud";
                }
                else if (statsolicitud.Estadofile == 2)
                {
                    dato = "Fase Pasajero";
                }
                else if (statsolicitud.Estadofile == 3)
                {
                    dato = "Fase Cotización";
                }
                else if (statsolicitud.Estadofile == 4)
                {
                    dato = "Fase Reserva";
                }
                else if (statsolicitud.Estadofile == 5)
                {
                    dato = "Fase Liquidación Cliente";
                }
                else if (statsolicitud.Estadofile == 6)
                {
                    dato = "Fase Liquidación Proveedor";
                }
                else if (statsolicitud.Estadofile == 7)
                {
                    dato = "Fase Factura";
                }
                else if (statsolicitud.Estadofile == 8)
                {
                    dato = "Finalizado";
                }
            }
            
            return dato;
        }

        public static Boolean NumePrincipales(List<TblPasajeros> pasajeros)
        {
            bool valor = false ; //False CUANDO NO EXISTE MAS DE UN PASAJERO PRINCIPAL
            var NumePrincipales = 0;
            if (pasajeros != null)
            {
                 NumePrincipales = pasajeros.Count(a => a.Principal == true);
            }

            if (NumePrincipales > 0)
            {
                valor = true; //YA EXISTE UN PASAJERO PRINCIPAL
            }

            return valor;
        }

        public static Boolean ValidarPasaPrincipal(TblPasajeros pasajero, bool ExistPrincipal)
        {
            bool valor = true; //CUANDO NO ES PASAJERO PRINCIPAL

            if (ExistPrincipal) //EXISTE UN PRINCIPAL
            {
                if (pasajero.Principal is true) //ES PASAJERO PRINCIPAL
                {
                    valor = false; //ES PASAJERO PRINCIPAL, SE DEJA EN FALSE PARA QUE CUANDO PASE A LA VISTA NO SE BLOQUEe
                }
            }
            else //NO EXISTE UN PRINCIPAL POR LO TANTO SE HABILITA EL CAMPO
            {
                valor = false;
            }


            return valor;
        }

        public static byte[] GetByteArray(IFormFile file)
        {
            using (var target = new MemoryStream())
            {
                file.CopyTo(target);
                return target.ToArray();
            }
        }

        public static Boolean PermitedExten(IFormFile file)
        {

            bool rpa = true;

            var ext = Path.GetExtension(file.FileName).ToLowerInvariant();

            if (!AdmitedExtens.Contains(ext))
            {
                rpa = false;
            }

            return rpa;
        }

        public static Boolean FileSize(IFormFile file, long fileSizeLimit)
        {
            bool rpa = true;

            if (file.Length > fileSizeLimit)
            {
                rpa = false;
            }

            return rpa;
        }

        public static SelectList LlenarBagPasajero(int adulto, int nino, int infa, List<TblPasajeros> pasajeros)
        {
            SelectList tpasa = null;
            List<SelectListItem> Tipopasa = new List<SelectListItem>();

            var NAdulto = 0;
            var Nnino = 0;
            var Ninante = 0;
            var sum = 0; //CUANDO SEA 0 se debe mandar como nulo

            if (pasajeros != null)
            {
                NAdulto = pasajeros.Count(a => a.TipoPasajeroId == 2);
                Nnino = pasajeros.Count(a => a.TipoPasajeroId == 3);
                Ninante = pasajeros.Count(a => a.TipoPasajeroId == 1);
            }

            if (adulto > 0 || nino > 0 || infa > 0)
            {
                if (adulto > 0) //Se necesitan esta cantidad
                {
                    if (NAdulto < adulto) //Si el numero de adultos(registro con ese tipo pasa) no excede de lo requerido entonces se mostrara el campo
                    {
                        Tipopasa.Add(new SelectListItem() { Value = "2", Text = "Adulto" });
                        sum++;
                    }
                }
                if (nino > 0)
                {
                    if (Nnino < nino)
                    {
                        Tipopasa.Add(new SelectListItem() { Value = "3", Text = "Niño" });
                        sum++;
                    }
                }
                if (infa > 0)
                {
                    if (Ninante < infa)
                    {
                        Tipopasa.Add(new SelectListItem() { Value = "1", Text = "Infante" });
                        sum++;
                    }
                }

                if (sum > 0)
                {
                    tpasa = new SelectList(Tipopasa, "Value", "Text");
                }
                
                return tpasa;
            }
            else
            {
                return tpasa;
            }
        }

        public static SelectList LlenarBagPasajeroSpecific(int id)
        {
            SelectList tpasa = null;
            List<SelectListItem> Tipopasa = new List<SelectListItem>();

            if (id == 2)
            {
                Tipopasa.Add(new SelectListItem() { Value = "2", Text = "Adulto" });
            }
            else if(id == 3){
                Tipopasa.Add(new SelectListItem() { Value = "3", Text = "Niño" });
            }
            else if (id == 1)
            {
                Tipopasa.Add(new SelectListItem() { Value = "1", Text = "Infante" });
            }
            tpasa = new SelectList(Tipopasa, "Value", "Text");
            return tpasa;
        }

    }
}
