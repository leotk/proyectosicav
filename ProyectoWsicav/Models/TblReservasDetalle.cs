﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_ReservasDetalle")]
    public partial class TblReservasDetalle
    {
        [Key]
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [Required(ErrorMessage = "Fecha Requerida")]
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaIn { get; set; }
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaOut { get; set; }
        public int TipoServicioId { get; set; }
        [Required(ErrorMessage = "Localizador requerido")]
        [StringLength(50)]
        public string Localizador { get; set; }
        [Required(ErrorMessage = "Fecha Requerida")]
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaLimite { get; set; }
        public int ProveedorId { get; set; }
        [Required(ErrorMessage = "Monto requerido")]
        [Column(TypeName = "decimal(18, 2)"), RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Monto invalido")]
        public decimal? Monto { get; set; }
        public bool Adicional { get; set; }
        public int GuiaId { get; set; }
        public int VehiculoId { get; set; }

        [ForeignKey(nameof(ProveedorId))]
        //[InverseProperty(nameof(TblProveedor.TblReservasDetalle))]
        public virtual TblProveedor Proveedor { get; set; }
        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblReservasDetalle))]
        public virtual TblSolicitud Solicitud { get; set; }
        [ForeignKey(nameof(TipoServicioId))]
        //[InverseProperty(nameof(TblTipoServicio.TblReservasDetalle))]
        public virtual TblTipoServicio TipoServicio { get; set; }
    }
}
