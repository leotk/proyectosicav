﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoServicio")]
    public partial class TblTipoServicio
    {
        public TblTipoServicio()
        {
            //TblReservasDetalle = new HashSet<TblReservasDetalle>();
            //TblEncuesta = new HashSet<TblEncuesta>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }
        public bool? Estadotiposervicio { get; set; }
        [StringLength(250)]
        public string Pregunta { get; set; }

        //[InverseProperty("TipoServicio")]
        //public virtual ICollection<TblReservasDetalle> TblReservasDetalle { get; set; }

        //[InverseProperty("TipoServicio")]
        //public virtual ICollection<TblEncuesta> TblEncuesta { get; set; }
    }
}
