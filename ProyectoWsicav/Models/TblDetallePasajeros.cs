﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_DetallePasajeros")]
    public partial class TblDetallePasajeros
    {
        [Key]
        public int Id { get; set; }
        public int PasajeroId { get; set; }
        public int SolicitudId { get; set; }
        [StringLength(50)]
        public string FotoNombre { get; set; }
        [StringLength(150)]
        public string FotoUrl { get; set; }

        [ForeignKey(nameof(PasajeroId))]
        //[InverseProperty(nameof(TblPasajeros.TblDetallePasajeros))]
        public virtual TblPasajeros Pasajero { get; set; }
        [ForeignKey(nameof(SolicitudId))]
        //[InverseProperty(nameof(TblSolicitud.TblDetallePasajeros))]
        public virtual TblSolicitud Solicitud { get; set; }
    }
}
