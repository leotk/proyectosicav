﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_TipoPasajero")]
    public partial class TblTipoPasajero
    {
        public TblTipoPasajero()
        {
            //TblPasajeros = new HashSet<TblPasajeros>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(10)]
        public string Codigo { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }
        //[Column("adulto")]
        //public int? Adulto { get; set; }
        //[Column("infante")]
        //public int? Infante { get; set; }
        //[Column("niño")]
        //public int? Niño { get; set; }

        //[InverseProperty("TipoPasajero")]
        //public virtual ICollection<TblPasajeros> TblPasajeros { get; set; }
    }
}
