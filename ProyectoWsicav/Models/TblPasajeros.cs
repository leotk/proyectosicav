﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Pasajeros")]
    public partial class TblPasajeros
    {
        public TblPasajeros()
        {
            //TblDetallePasajeros = new HashSet<TblDetallePasajeros>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string NombresPasajeros { get; set; }
        public int? TipoPasajeroId { get; set; }
        public int? Edad { get; set; }
        public int? TipoDocumentoIdentidadId { get; set; }
        [StringLength(50)]
        public string NumeroDucumento { get; set; }
        [StringLength(50)]
        public string Nacionalidad { get; set; }
        public bool Principal { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? FechaNac { get; set; }

        [ForeignKey(nameof(TipoDocumentoIdentidadId))]
        //[InverseProperty(nameof(TblTipoDocumentoIdentidad.TblPasajeros))]
        public virtual TblTipoDocumentoIdentidad TipoDocumentoIdentidad { get; set; }
        [ForeignKey(nameof(TipoPasajeroId))]
        //[InverseProperty(nameof(TblTipoPasajero.TblPasajeros))]
        public virtual TblTipoPasajero TipoPasajero { get; set; }

        //[InverseProperty("Pasajero")]
        //public virtual ICollection<TblDetallePasajeros> TblDetallePasajeros { get; set; }
    }
}
