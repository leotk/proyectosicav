﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Estatus")]
    public partial class TblEstatus
    {
        public TblEstatus()
        {
            TblSolicitud = new HashSet<TblSolicitud>();
        }

        [Key]
        public int Id { get; set; }
        public bool Estado { get; set; }
        [Required]
        [StringLength(50)]
        public string Color { get; set; }

        [InverseProperty("Estatus")]
        public virtual ICollection<TblSolicitud> TblSolicitud { get; set; }
    }
}
