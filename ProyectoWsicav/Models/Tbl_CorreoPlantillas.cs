﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.Models
{
    public partial class Tbl_CorreoPlantillas
    {
        public Tbl_CorreoPlantillas()
        {
            CorreoCampana = new HashSet<Tbl_CorreoCampana>();
        }
        [Key]
        [Column("id")]
        public int Id { get; set; }
        public string NombrePlantilla { get; set; }
        public string CodigoHTML { get; set; }
        public string Observaciones { get; set; }

        public virtual ICollection<Tbl_CorreoCampana> CorreoCampana { get; set; }
    }
}
