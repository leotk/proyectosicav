﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Liquidacion")]
    public partial class TblLiquidacion
    {
        [Key]
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaLiqui { get; set; }
        [StringLength(50)]
        public string NumeLiqui { get; set; }
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaLimiteLiqui { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Monto { get; set; }
        [StringLength(50)]
        public string Documentonombre { get; set; }
        [StringLength(150)]
        public string DocumentoUrl { get; set; }

        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblLiquidacion))]
        public virtual TblSolicitud Solicitud { get; set; }
    }
}
