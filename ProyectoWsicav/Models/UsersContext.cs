﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Models
{
    public partial class UsersContext : DbContext
    {
        public UsersContext()
        {
        }

        public UsersContext(DbContextOptions<UsersContext> options)
            : base(options)
        {
        }
        public virtual DbSet<viw_ClientePerteneceCampana> viw_ClientePerteneceCampana { get; set; }
        public virtual DbSet<Tbl_CorreoPlantillas> Tbl_CorreoPlantillas { get; set; }
        public virtual DbSet<Tbl_CorreoEmpresa> Tbl_CorreoEmpresa { get; set; }
        public virtual DbSet<Tbs_CorreoClienteCampanaPertenece> Tbs_CorreoClienteCampanaPertenece { get; set; }
        public virtual DbSet<Tbl_CorreoCampana> Tbl_CorreoCampana { get; set; }
        public virtual DbSet<TblCiudad> TblCiudad { get; set; }
        public virtual DbSet<TblCliente> TblCliente { get; set; }
        public virtual DbSet<TblConductor> TblConductor { get; set; }
        public virtual DbSet<TblConfiguracion> TblConfiguracion { get; set; }
        public virtual DbSet<TblCotizacion> TblCotizacion { get; set; }
        public virtual DbSet<TblDestino> TblDestino { get; set; }
        public virtual DbSet<TblDetallePasajeros> TblDetallePasajeros { get; set; }
        public virtual DbSet<TblEncuesta> TblEncuesta { get; set; }
        public virtual DbSet<TblEstatus> TblEstatus { get; set; }
        public virtual DbSet<TblFactura> TblFactura { get; set; }
        public virtual DbSet<TblGuia> TblGuia { get; set; }
        public virtual DbSet<TblLiquiCliente> TblLiquiCliente { get; set; }
        public virtual DbSet<TblLiquiProve> TblLiquiProve { get; set; }
        public virtual DbSet<TblLiquidacion> TblLiquidacion { get; set; }
        public virtual DbSet<TblPasajeros> TblPasajeros { get; set; }
        public virtual DbSet<TblPermiso> TblPermiso { get; set; }
        public virtual DbSet<TblProveedor> TblProveedor { get; set; }
        public virtual DbSet<TblReservasDetalle> TblReservasDetalle { get; set; }
        public virtual DbSet<TblSolicitud> TblSolicitud { get; set; }
        public virtual DbSet<TblTipoCliente> TblTipoCliente { get; set; }
        public virtual DbSet<TblTipoDocumentoIdentidad> TblTipoDocumentoIdentidad { get; set; }
        public virtual DbSet<TblTipoLicencia> TblTipoLicencia { get; set; }
        public virtual DbSet<TblTipoPasajero> TblTipoPasajero { get; set; }
        public virtual DbSet<TblTipoServicio> TblTipoServicio { get; set; }
        //public virtual DbSet<TblTipoServicioEstado> TblTipoServicioEstado { get; set; }
        public virtual DbSet<TblTipoVehiculo> TblTipoVehiculo { get; set; }
        public virtual DbSet<TblUsuario> TblUsuario { get; set; }
        public virtual DbSet<TblVehiculo> TblVehiculo { get; set; }
        public virtual DbSet<TblVendedor> TblVendedor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=localhost\\sql2012;Database=dbwsicavprueba;User Id=sa;Password=Ws1c4v321");
                //optionsBuilder.UseSqlServer("Data Source=LCONDORI;Initial Catalog=dbwsicavprueba;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                //optionsBuilder.UseSqlServer("Server=localhost,1440;Database=dbwsicavprueba;User Id=sa;Password=proY3ct0#Camara");
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-9V5T2DM;Initial Catalog=dbwsicavprueba;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblCliente>(entity =>
            {
                entity.Property(e => e.Celular).IsUnicode(false);

                entity.Property(e => e.Codigo)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Direccion).IsUnicode(false);

                entity.Property(e => e.Documento).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.Telefono).IsUnicode(false);

                entity.HasOne(d => d.TipoCliente)
                    .WithMany(p => p.TblCliente)
                    .HasForeignKey(d => d.TipoClienteId)
                    .HasConstraintName("FK_Tbl_Cliente_Tbl_TipoCliente");

                entity.HasOne(d => d.TipoDocumentoIdentidad)
                    .WithMany(p => p.TblCliente)
                    .HasForeignKey(d => d.TipoDocumentoIdentidadId)
                    .HasConstraintName("FK_Tbl_Cliente_Tbl_TipoDocumentoIdentidad");
            });

            modelBuilder.Entity<TblConductor>(entity =>
            {
                entity.Property(e => e.Apellidos).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Direccion).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nombres).IsUnicode(false);

                entity.HasOne(d => d.TipoDocumentoIdentidad)
                    .WithMany(p => p.TblConductor)
                    .HasForeignKey(d => d.TipoDocumentoIdentidadId)
                    .HasConstraintName("FK__Tbl_Condu__TipoD__4222D4EF");

                entity.HasOne(d => d.TipoLicencia)
                    .WithMany(p => p.TblConductor)
                    .HasForeignKey(d => d.TipoLicenciaId)
                    .HasConstraintName("FK__Tbl_Condu__TipoL__4316F928");
            });

            modelBuilder.Entity<TblConfiguracion>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Valor).IsFixedLength();
            });

            modelBuilder.Entity<TblCotizacion>(entity =>
            {
                entity.Property(e => e.Id).UseIdentityColumn();
                entity.Property(e => e.NumeCotizacion).IsUnicode(false);

                entity.HasOne(d => d.Solicitud)
                    .WithMany(p => p.TblCotizacion)
                    .HasForeignKey(d => d.SolicitudId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tbl_Cotizacion_Tbl_Solicitud");
            });

            modelBuilder.Entity<TblDestino>(entity =>
            {
                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.HasOne(d => d.Ciudad)
                    .WithMany(p => p.TblDestino)
                    .HasForeignKey(d => d.CiudadId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tbl_Destino_Tbl_Ciudad");
            });

            modelBuilder.Entity<TblFactura>(entity =>
            {
                entity.Property(e => e.Id).UseIdentityColumn();
                entity.HasOne(d => d.Solicitud)
                    .WithMany(p => p.TblFactura)
                    .HasForeignKey(d => d.SolicitudId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tbl_Factura_Tbl_Solicitud");
            });

            modelBuilder.Entity<TblGuia>(entity =>
            {
                entity.Property(e => e.Apellidos).IsUnicode(false);

                entity.Property(e => e.Codigo).IsUnicode(false);

                entity.Property(e => e.Direccion).IsUnicode(false);

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nombres).IsUnicode(false);

                entity.Property(e => e.Telefono).IsUnicode(false);

                entity.HasOne(d => d.TipoDocumentoIdentidad)
                    .WithMany(p => p.TblGuia)
                    .HasForeignKey(d => d.TipoDocumentoIdentidadId)
                    .HasConstraintName("FK__Tbl_Guia__TipoDo__46E78A0C");
            });

            modelBuilder.Entity<TblLiquiCliente>(entity =>
            {
                entity.Property(e => e.Banco).IsUnicode(false);

                entity.Property(e => e.FormaPago).IsUnicode(false);

                entity.Property(e => e.NumeTarjeta).IsFixedLength();

                entity.Property(e => e.Titular).IsUnicode(false);

                entity.HasOne(d => d.Solicitud)
                    .WithMany(p => p.TblLiquiCliente)
                    .HasForeignKey(d => d.SolicitudId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tbl_LiquiCliente_Tbl_Solicitud");
            });

            modelBuilder.Entity<TblLiquiProve>(entity =>
            {
                entity.Property(e => e.Banco).IsUnicode(false);

                entity.Property(e => e.FormaPago).IsUnicode(false);

                entity.Property(e => e.NumeTarjeta).IsFixedLength();

                entity.Property(e => e.Titular).IsUnicode(false);

                //entity.HasOne(d => d.Proveedor)
                //    .WithMany(p => p.TblLiquiProve)
                //    .HasForeignKey(d => d.ProveedorId)
                //    .HasConstraintName("FK_Tbl_LiquiProve_Tbl_Proveedor");

                entity.HasOne(d => d.Solicitud)
                    .WithMany(p => p.TblLiquiProve)
                    .HasForeignKey(d => d.SolicitudId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Tbl_LiquiProve_Tbl_Solicitud");
            });

            modelBuilder.Entity<TblPasajeros>(entity =>
            {
                entity.Property(e => e.Nacionalidad).IsUnicode(false);

                entity.Property(e => e.NombresPasajeros).IsUnicode(false);

                entity.Property(e => e.NumeroDucumento).IsUnicode(false);

                //entity.HasOne(d => d.TipoDocumentoIdentidad)
                //    .WithMany(p => p.TblPasajeros)
                //    .HasForeignKey(d => d.TipoDocumentoIdentidadId)
                //    .HasConstraintName("FK_Tbl_Pasajeros_Tbl_TipoDocumentoIdentidad");

                //    entity.HasOne(d => d.TipoPasajero)
                //        .WithMany(p => p.TblPasajeros)
                //        .HasForeignKey(d => d.TipoPasajeroId)
                //        .HasConstraintName("FK_Tbl_Pasajeros_Tbl_TipoPasajero");
                //});
            });

            modelBuilder.Entity<TblPermiso>(entity =>
                {
                    entity.Property(e => e.Descripcion).IsUnicode(false);
                });

                modelBuilder.Entity<TblProveedor>(entity =>
                {
                    entity.Property(e => e.Codigo).IsUnicode(false);

                    entity.Property(e => e.Direccion).IsUnicode(false);

                    entity.Property(e => e.Documento).IsUnicode(false);

                    entity.Property(e => e.Email).IsUnicode(false);

                    entity.Property(e => e.Nombre).IsUnicode(false);

                    entity.Property(e => e.Telefono).IsUnicode(false);

                    entity.HasOne(d => d.TipoDocumentoIdentidad)
                        .WithMany(p => p.TblProveedor)
                        .HasForeignKey(d => d.TipoDocumentoIdentidadId)
                        .HasConstraintName("FK_Tbl_Proveedor_Tbl_TipoDocumentoIdentidad");
                });

                modelBuilder.Entity<TblReservasDetalle>(entity =>
                {
                    entity.Property(e => e.Localizador).IsUnicode(false);

                    //entity.HasOne(d => d.Proveedor)
                    //    .WithMany(p => p.TblReservasDetalle)
                    //    .HasForeignKey(d => d.ProveedorId)
                    //    .OnDelete(DeleteBehavior.ClientSetNull)
                    //    .HasConstraintName("FK_Tbl_ReservasDetalle_Tbl_Proveedor");

                    entity.HasOne(d => d.Solicitud)
                        .WithMany(p => p.TblReservasDetalle)
                        .HasForeignKey(d => d.SolicitudId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_Tbl_ReservasDetalle_Tbl_Solicitud");

                    //entity.HasOne(d => d.TipoServicio)
                    //    .WithMany(p => p.TblReservasDetalle)
                    //    .HasForeignKey(d => d.TipoServicioId)
                    //    .OnDelete(DeleteBehavior.ClientSetNull)
                    //    .HasConstraintName("FK_Tbl_ReservasDetalle_Tbl_TipoServicio");
                });

                modelBuilder.Entity<TblDetallePasajeros>(entity =>
                {
                    //entity.HasOne(d => d.Pasajero)
                    //    .WithMany(p => p.TblDetallePasajeros)
                    //    .HasForeignKey(d => d.PasajeroId)
                    //    .OnDelete(DeleteBehavior.ClientSetNull)
                    //    .HasConstraintName("FK_DetallePasajeros_Tbl_Pasajeros");

                    //entity.HasOne(d => d.Solicitud)
                    //    .WithMany(p => p.TblDetallePasajeros)
                    //    .HasForeignKey(d => d.SolicitudId)
                    //    .OnDelete(DeleteBehavior.ClientSetNull)
                    //    .HasConstraintName("FK_DetallePasajeros_Tbl_Solicitud");
                });

                modelBuilder.Entity<TblEncuesta>(entity =>
                {
                    entity.HasOne(d => d.Solicitud)
                        .WithMany(p => p.TblEncuesta)
                        .HasForeignKey(d => d.SolicitudId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_Tbl_Encuesta_Tbl_Solicitud");

                    //entity.HasOne(d => d.TipoServicio)
                    //    .WithMany(p => p.TblEncuesta)
                    //    .HasForeignKey(d => d.TipoServicioId)
                    //    .HasConstraintName("FK_Tbl_Encuesta_Tbl_TipoServicio");
                });

                modelBuilder.Entity<TblLiquidacion>(entity =>
                {
                    entity.Property(e => e.NumeLiqui).IsUnicode(false);

                    entity.HasOne(d => d.Solicitud)
                        .WithMany(p => p.TblLiquidacion)
                        .HasForeignKey(d => d.SolicitudId)
                        .OnDelete(DeleteBehavior.ClientSetNull)
                        .HasConstraintName("FK_Tbl_Liquidacion_Tbl_Solicitud");
                });



                modelBuilder.Entity<TblSolicitud>(entity =>
                {
                    entity.Property(e => e.CorreoContacto).IsUnicode(false);

                    entity.Property(e => e.Descripcion).IsUnicode(false);

                    entity.Property(e => e.HoraVueloIda).IsUnicode(false);

                    entity.Property(e => e.HoraVueloRegreso).IsUnicode(false);

                    entity.Property(e => e.Idioma).IsUnicode(false);

                    entity.Property(e => e.Mediollegada).IsUnicode(false);

                    entity.Property(e => e.Nomfile).IsUnicode(false);

                    entity.Property(e => e.NumeroSolicitud).IsUnicode(false);

                    entity.Property(e => e.PersonaContacto).IsUnicode(false);

                    entity.Property(e => e.Solicitadode).IsUnicode(false);

                    entity.Property(e => e.TelefonoContacto).IsUnicode(false);

                    entity.HasOne(d => d.Cliente)
                        .WithMany(p => p.TblSolicitud)
                        .HasForeignKey(d => d.ClienteId)
                        .HasConstraintName("FK_Tbl_Solicitud_Tbl_Cliente");

                    entity.HasOne(d => d.Destino)
                        .WithMany(p => p.TblSolicitud)
                        .HasForeignKey(d => d.DestinoId)
                        .HasConstraintName("FK_Tbl_Solicitud_Tbl_Destino");

                    entity.HasOne(d => d.Estatus)
                        .WithMany(p => p.TblSolicitud)
                        .HasForeignKey(d => d.EstatusId)
                        .HasConstraintName("FK_Tbl_Solicitud_Tbl_Estatus");

                    entity.HasOne(d => d.Usuario)
                        .WithMany(p => p.TblSolicitud)
                        .HasForeignKey(d => d.UsuarioId)
                        .HasConstraintName("FK_Tbl_Solicitud_Tbl_Usuario");

                    entity.HasOne(d => d.Vendedor)
                        .WithMany(p => p.TblSolicitud)
                        .HasForeignKey(d => d.VendedorId)
                        .HasConstraintName("FK_Tbl_Solicitud_Tbl_Vendedor");
                });

                modelBuilder.Entity<TblTipoCliente>(entity =>
                {
                    entity.Property(e => e.Descripcion).IsUnicode(false);
                });

                modelBuilder.Entity<TblTipoDocumentoIdentidad>(entity =>
                {
                    entity.Property(e => e.Descripcion).IsUnicode(false);
                });

                modelBuilder.Entity<TblTipoLicencia>(entity =>
                {
                    entity.Property(e => e.Licencia).IsUnicode(false);
                });

                modelBuilder.Entity<TblTipoPasajero>(entity =>
                {
                    entity.Property(e => e.Codigo).IsUnicode(false);

                    entity.Property(e => e.Descripcion).IsUnicode(false);
                });

                modelBuilder.Entity<TblTipoServicio>(entity =>
                {
                    entity.Property(e => e.Descripcion).IsUnicode(false);

                    //entity.HasOne(d => d.TipoServicioEstado)
                    //    .WithMany(p => p.TblTipoServicio)
                    //    .HasForeignKey(d => d.TipoServicioEstadoId)
                    //    .HasConstraintName("FK__Tbl_TipoS__TipoS__5629CD9C");
                });

                //modelBuilder.Entity<TblTipoServicioEstado>(entity =>
                //{
                //    entity.Property(e => e.Estado).IsUnicode(false);
                //});

                modelBuilder.Entity<TblTipoVehiculo>(entity =>
                {
                    entity.Property(e => e.TipoVehiculo).IsUnicode(false);
                });

                modelBuilder.Entity<TblUsuario>(entity =>
            {
                entity.Property(e => e.Apellido).IsUnicode(false);

                entity.Property(e => e.CodigoDepartamento)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Email).IsUnicode(false);

                entity.Property(e => e.Nombre).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Usuario).IsUnicode(false);

                entity.HasOne(d => d.Permiso)
                    .WithMany(p => p.TblUsuario)
                    .HasForeignKey(d => d.PermisoId)
                    .HasConstraintName("FK_Tbl_Usuario_Tbl_Permiso");
            });

                modelBuilder.Entity<TblVehiculo>(entity =>
                {
                    entity.Property(e => e.Codigo).IsUnicode(false);

                    entity.Property(e => e.Combustible).IsUnicode(false);

                    entity.Property(e => e.Marca).IsUnicode(false);

                    entity.Property(e => e.Placa).IsUnicode(false);

                    entity.HasOne(d => d.TipoVehiculo)
                        .WithMany(p => p.TblVehiculo)
                        .HasForeignKey(d => d.TipoVehiculoId)
                        .HasConstraintName("FK__Tbl_Vehic__TipoV__5812160E");
                });

                modelBuilder.Entity<TblVendedor>(entity =>
            {
                entity.Property(e => e.Nombres).IsUnicode(false);
            });

                OnModelCreatingPartial(modelBuilder);
            
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public DbSet<ProyectoWsicav.Models.viw_Biblia> viw_Biblia { get; set; }
    }
}
