﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Configuracion")]
    public partial class TblConfiguracion
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(500)]
        public string Clave { get; set; }
        [Required]
        [StringLength(50)]
        public string Valor { get; set; }
        [StringLength(500)]
        public string Observacion { get; set; }
    }
}
