﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.Models
{
    public partial class Tbs_CorreoClienteCampanaPertenece
    {
        public int Id { get; set; }
        public int ClienteID { get; set; }
        public int CampanaID { get; set; }
        //   public virtual ICollection<Tbl_CorreoCampana> Tbl_CorreoCampana { get; set; }

        // public virtual ICollection<TblCliente> TblCliente { get; set; }
         //public virtual Tbl_CorreoCampana Tbl_CorreoCampana { get; set; }
        //public virtual TblCliente Tbl_Cliente { get; set; }
    }
}
