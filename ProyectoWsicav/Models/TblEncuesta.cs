﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Encuesta")]
    public partial class TblEncuesta
    {
        [Key]
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        public int? TipoServicioId { get; set; }
        public int? Puntaje { get; set; }
        [Column(TypeName = "text")]
        public string Comentario { get; set; }

        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblEncuesta))]
        public virtual TblSolicitud Solicitud { get; set; }
        [ForeignKey(nameof(TipoServicioId))]
        //[InverseProperty(nameof(TblTipoServicio.TblEncuesta))]
        public virtual TblTipoServicio TipoServicio { get; set; }
    }
}
