﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Guia")]
    public partial class TblGuia
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Codigo { get; set; }
        public int? TipoDocumentoIdentidadId { get; set; }
        public int? NumDocumento { get; set; }
        [StringLength(100)]
        public string Apellidos { get; set; }
        [StringLength(100)]
        public string Nombres { get; set; }
        [StringLength(100)]
        public string Direccion { get; set; }
        [StringLength(10)]
        public string Telefono { get; set; }
        [StringLength(100)]
        public string Email { get; set; }

        [ForeignKey(nameof(TipoDocumentoIdentidadId))]
        [InverseProperty(nameof(TblTipoDocumentoIdentidad.TblGuia))]
        public virtual TblTipoDocumentoIdentidad TipoDocumentoIdentidad { get; set; }
    }
}
