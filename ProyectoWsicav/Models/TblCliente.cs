﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Cliente")]
    public partial class TblCliente
    {
        public TblCliente()
        {
            TblSolicitud = new HashSet<TblSolicitud>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(20)]
        public string Codigo { get; set; }
        public int? TipoClienteId { get; set; }
        [Column("nombre")]
        [StringLength(150)]
        public string Nombre { get; set; }
        public int? TipoDocumentoIdentidadId { get; set; }
        [StringLength(20)]
        public string Documento { get; set; }
        [StringLength(250)]
        public string Direccion { get; set; }
        [StringLength(100)]
        public string Telefono { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        //[Column("fechaNacimiento", TypeName = "date")] No estoy seguro como era antes
        //public DateTime? FechaNacimiento { get; set; }
        //public bool? Principal { get; set; }
        [StringLength(50)]
        public string Celular { get; set; }
        //[Column("ClienteProvieneID")]
        //public int? ClienteProvieneId { get; set; }

        [ForeignKey(nameof(TipoClienteId))]
        [InverseProperty(nameof(TblTipoCliente.TblCliente))]
        public virtual TblTipoCliente TipoCliente { get; set; }
        [ForeignKey(nameof(TipoDocumentoIdentidadId))]
        [InverseProperty(nameof(TblTipoDocumentoIdentidad.TblCliente))]
        public virtual TblTipoDocumentoIdentidad TipoDocumentoIdentidad { get; set; }
        [InverseProperty("Cliente")]
        public virtual ICollection<TblSolicitud> TblSolicitud { get; set; }
    }
}
