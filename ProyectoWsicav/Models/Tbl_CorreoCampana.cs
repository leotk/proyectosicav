﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.Models
{
    public partial class Tbl_CorreoCampana
    {
        public Tbl_CorreoCampana()
        {
            //Tbs_CorreoClienteCampanaPertenece = new HashSet<Tbs_CorreoClienteCampanaPertenece>();
        }
        [Key]
        public int Id { get; set; }
        public string Asunto { get; set; }
        public System.DateTime Desde { get; set; }
        public System.DateTime Hasta { get; set; }
        public bool Lunes { get; set; }
        public bool Martes { get; set; }
        public bool Miercoles { get; set; }
        public bool Jueves { get; set; }
        public bool Viernes { get; set; }
        public bool Sabado { get; set; }
        public bool Domingo { get; set; }
        public int CorreoPlantillaId { get; set; }
        public int CorreoEmpresaId { get; set; }
        public string Hora1 { get; set; }
        public string Hora2 { get; set; }
        public string Hora3 { get; set; }
        public bool Activo { get; set; }

        [ForeignKey(nameof(CorreoPlantillaId))]
        [InverseProperty(nameof(Tbl_CorreoPlantillas.CorreoCampana))]
        public virtual Tbl_CorreoPlantillas CorreoPlantilla { get; set; }

        [ForeignKey(nameof(CorreoEmpresaId))]
        [InverseProperty(nameof(Tbl_CorreoEmpresa.CorreoEmpresa))]
        public virtual Tbl_CorreoEmpresa CorreoEmpresa { get; set; }
        //public virtual ICollection<Tbs_CorreoClienteCampanaPertenece> Tbs_CorreoClienteCampanaPertenece { get; set; }
    }
}
