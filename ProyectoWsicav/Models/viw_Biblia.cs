﻿using System;
using System.Collections.Generic;

namespace ProyectoWsicav.Models
{
    public partial class viw_Biblia
    {
        public int Id { get; set; }
        public string NombresPasajeros { get; set; }
        public string Files { get; set; }
        public int nPasajeros { get; set; }
        public string idioma { get; set; }
        public string Agencia { get; set; }
        public string hora { get; set; }
        public string Servicios { get; set; }
        public string TipoServicio { get; set; }
        public string Hotel { get; set; }
        public int GuiaId { get; set; }
        public int VehiculoId { get; set; }
        public int ReservaDetalleId { get; set; }
        public string Observaciones { get; set; }
        public DateTime FechaIn { get; set; }



    }
}
