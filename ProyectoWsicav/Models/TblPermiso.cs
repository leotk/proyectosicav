﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Permiso")]
    public partial class TblPermiso
    {
        public TblPermiso()
        {
            TblUsuario = new HashSet<TblUsuario>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Descripcion { get; set; }

        [InverseProperty("Permiso")]
        public virtual ICollection<TblUsuario> TblUsuario { get; set; }
    }
}
