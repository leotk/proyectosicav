﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Destino")]
    public partial class TblDestino
    {
        public TblDestino()
        {
            TblSolicitud = new HashSet<TblSolicitud>();
        }

        [Key]
        public int Id { get; set; }
        public int CiudadId { get; set; }
        [StringLength(100)]
        public string Nombre { get; set; }

        [ForeignKey(nameof(CiudadId))]
        [InverseProperty(nameof(TblCiudad.TblDestino))]
        public virtual TblCiudad Ciudad { get; set; }
        [InverseProperty("Destino")]
        public virtual ICollection<TblSolicitud> TblSolicitud { get; set; }
    }
}
