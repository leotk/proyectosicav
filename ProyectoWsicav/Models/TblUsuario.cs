﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Usuario")]
    public partial class TblUsuario
    {
        public TblUsuario()
        {
            TblSolicitud = new HashSet<TblSolicitud>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        [Column("nombre")]
        [StringLength(50)]
        public string Nombre { get; set; }
        [Required]
        [Column("apellido")]
        [StringLength(50)]
        public string Apellido { get; set; }
        [Required]
        [Column("password")]
        [StringLength(50)]
        public string Password { get; set; }
        [Required]
        //[Column("supervisor")] No se si vaya
        //public bool Supervisor { get; set; }
        [Column("email")]
        [StringLength(50)]
        public string Email { get; set; }
        [Column("usuario")]
        [StringLength(50)]
        public string Usuario { get; set; }
        [Column("codigoDepartamento")]
        [StringLength(3)]
        public string CodigoDepartamento { get; set; }
        [Column("permisoId")]
        public int? PermisoId { get; set; }

        [ForeignKey(nameof(PermisoId))]
        [InverseProperty(nameof(TblPermiso.TblUsuario))]
        public virtual TblPermiso Permiso { get; set; }
        [InverseProperty("Usuario")]
        public virtual ICollection<TblSolicitud> TblSolicitud { get; set; }
    }
}
