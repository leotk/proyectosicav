﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Solicitud")]
    public partial class TblSolicitud
    {
        public TblSolicitud()
        {
            //TblDetallePasajeros = new HashSet<TblDetallePasajeros>();
            TblEncuesta = new HashSet<TblEncuesta>();
            TblLiquidacion = new HashSet<TblLiquidacion>();
            TblCotizacion = new HashSet<TblCotizacion>();
            TblFactura = new HashSet<TblFactura>();
            TblLiquiCliente = new HashSet<TblLiquiCliente>();
            TblLiquiProve = new HashSet<TblLiquiProve>();
            TblReservasDetalle = new HashSet<TblReservasDetalle>();
        }

        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string NumeroSolicitud { get; set; }
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaSolicitud { get; set; }
        public int? UsuarioId { get; set; }
        public int? DestinoId { get; set; }
        [Column("Boleto_aereo")]
        public bool BoletoAereo { get; set; }
        public bool Hotel { get; set; }
        public bool Traslado { get; set; }
        public bool Excursion { get; set; }
        public bool Guias { get; set; }
        [Column("Seguro_viajes")]
        public bool SeguroViajes { get; set; }
        [Column("Alquiler_carro")]
        public bool AlquilerCarro { get; set; }
        //[Required(ErrorMessage = "*La fecha es requerida")]
        [Column("FechaIN", TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaIn { get; set; }
        //[Required(ErrorMessage = "*La fecha es requerida")]
        [Column("FechaOUT", TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaOut { get; set; }
        [Column("Hora_vuelo_ida")]
        [StringLength(10)]
        public string HoraVueloIda { get; set; }
        [Column("Hora_vuelo_regreso")]
        [StringLength(10)]
        public string HoraVueloRegreso { get; set; }
        [Column("Persona_contacto")]
        [StringLength(100)]
        public string PersonaContacto { get; set; }
        [Column("Telefono_contacto")]
        [StringLength(50)]
        public string TelefonoContacto { get; set; }
        [Column("Correo_contacto")]
        [StringLength(50)]
        public string CorreoContacto { get; set; }
        public int? ClienteId { get; set; }
        public int? EstatusId { get; set; }
        public string Descripcion { get; set; }
        public int? VendedorId { get; set; }
        public int Cntadulto { get; set; }
        public int Cntnino { get; set; }
        public int Cntinfa { get; set; }
        [StringLength(50)]
        public string Idioma { get; set; }
        [StringLength(50)]
        public string Solicitadode { get; set; }
        [StringLength(50)]
        public string Mediollegada { get; set; }
        [StringLength(50)]
        public string Nomfile { get; set; }
        public int? Estadofile { get; set; }
        public bool? Cancelaestado { get; set; }
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? Fechacancelacion { get; set; }
        public bool? Reaperturaestado { get; set; }

        [ForeignKey(nameof(ClienteId))]
        [InverseProperty(nameof(TblCliente.TblSolicitud))]
        public virtual TblCliente Cliente { get; set; }
        [ForeignKey(nameof(DestinoId))]
        [InverseProperty(nameof(TblDestino.TblSolicitud))]
        public virtual TblDestino Destino { get; set; }
        [ForeignKey(nameof(EstatusId))]
        [InverseProperty(nameof(TblEstatus.TblSolicitud))]
        public virtual TblEstatus Estatus { get; set; }
        [ForeignKey(nameof(UsuarioId))]
        [InverseProperty(nameof(TblUsuario.TblSolicitud))]
        public virtual TblUsuario Usuario { get; set; }
        [ForeignKey(nameof(VendedorId))]
        [InverseProperty(nameof(TblVendedor.TblSolicitud))]
        public virtual TblVendedor Vendedor { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<TblCotizacion> TblCotizacion { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<TblFactura> TblFactura { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<TblLiquiCliente> TblLiquiCliente { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<TblLiquiProve> TblLiquiProve { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<TblReservasDetalle> TblReservasDetalle { get; set; }
        //[InverseProperty("Solicitud")]
        //public virtual ICollection<TblDetallePasajeros> TblDetallePasajeros { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<TblEncuesta> TblEncuesta { get; set; }
        [InverseProperty("Solicitud")]
        public virtual ICollection<TblLiquidacion> TblLiquidacion { get; set; }
    }
}
