﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Ciudad")]
    public partial class TblCiudad
    {
        public TblCiudad()
        {
            TblDestino = new HashSet<TblDestino>();
        }

        [Key]
        public int Id { get; set; }
        [Column("Nombre_ciudad")]
        public string NombreCiudad { get; set; }

        [InverseProperty("Ciudad")]
        public virtual ICollection<TblDestino> TblDestino { get; set; }
    }
}
