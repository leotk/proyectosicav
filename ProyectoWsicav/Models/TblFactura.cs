﻿using ProyectoWsicav.ViewObject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProyectoWsicav.Models
{
    [Table("Tbl_Factura")]
    public partial class TblFactura
    {
        public TblFactura()
        {
        }
        public TblFactura(VFactura fac) //Para guardar
        {
            SolicitudId = fac.SolicitudId;
            FechaFactu = fac.Fecha;
            NumeFactu = fac.NumeFactu;
            Monto = fac.Monto;
        }

        [Key]
        public int Id { get; set; }
        public int SolicitudId { get; set; }
        [Column(TypeName = "datetime"), DataType(DataType.Date)]
        public DateTime? FechaFactu { get; set; }
        [StringLength(20)]
        public string NumeFactu { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal? Monto { get; set; }
        [StringLength(50)]
        public string Documentonumero { get; set; }
        [StringLength(50)]
        public string Documentonombre { get; set; }
        public byte[] Documento { get; set; }

        [ForeignKey(nameof(SolicitudId))]
        [InverseProperty(nameof(TblSolicitud.TblFactura))]
        public virtual TblSolicitud Solicitud { get; set; }
    }
}
