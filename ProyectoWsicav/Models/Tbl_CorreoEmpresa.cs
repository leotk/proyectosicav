﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoWsicav.Models
{
    public partial class Tbl_CorreoEmpresa
    {
        public Tbl_CorreoEmpresa()
        {
            CorreoEmpresa = new HashSet<Tbl_CorreoCampana>();
        }
        [Key]
        [Column("id")]
        public int Id { get; set; }
        public string Correo { get; set; }
        public string Password { get; set; }
        public string SmptHost { get; set; }
        public string SmptPort { get; set; }

        public virtual ICollection<Tbl_CorreoCampana> CorreoEmpresa { get; set; }
    }
}
