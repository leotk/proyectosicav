﻿using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Controllers;
using ProyectoWsicav.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoWsicav.Demo
{


    public interface IDemoService
    {
        void RunTask();
    }
    public class DemoService : IDemoService
    {
        UsersContext Context = new UsersContext();


        public void RunTask()
        {

            var listaCampana = Context.Tbl_CorreoCampana.Where(a => a.Activo == true).ToList();
            foreach (var i in listaCampana)
            {
                DateTime FechaActual = DateTime.Now;
                if (i.Hasta < FechaActual)
                {
                    var Campana = Context.Tbl_CorreoCampana.Find(i.Id);
                    Campana.Activo = false;
                    Context.Entry(Campana).State = EntityState.Modified;
                    Context.SaveChanges();
                }
                else
                {

                    int Year = DateTime.Now.Year;
                    int Mes = DateTime.Now.Month;
                    int dia = DateTime.Now.Day;
                    int hora = 0;
                    int minutos = 0;

                    string cadenaHora = i.Hora1;
                    string[] horaSeparada = cadenaHora.Split(':');
                    //----- array para separar la hora
                    int Array = 0;
                    foreach (var x in horaSeparada)
                    {
                        if (Array == 0)
                        {
                            hora = Convert.ToInt32(x);
                        }
                        if (Array == 1)
                        {
                            minutos = Convert.ToInt32(x);
                        }

                        Array = Array + 1;
                    }



                    int DiaSemana = Convert.ToInt32(DateTime.Now.DayOfWeek);

                    var listaCorreoCampana = Context.viw_ClientePerteneceCampana.Where(a => a.CampanaID == i.Id).ToList();

                    var date = new DateTime(Year, Mes, dia, hora, minutos, 0);

                    int NoSePuede = 1;

                    if (DiaSemana == 1 && i.Lunes == false)
                    {
                        NoSePuede = 0;
                    }
                    if (DiaSemana == 2 && i.Martes == false)
                    {
                        NoSePuede = 0;
                    }
                    if (DiaSemana == 3 && i.Miercoles == false)
                    {
                        NoSePuede = 0;
                    }
                    if (DiaSemana == 4 && i.Jueves == false)
                    {
                        NoSePuede = 0; ;
                    }
                    if (DiaSemana == 5 && i.Viernes == false)
                    {
                        NoSePuede = 0;
                    }

                    if (DiaSemana == 6 && i.Sabado == false)
                    {
                        NoSePuede = 0;
                    }
                    if (DiaSemana == 7 && i.Domingo == false)
                    {
                        NoSePuede = 0;
                    }
                    if (NoSePuede == 1)
                    {
                        DateTime date2 = date.AddMinutes(1);

                        if (date <= FechaActual && date2 >= FechaActual)
                        {

                            foreach (var o in listaCorreoCampana)
                            {
                                string Para = o.Email.Trim();
                                // o.TblCliente.e.Replace("/", ",");

                                string Mensaje = Context.Tbl_CorreoPlantillas.Where(a => a.Id == i.CorreoPlantillaId).Select(a => a.CodigoHTML).FirstOrDefault();
                                if (Para == null)
                                {

                                    Para = "SinCorreoCliente321@hotmail.com";

                                }

                                int CorreoEmpresa = i.CorreoEmpresaId;
                                // string NombreReporte = _context.Tbl_FormatoImpresoFactura.Where(a => a.codigo == "COT").Select(a => a.reporte).FirstOrDefault();
                                // HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + Url.Action("ExportReport", "CuentaPorCobrar", new { id = id, NombreReporte = NombreReporte });
                                var CorreoEmpres = Context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.Correo).FirstOrDefault();
                                var CorreoPass = Context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.Password).FirstOrDefault();
                                int Port = Convert.ToInt32(Context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.SmptPort).FirstOrDefault());

                                SmtpClient client = new SmtpClient();
                                string puerto = Context.Tbl_CorreoEmpresa.Where(a => a.Id == i.CorreoEmpresaId).Select(a => a.SmptPort.Trim()).FirstOrDefault();
                                client.Port = Convert.ToInt32(puerto);// puerto de gmail para enviar los correos
                                                                      // utilizamos el servidor SMTP de gmail
                                client.Host = Context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.SmptHost.Trim()).FirstOrDefault();//Host de gmail para enviar correos
                                client.EnableSsl = true;
                                //client.Timeout = 10000;
                                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                client.UseDefaultCredentials = true;

                                // nos autenticamos con nuestra cuenta de gmail
                                client.Credentials = new NetworkCredential(CorreoEmpres.Trim(), CorreoPass.Trim());//Correo,Constraseña
                                                                                                                   //colocamos los valores (Correo,Para,Asunto,Mensaje)

                                string Asunto = i.Asunto;
                                MailMessage mail = new MailMessage(CorreoEmpres.Trim(), Para.Trim(), Asunto.Trim(), Mensaje.Trim());// + " <br /> <br /> <br /> <br /> Este correo es enviado gracias a <a href='www.bsc.pe'>PeruSicav - @bscsistemas</a>");
                                mail.BodyEncoding = UTF8Encoding.UTF8;
                                mail.IsBodyHtml = true;//--- para que el mensaje pueda leer una plantilla html <br/>, tablas ,etc
                                mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                                // mail.Attachments.Add(data);//Aqui adjunta al correo los archivos
                                //mail.Attachments.Add(data2);//Aqui adjunta al correo los archivos
                                client.Send(mail);//Listo y enviado
                            }
                        }

                    }

                }



            }

        }


    }
}
