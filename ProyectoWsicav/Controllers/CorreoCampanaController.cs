﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Demo;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class CorreoCampanaController : Controller

    {
        private readonly UsersContext _context;

        public CorreoCampanaController(UsersContext context)
        {
            _context = context;
        }
      
        public ActionResult SendEmail()
        {

            var listaCampana =  _context.Tbl_CorreoCampana.Where(a => a.Activo == true).ToList();
            foreach (var i in listaCampana)
            {
                DateTime FechaActual = DateTime.Now;
                if (i.Hasta < FechaActual)
                {
                    var Campana = _context.Tbl_CorreoCampana.Find(i.Id);
                    Campana.Activo = false;
                    _context.Entry(Campana).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {

                    int Year = DateTime.Now.Year;
                    int Mes = DateTime.Now.Month;
                    int dia = DateTime.Now.Day;
                    int hora = 0;
                    int minutos = 0;

                    string cadenaHora = i.Hora1;
                    string[] horaSeparada = cadenaHora.Split(':');
                    //----- array para separar la hora
                    int Array = 0;
                    foreach (var x in horaSeparada)
                    {
                        if (Array == 0)
                        {
                            hora = Convert.ToInt32(x);
                        }
                        if (Array == 1)
                        {
                            minutos = Convert.ToInt32(x);
                        }

                        Array = Array + 1;
                    }



                    int DiaSemana = Convert.ToInt32(DateTime.Now.DayOfWeek);

                    var listaCorreoCampana =  _context.Tbs_CorreoClienteCampanaPertenece.Where(a => a.CampanaID == i.Id).ToList();

                    var date = new DateTime(Year, Mes, dia, hora, minutos, 0);

                    if (DiaSemana == 1 && i.Lunes == false)
                    {
                        return Json(0);
                    }
                    if (DiaSemana == 2 && i.Martes == false)
                    {
                        return Json(0);
                    }
                    if (DiaSemana == 3 && i.Miercoles == false)
                    {
                        return Json(0);
                    }
                    if (DiaSemana == 4 && i.Jueves == false)
                    {
                        return Json(0);
                    }
                    if (DiaSemana == 5 && i.Viernes == false)
                    {
                        return Json(0);
                    }

                    if (DiaSemana == 6 && i.Sabado == false)
                    {
                        return Json(0);
                    }
                    if (DiaSemana == 7 && i.Domingo == false)
                    {
                        return Json(0);
                    }

                    DateTime date2 = date.AddMinutes(1);

                    if (date <= FechaActual && date2 >= FechaActual)
                    {

                        foreach (var o in listaCorreoCampana)
                        {
                            string Para = "";
                            // o.TblCliente.e.Replace("/", ",");

                            string Mensaje =  _context.Tbl_CorreoPlantillas.Where(a => a.Id == i.CorreoPlantillaId).Select(a => a.CodigoHTML).FirstOrDefault();
                            if (Para == null)
                            {

                                Para = "SinCorreoCliente321@hotmail.com";

                            }

                            int CorreoEmpresa = i.CorreoEmpresaId;
                            // string NombreReporte = _context.Tbl_FormatoImpresoFactura.Where(a => a.codigo == "COT").Select(a => a.reporte).FirstOrDefault();
                            // HttpContext.Request.Url.Scheme + "://" + HttpContext.Request.Url.Authority + Url.Action("ExportReport", "CuentaPorCobrar", new { id = id, NombreReporte = NombreReporte });
                            var CorreoEmpres = _context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.Correo).FirstOrDefault();
                            var CorreoPass = _context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.Password).FirstOrDefault();
                            int Port = Convert.ToInt32(_context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.SmptPort).FirstOrDefault());

                            SmtpClient client = new SmtpClient();
                            string puerto = _context.Tbl_CorreoEmpresa.Where(a => a.Id == i.CorreoEmpresaId).Select(a => a.SmptPort.Trim()).FirstOrDefault();
                            client.Port = Convert.ToInt32(puerto);// puerto de gmail para enviar los correos
                            // utilizamos el servidor SMTP de gmail
                            client.Host = _context.Tbl_CorreoEmpresa.Where(a => a.Id == CorreoEmpresa).Select(a => a.SmptHost.Trim()).FirstOrDefault();//Host de gmail para enviar correos
                            client.EnableSsl = true;
                            //client.Timeout = 10000;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = true;

                            // nos autenticamos con nuestra cuenta de gmail
                            client.Credentials = new NetworkCredential(CorreoEmpres.Trim(), CorreoPass.Trim());//Correo,Constraseña
                            //colocamos los valores (Correo,Para,Asunto,Mensaje)

                            string Asunto = i.Asunto;
                            MailMessage mail = new MailMessage(CorreoEmpres.Trim(), Para.Trim(), Asunto.Trim(), Mensaje.Trim());// + " <br /> <br /> <br /> <br /> Este correo es enviado gracias a <a href='www.bsc.pe'>PeruSicav - @bscsistemas</a>");
                            mail.BodyEncoding = UTF8Encoding.UTF8;
                            mail.IsBodyHtml = true;//--- para que el mensaje pueda leer una plantilla html <br/>, tablas ,etc
                            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                            // mail.Attachments.Add(data);//Aqui adjunta al correo los archivos
                            //mail.Attachments.Add(data2);//Aqui adjunta al correo los archivos
                            client.Send(mail);//Listo y enviado
                        }
                    }

                }





            }
            return Json(0);
        }

        public async Task<IActionResult> Edit(int id, string codigoTipoCliente, string Cliente2,string codigoTipoCliente2, string Cliente)
        {
            var db = _context;
            var tbl_CorreoCampana = await _context.Tbl_CorreoCampana.FindAsync(id);
            List<TblCliente> lista = new List<TblCliente>();
            lista = await _context.TblCliente.Where(a =>a.TipoClienteId.ToString().Contains(codigoTipoCliente) &&a.Nombre.Contains(Cliente2)).OrderBy(a => a.Nombre).ToListAsync();

            if (tbl_CorreoCampana == null)
            {
                return NotFound();
            }
            if (codigoTipoCliente == null && Cliente2 != null)
            {
                lista = await _context.TblCliente.Where(a => a.Nombre.Contains(Cliente2)).OrderBy(a => a.Nombre).ToListAsync();

            }
            if (codigoTipoCliente == null && Cliente2 == null)
            {
                 lista = await _context.TblCliente.ToListAsync();

            }
            //if (Cliente == null)
            //{
            //    Cliente = " ";
            //}
            //--------- Listas
            // List<TblDestino> Destinos = await _context.TblDestino.Include("Ciudad").ToListAsync();
            ViewBag.Clientes = lista;

            List<viw_ClientePerteneceCampana> listaClientesCargados = new List<viw_ClientePerteneceCampana>();
            listaClientesCargados = await _context.viw_ClientePerteneceCampana.Where(a => a.CampanaID == id).ToListAsync();
            ViewBag.ListaClienteCargados = listaClientesCargados;

            //----------------------  List<viw_ClientePerteneceCampana> listaClientesCargados = new List<viw_ClientePerteneceCampana>();
            listaClientesCargados = await _context.viw_ClientePerteneceCampana.Where(a => a.CampanaID == id).ToListAsync();
           
            if (Cliente != null)
            {
                listaClientesCargados = await _context.viw_ClientePerteneceCampana.Where(a => a.CampanaID == id && a.nombre.Contains(Cliente)).OrderBy(a => a.nombre).ToListAsync();

            }

            ViewBag.ListaClienteCargados = listaClientesCargados;
            ViewData["Plantilla"] = new SelectList(_context.Tbl_CorreoPlantillas, "Id", "NombrePlantilla");
            ViewData["CorreoE"] = new SelectList(_context.Tbl_CorreoEmpresa, "Id", "Correo");

            //ViewBag.codigoTipoCliente3 = new SelectList(db.TblTipoCliente.OrderBy(a => a.Descripcion), "Id", "Descripcion");
            //ViewBag.Plantilla = new SelectList(db.Tbl_CorreoPlantillas.OrderBy(a => a.NombrePlantilla), "Id", "NombrePlantilla");
            //ViewBag.ClienteProvieneID = new SelectList(db.Tbl_ClienteProviene.OrderBy(a => a.Nombre), "Id", "Nombre");

            //ViewBag.codigoTipoCliente = new SelectList(db.TblTipoCliente.OrderBy(a => a.Descripcion), "Id", "Descripcion");
            //ViewBag.CorreoEmpresa = new SelectList(db.Tbl_CorreoEmpresa.OrderBy(a => a.Correo), "Id", "Correo");
            //// ViewBag.CorreoEmpresaId = new SelectList(db.Tbl_CorreoEmpresa, "id", "Correo", tbl_CorreoCampana.CorreoEmpresaId);
            //ViewBag.CorreoPlantillaId = new SelectList(db.Tbl_CorreoPlantillas, "id", "NombrePlantilla", tbl_CorreoCampana.CorreoPlantillaId);
            return View(tbl_CorreoCampana);
        }

        // POST: CorreoCampana/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Tbl_CorreoCampana tbl_CorreoCampana,string codigoTipoCliente2, string ClienteProvieneID, string[] ids, string codigoTipoCliente3, string submitButton, string Cliente2, string codigoTipoCliente, string Cliente, string ClienteCargado, string Rif, string Correo, string Nombre, string Direccion, string Telefono)
        {
            var db = _context;
            ViewBag.codigoTipoCliente2 = new SelectList(db.TblTipoCliente.OrderBy(a => a.Descripcion), "Id", "Descripcion");

            //ViewBag.codigoTipoCliente3 = new SelectList(db.TblTipoCliente.OrderBy(a => a.Descripcion), "Id", "Descripcion");
            //ViewBag.Plantilla = new SelectList(db.Tbl_CorreoPlantillas.OrderBy(a => a.NombrePlantilla), "Id", "NombrePlantilla");
            //ViewBag.ClienteProvieneID = new SelectList(db.Tbl_ClienteProviene.OrderBy(a => a.Nombre), "Id", "Nombre");
            //ViewBag.Plantilla = new SelectList(db.Tbl_CorreoPlantillas.OrderBy(a => a.NombrePlantilla), "Id", "NombrePlantilla");
            //ViewBag.codigoTipoCliente = new SelectList(db.TblTipoCliente.OrderBy(a => a.Descripcion), "Id", "Descripcion");
            //ViewBag.CorreoEmpresa = new SelectList(db.Tbl_CorreoEmpresa.OrderBy(a => a.Correo), "Id", "Correo");
            //// ViewBag.CorreoEmpresaId = new SelectList(db.Tbl_CorreoEmpresa, "id", "Correo", tbl_CorreoCampana.CorreoEmpresaId);
            //ViewBag.CorreoPlantillaId = new SelectList(db.Tbl_CorreoPlantillas, "id", "NombrePlantilla", tbl_CorreoCampana.CorreoPlantillaId);
   
         
            switch (submitButton)
            {
                case "Crear/Clientes":


                    tbl_CorreoCampana = db.Tbl_CorreoCampana.Find(tbl_CorreoCampana.Id);
                    if (Correo == null || Correo == "")
                    {
                        ViewBag.Tab = "3";
                        ViewBag.MensajeCrearCliente = "Por favor llene los datos sugeridos";
                        //return View(tbl_CorreoCampana);
                        return View( tbl_CorreoCampana);
                    }

                    if (Nombre == null || Nombre == "")
                    {
                        ViewBag.Tab = "3";
                        ViewBag.MensajeCrearCliente = "Por favor llene los datos sugeridos";
                        //return View(tbl_CorreoCampana);
                        return View( tbl_CorreoCampana);
                    }

                    if (Direccion == null)
                    {

                        Direccion = "";
                    }
                    if (Telefono == null)
                    {

                        Telefono = "";
                    }
                    if (Rif == null)
                    {

                        Rif = "00000";
                    }
                    TblCliente cliente = new TblCliente();
                    var listacliente = db.TblCliente.ToList();
                    int codigoCliente = listacliente.Count() + 1;
                    string codigoComprueba = codigoCliente.ToString();
                    var codigoExiste = db.TblCliente.Where(a => a.Codigo.Trim() == codigoComprueba).Select(a => a.Codigo).FirstOrDefault();
                    if (codigoExiste == null)
                    {
                        cliente.Codigo = codigoComprueba.Trim();
                    }
                    else
                    {
                        cliente.Codigo = (codigoCliente + 2).ToString().Trim();
                    }
                    cliente.Direccion = Direccion.Trim();
                    cliente.Email = Correo.Trim();
                    cliente.Nombre = Nombre.Trim();
                    cliente.Telefono = Telefono;
                    cliente.Celular = Telefono;

                    //cliente.ClienteProvieneID = Convert.ToInt32(ClienteProvieneID);

                    db.TblCliente.Add(cliente);
                    db.SaveChanges();
                    var ClienteCamp2 = new Tbs_CorreoClienteCampanaPertenece();
                    ClienteCamp2.ClienteID = cliente.Id;
                    ClienteCamp2.CampanaID = tbl_CorreoCampana.Id;

                    db.Tbs_CorreoClienteCampanaPertenece.Add(ClienteCamp2);
                    db.SaveChanges();
                    ViewBag.Mensaje = "Cliente " + Nombre + " ha sido creado y agregado a la campaña";

                    //return View(tbl_CorreoCampana);
                    return View("~/Views/CorreoCampana/Edit.cshtml", tbl_CorreoCampana);
                case "Cargar/Clientes":
                    int[] id = null;

                    if (ids != null)
                    {
                        id = new int[ids.Length];
                        int j = 0;

                        foreach (string i in ids)
                        {

                            int.TryParse(i, out id[j++]);
                        }
                    }

                    if (id != null && id.Length > 0)
                    {

                        var allselected = db.TblCliente.Where(a => ids.Contains(a.Id.ToString())).ToList();

                        foreach (var i in allselected)
                        {
                            int ClienteExiste = db.Tbs_CorreoClienteCampanaPertenece.Where(a => a.CampanaID == tbl_CorreoCampana.Id && a.ClienteID == i.Id).Select(a => a.ClienteID).FirstOrDefault();
                            if (ClienteExiste == null || ClienteExiste == 0)
                            {
                                var ClienteCamp = new Tbs_CorreoClienteCampanaPertenece();
                                ClienteCamp.ClienteID = i.Id;
                                ClienteCamp.CampanaID = tbl_CorreoCampana.Id;

                                db.Tbs_CorreoClienteCampanaPertenece.Add(ClienteCamp);
                                db.SaveChanges();
                            }
                        }
                    }
                    return RedirectToAction("Edit", new { id = tbl_CorreoCampana.Id });
                case "Buscar":
                    tbl_CorreoCampana = db.Tbl_CorreoCampana.Find(tbl_CorreoCampana.Id);
                    ViewBag.Tab = "2";

                    //return View(tbl_CorreoCampana);
                    return RedirectToAction("Edit", new { id = tbl_CorreoCampana.Id, codigoTipoCliente = codigoTipoCliente, Cliente2 = Cliente2 });
                case "Buscar/Cargados":
                    tbl_CorreoCampana = db.Tbl_CorreoCampana.Find(tbl_CorreoCampana.Id);

                    //return View(tbl_CorreoCampana);
                    return RedirectToAction("Edit",new { codigoTipoCliente2 = codigoTipoCliente2, Cliente = Cliente });
                

                default:
                    if (tbl_CorreoCampana.Hora2 == null)
                    {
                        tbl_CorreoCampana.Hora2 = " ";
                    }
                    if (tbl_CorreoCampana.Hora3 == null)
                    {
                        tbl_CorreoCampana.Hora3 = " ";
                    }


                    if (ModelState.IsValid)
                    {
                        try
                        {
                            _context.Update(tbl_CorreoCampana);
                            await _context.SaveChangesAsync();
                        }
                        catch (DbUpdateConcurrencyException)
                        {
                            if (!Tbl_CorreoCampanaExists(tbl_CorreoCampana.Id))
                            {
                                return NotFound();
                            }
                            else
                            {
                                throw;
                            }
                        }
                        return RedirectToAction("Edit", new { id = tbl_CorreoCampana.Id });
                    }
                    //return View(tbl_CorreoCampana);
                    return View("~/Views/CorreoCampana/Edit.cshtml", tbl_CorreoCampana);
            }
        }


        // GET: CorreoCampana
        public async Task<IActionResult> Index()
        {
            Tbl_CorreoCampana campa = new Tbl_CorreoCampana();

            ViewBag.Campa = await _context.Tbl_CorreoCampana
                .Include("CorreoPlantilla")
                .Include("CorreoEmpresa")
                .ToListAsync();

            ViewData["Plantilla"] = new SelectList(_context.Tbl_CorreoPlantillas, "Id", "NombrePlantilla");
            ViewData["CorreoE"] = new SelectList(_context.Tbl_CorreoEmpresa, "Id", "Correo");
            
            return View(campa);
        }

        // GET: CorreoCampana/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbl_CorreoCampana = await _context.Tbl_CorreoCampana
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbl_CorreoCampana == null)
            {
                return NotFound();
            }

            return View(tbl_CorreoCampana);
        }

        // GET: CorreoCampana/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CorreoCampana/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Asunto,Desde,Hasta,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo,CorreoPlantillaId,CorreoEmpresaId,Hora1,Hora2,Hora3,Activo")] Tbl_CorreoCampana tbl_CorreoCampana)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tbl_CorreoCampana);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tbl_CorreoCampana);
        }

        // GET: CorreoCampana/Edit/5
        public async Task<IActionResult> Edit2(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbl_CorreoCampana = await _context.Tbl_CorreoCampana.FindAsync(id);
            if (tbl_CorreoCampana == null)
            {
                return NotFound();
            }
            return View(tbl_CorreoCampana);
        }

        // POST: CorreoCampana/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit2(int id, [Bind("Id,Asunto,Desde,Hasta,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo,CorreoPlantillaId,CorreoEmpresaId,Hora1,Hora2,Hora3,Activo")] Tbl_CorreoCampana tbl_CorreoCampana)
        {
            if (id != tbl_CorreoCampana.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tbl_CorreoCampana);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Tbl_CorreoCampanaExists(tbl_CorreoCampana.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tbl_CorreoCampana);
        }

        // GET: CorreoCampana/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tbl_CorreoCampana = await _context.Tbl_CorreoCampana
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tbl_CorreoCampana == null)
            {
                return NotFound();
            }

            return View(tbl_CorreoCampana);
        }
        public async Task<IActionResult> Eliminar(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var _Cliente = await _context.Tbs_CorreoClienteCampanaPertenece.FindAsync(id);
            var tbl_CorreoCampana = await _context.Tbl_CorreoCampana
                .FirstOrDefaultAsync(m => m.Id == _Cliente.CampanaID);
            if (tbl_CorreoCampana == null)
            {
                return NotFound();
            }

            

            if (_Cliente == null)
            {
                return RedirectToAction("Edit", new { id = tbl_CorreoCampana.Id });
            }
            else
            {
                _context.Tbs_CorreoClienteCampanaPertenece.Remove(_Cliente);
                _context.SaveChanges();
                return RedirectToAction("Edit", new { id = tbl_CorreoCampana.Id });
            }
        }
        public async Task<IActionResult> EliminarCliente(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var _Cliente1 = await _context.Tbs_CorreoClienteCampanaPertenece.FindAsync(id);
            var tbl_CorreoCampana = await _context.Tbl_CorreoCampana
                .FirstOrDefaultAsync(m => m.Id == _Cliente1.CampanaID);
            if (tbl_CorreoCampana == null)
            {
                return NotFound();
            }

            var _Cliente = await _context.TblCliente.FindAsync(id);

            if (_Cliente == null)
            {
                return RedirectToAction("Edit", new { id = tbl_CorreoCampana.Id });

            }
            else
            {
                _context.TblCliente.Remove(_Cliente);
                _context.SaveChanges();
                return RedirectToAction("Edit", new { id = tbl_CorreoCampana.Id });
            }            
        }
        // POST: CorreoCampana/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tbl_CorreoCampana = await _context.Tbl_CorreoCampana.FindAsync(id);
            _context.Tbl_CorreoCampana.Remove(tbl_CorreoCampana);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool Tbl_CorreoCampanaExists(int id)
        {
            return _context.Tbl_CorreoCampana.Any(e => e.Id == id);
        }
    }
}
