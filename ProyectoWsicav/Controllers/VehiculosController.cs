
using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class VehiculosController : Controller
    {
        UsersContext ctx;

        public VehiculosController(UsersContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<IActionResult> Index()
        {
            List<TblVehiculo> Vehiculos = await ctx.TblVehiculo.Include("TipoVehiculo").ToListAsync();
            TblVehiculo Vehi = new TblVehiculo();
            ViewBag.Vehiculo = Vehiculos;
            ViewBag.TblTipoVehiculo = await ctx.TblTipoVehiculo.OrderBy(x => x.TipoVehiculo).ToListAsync();

            return View(Vehi);
        }

        [BindProperty]
        public TblVehiculo Vehiculo { get; set; }
        public async Task<IActionResult> GuardarVehiculo()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var _Vehiculo = await ctx.TblVehiculo.Where(x => x.Id == Vehiculo.Id).AnyAsync();

            if (!_Vehiculo)
            {
                ctx.TblVehiculo.Add(Vehiculo);
            }
            else
            {
                ctx.TblVehiculo.Attach(Vehiculo);
                ctx.Entry(Vehiculo).State = EntityState.Modified;
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");

        }


        public async Task<IActionResult> Modificar(int id)
        {
            var Vehiculo = await ctx.TblVehiculo.FindAsync(id);

            if (Vehiculo == null)
            {
                return RedirectToAction("Index");
            }


            ViewBag.TblTipoVehiculo = await ctx.TblTipoVehiculo.OrderBy(x => x.TipoVehiculo).ToListAsync();

            ViewBag.Vehiculo = await ctx.TblVehiculo.Include("TipoVehiculo").ToListAsync();



            return View(Vehiculo);

        }

        public async Task<IActionResult> Eliminar(int id)
        {
            var _Vehiculo = await ctx.TblVehiculo.FindAsync(id);

            if (_Vehiculo == null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ctx.TblVehiculo.Remove(_Vehiculo);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}