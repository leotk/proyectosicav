﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProyectoWsicav.Demo;

namespace ProyectoWsicav.Controllers
{
    public class HangFireController : Controller
    {
        private IDemoService demoService;
        //private CorreoCampanaController obj;

        // GET: HangFire

        public HangFireController(IDemoService demoService)
        {
            this.demoService = demoService;
        }
        [HttpGet]
        public ActionResult Index()
        {

           // BackgroundJob.Enqueue(() => demoService.RunTask());
            RecurringJob.AddOrUpdate(() =>demoService.RunTask(), Cron.Minutely);
            return Ok();
        }


        // GET: HangFire/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: HangFire/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HangFire/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HangFire/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HangFire/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HangFire/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HangFire/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}