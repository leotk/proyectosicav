﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class UsuariosController : Controller
    {
        UsersContext ctx;

        public UsuariosController(UsersContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<IActionResult> Index()
        {
            //List<TblUsuario> Usuarios = await ctx.TblUsuario.Include("Permiso").ToListAsync();
            List<TblUsuario> Usuarios = await ctx.TblUsuario.ToListAsync();
            TblUsuario Usua = new TblUsuario();
            ViewBag.Usuario = Usuarios;

            ViewBag.TblPermiso = await ctx.TblPermiso.OrderBy(x => x.Descripcion).ToListAsync();

            return View(Usua);
        }

        [BindProperty]
        public TblUsuario Usuari { get; set; }
        public async Task<IActionResult> GuardarUsuario()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var _Usuari = await ctx.TblUsuario.Where(x => x.Id == Usuari.Id).AnyAsync();

            if (!_Usuari)
            {
                ctx.TblUsuario.Add(Usuari);
            }
            else
            {
                ctx.TblUsuario.Attach(Usuari);
                ctx.Entry(Usuari).State = EntityState.Modified;
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        public async Task<IActionResult> Modificar(int id)
        {
            var Usuari = await ctx.TblUsuario.FindAsync(id);

            if (Usuari == null)
            {
                return RedirectToAction("Index");
            }


            ViewBag.TblPermiso = await ctx.TblPermiso.OrderBy(x => x.Descripcion).ToListAsync();

            //ViewBag.Usuario = await ctx.TblUsuario.Include("Permiso").ToListAsync();
            ViewBag.Usuario = await ctx.TblUsuario.ToListAsync();


            return View(Usuari);

        }

        public async Task<IActionResult> Eliminar(int id)
        {
            var _Usuari = await ctx.TblUsuario.FindAsync(id);

            if (_Usuari == null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ctx.TblUsuario.Remove(_Usuari);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}