﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class ProveedoresController : Controller
    {
        UsersContext ctx;

        public ProveedoresController(UsersContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<IActionResult> Index()
        {
            List<TblProveedor> Proveedores = await ctx.TblProveedor.Include("TipoDocumentoIdentidad").ToListAsync();
            TblProveedor Prove = new TblProveedor();
            ViewBag.Proveedor = Proveedores;
            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();

            return View(Prove);
        }

        [BindProperty]
        public TblProveedor Proveedor { get; set; }
        public async Task<IActionResult> GuardarProveedor()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var _Proveedor = await ctx.TblProveedor.Where(x => x.Id == Proveedor.Id).AnyAsync();

            if (!_Proveedor)
            {
                ctx.TblProveedor.Add(Proveedor);
            }
            else
            {
                ctx.TblProveedor.Attach(Proveedor);
                ctx.Entry(Proveedor).State = EntityState.Modified;
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        public async Task<IActionResult> Modificar(int id)
        {
            var Proveedor = await ctx.TblProveedor.FindAsync(id);

            if (Proveedor == null)
            {
                return RedirectToAction("Index");
            }


            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();

            ViewBag.Proveedor = await ctx.TblProveedor.Include("TipoDocumentoIdentidad").ToListAsync();


            return View(Proveedor);

        }

        public async Task<IActionResult> Eliminar(int id)
        {
            var _Proveedor = await ctx.TblProveedor.FindAsync(id);

            if (_Proveedor == null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ctx.TblProveedor.Remove(_Proveedor);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}