﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ProyectoWsicav.Controllers
{
    public class AccesoController : Controller
    {
        UsersContext uct = new UsersContext();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ValidarAcceso(TblUsuario user)
        {
            var _usuario = uct.TblUsuario.Where(x => x.Usuario == user.Usuario);
            if (_usuario.Any())
            {
                if (_usuario.Where(x => x.Password == user.Password).Any())
                {
                    if (HttpContext.Session.GetString("NombreUser") == null)
                    {
                        TblUsuario _nomuser = new TblUsuario();
                        _nomuser = (TblUsuario)uct.TblUsuario.Where(x => x.Usuario == user.Usuario).FirstOrDefault();
                        string nombrecompleto = _nomuser.Nombre.ToString() + " " + _nomuser.Apellido.ToString();
                //        string stato = _nomuser.Supervisor.ToString();
                        string usuar = _nomuser.Usuario.ToString();
                        HttpContext.Session.SetObjectAsJson("Usuario", _nomuser);
                        //HttpContext.Session.SetString("NombreUser", nombrecompleto);
                        //HttpContext.Session.SetString("stato", stato);
                        //HttpContext.Session.SetString("User", usuar);
                    }
                    TblUsuario nom = HttpContext.Session.GetObjectFromJson<TblUsuario>("Usuario");
                    string nomCom = nom.Nombre.ToString() + " " + nom.Apellido.ToString();
                    return Json(new { status = true, message = nomCom });
                }
                else
                {
                    return Json(new { status = false, message = "Contraseña Incorrecta" });
                }
            }
            else
            {
                return Json(new { status = false, message = "Usuario Incorrecto" });
            }
        }
        public void linnq(TblUsuario user)
        {
            var _usuario = uct.TblUsuario.Where(x => x.Usuario == user.Usuario);
            TblUsuario _nomuser = new TblUsuario();
            _nomuser = (TblUsuario)uct.TblUsuario.Where(x => x.Usuario == user.Usuario).FirstOrDefault();
            string nombrecompleto = _nomuser.Nombre.ToString() + " " + _nomuser.Apellido.ToString();
           // string stato = _nomuser.Supervisor.ToString();
            HttpContext.Session.SetString("NombreUser", nombrecompleto);
          //  HttpContext.Session.SetString("stato", stato);
        }

        [HttpGet]
        public IActionResult _MostrarUsuario()
        {
            //ViewBag.Huesped = HttpContext.Session.GetObjectFromJson<TblUsuario>("Huesped");
            //var humm = ViewBag.Huesped;
            //var eldd = ViewBag.Huesped;
            string usuario = HttpContext.Session.GetString("Usuario");
            TblUsuario _nomuser = new TblUsuario();
            _nomuser = (TblUsuario)uct.TblUsuario.Where(x => x.Usuario == usuario).FirstOrDefault();
            ViewBag.Huesped = HttpContext.Session.GetString("NombreUser");
            ViewBag.statu = HttpContext.Session.GetString("stato");
            return PartialView(_nomuser);
        }
    }
}