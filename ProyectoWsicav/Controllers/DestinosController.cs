﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;
using System.Web;
using System.Text;
using Newtonsoft.Json.Linq;
using ProyectoWsicav.ViewModel;
using Hangfire;
using ProyectoWsicav.Demo;

namespace ProyectoWsicav.Controllers
{
    public class DestinosController : Controller
    {
        private readonly UsersContext _context;

        private IDemoService demoService;
        //private CorreoCampanaController obj;

        // GET: HangFire

        public DestinosController(UsersContext context)
        {
            _context = context;
        }
       
        public ActionResult Test()
        {

            // BackgroundJob.Enqueue(() => demoService.RunTask());
            RecurringJob.AddOrUpdate(() => demoService.RunTask(), Cron.Minutely);
            return Json(0);
        }
      

        // GET: Destinos
        public async Task<IActionResult> Index()
        {
          
            List<VmDestino> list = new List<VmDestino>();
            var v = (from a in _context.TblDestino
                     join b in _context.TblCiudad on a.CiudadId equals b.Id
                     select new VmDestino
                     {
                         Id = a.Id,
                         Nombre = a.Nombre,
                         CiudadId = a.CiudadId,
                         NombreCiudad = b.NombreCiudad
                     });
            list = await v.ToListAsync();
            return View(list);
        }
        [HttpPost]
        public ActionResult saveuser(int id, string propertyName, string value)
        {
            var status = false;
            var message = "";

            //Update data to database 

            var user = _context.TblDestino.Find(id);

            object updateValue = value;
            bool isValid = true;

            if (propertyName == "CiudadId")
            {
                int newRoleID = 0;
                if (int.TryParse(value, out newRoleID))
                {
                    updateValue = newRoleID;
                    //Update value field
                    value = _context.TblCiudad.Where(a => a.Id == newRoleID).First().NombreCiudad;
                }
                else
                {
                    isValid = false;
                }

            }
            //else if (propertyName == "DOB")
            //{
            //    DateTime dob;
            //    if (DateTime.TryParseExact(value, "dd-MM-yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out dob))
            //    {
            //        updateValue = dob;
            //    }
            //    else
            //    {
            //        isValid = false;
            //    }
            //}

            if (user != null && isValid)
            {
                _context.Entry(user).Property(propertyName).CurrentValue = updateValue;
                _context.SaveChanges();
                status = true;
            }
            else
            {
                message = "Error!";
            }


            var response = new { value = value, status = status, message = message };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }

        public ActionResult GetUserRoles(int id)
        {
            //{'E':'Letter E','F':'Letter F','G':'Letter G', 'selected':'F'}
            int selectedRoleID = 0;
            StringBuilder sb = new StringBuilder();

            var roles = _context.TblCiudad.OrderBy(a => a.NombreCiudad).ToList();
            foreach (var item in roles)
            {
                sb.Append(string.Format("'{0}':'{1}',", item.Id, item.NombreCiudad));
            }

            selectedRoleID = _context.TblCiudad.Where(a => a.Id == id).First().Id;



            sb.Append(string.Format("'selected': '{0}'", selectedRoleID));
            return Content("{" + sb.ToString() + "}");
        }


        // GET: Destinos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblDestino = await _context.TblDestino
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblDestino == null)
            {
                return NotFound();
            }

            return View(tblDestino);
        }

        // GET: Destinos/Create
        public IActionResult Create()
        {
            ViewData["CiudadId"] = new SelectList(_context.TblCiudad, "Id", "Id");
            return View();
        }

        // POST: Destinos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,CiudadId,Nombre")] TblDestino tblDestino)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblDestino);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CiudadId"] = new SelectList(_context.TblCiudad, "Id", "Id", tblDestino.CiudadId);
            return View(tblDestino);
        }

        // GET: Destinos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblDestino = await _context.TblDestino.FindAsync(id);
            if (tblDestino == null)
            {
                return NotFound();
            }
            ViewData["CiudadId"] = new SelectList(_context.TblCiudad, "Id", "Id", tblDestino.CiudadId);
            return View(tblDestino);
        }

        // POST: Destinos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CiudadId,Nombre")] TblDestino tblDestino)
        {
            if (id != tblDestino.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblDestino);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblDestinoExists(tblDestino.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CiudadId"] = new SelectList(_context.TblCiudad, "Id", "Id", tblDestino.CiudadId);
            return View(tblDestino);
        }

        // GET: Destinos/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblDestino = await _context.TblDestino
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblDestino == null)
            {
                return NotFound();
            }

            return View(tblDestino);
        }

        // POST: Destinos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblDestino = await _context.TblDestino.FindAsync(id);
            _context.TblDestino.Remove(tblDestino);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblDestinoExists(int id)
        {
            return _context.TblDestino.Any(e => e.Id == id);
        }
    }
}
