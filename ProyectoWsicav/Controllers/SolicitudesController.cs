﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProyectoWsicav.Models;
using ProyectoWsicav.ViewModel;
using ProyectoWsicav.ViewObject;

namespace ProyectoWsicav.Controllers
{
    public class SolicitudesController : Controller
    {
        private readonly UsersContext ctx;
        Respuesta res = new Respuesta();
        public SolicitudesController(UsersContext _ctx)
        {
            ctx = _ctx;
        }
        [HttpGet]
        public async Task<ActionResult> Inicio() {
            ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino")
               .Include("Cliente")
               .ToListAsync();
            ViewData["Clientes"] = new SelectList(ctx.TblCliente, "Id", "Nombre");
            return View();
        }
   
        public async Task<ActionResult> _ModalSolicitud()
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/Solicitudes/");
            }
            //Ingresando Fecha de Solicitud por defecto     
            Solicitud.Estadofile = 0;
            Solicitud.Cancelaestado = false;
            Solicitud.FechaSolicitud = DateTime.Now;
            Solicitud.NumeroSolicitud = "S00" + (Numeracion());
            ctx.Add(Solicitud);
            await ctx.SaveChangesAsync();
            var IndentificadorTabla = await ctx.TblSolicitud.FirstOrDefaultAsync(i => i.NumeroSolicitud == Solicitud.NumeroSolicitud & i.FechaSolicitud == Solicitud.FechaSolicitud & i.ClienteId == Solicitud.ClienteId);
            return RedirectToAction("Editar2", new { id = IndentificadorTabla.Id });
        }

        [HttpPost]
        public async Task<ActionResult> Inicio(IFormCollection _FrmCollection)
        {
            ///IformCollection sirve para traer toda las variables del form de esta forma logre traer los valores que tomaba la etiqueta select
            ///denominada "selectBuscar" y hacer las busquedas filtradas finalmentes solo se hicieron consultas usando entity frameworks
            ///falta aplicar css y javascript para ocultar y demas
            String _selectBuscar = _FrmCollection["selectCon"];
            if (_selectBuscar.Equals("1"))
            {
                String _file = _FrmCollection["nomFile"];

                ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino").Where(x => x.Nomfile.Contains(_file))
                    .Include("Cliente")
                    .OrderByDescending(x => x.FechaSolicitud)
                    .ToListAsync();
            }
            else if (_selectBuscar.Equals("2"))
            {
                DateTime _fechaInicio = DateTime.ParseExact(_FrmCollection["fechaInicio"], "yyyy-MM-dd",
                               System.Globalization.CultureInfo.InvariantCulture);
                DateTime _fechaFin = DateTime.ParseExact(_FrmCollection["fechaFin"], "yyyy-MM-dd",
                               System.Globalization.CultureInfo.InvariantCulture);


                ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino").Where(x => x.FechaIn >= _fechaInicio && x.FechaIn <= _fechaFin)
                        .Include("Cliente")
                        .OrderByDescending(x => x.FechaSolicitud)
                        .ToListAsync();
            }
            else if (_selectBuscar.Equals("3"))
            {
                String _file = _FrmCollection["nomFile"];
                ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino")
                    .Include("Cliente")
                    .Where(x => x.Cliente.Nombre.Contains(_file))
                    .OrderByDescending(x => x.FechaSolicitud)
                    .ToListAsync();
            }
            else if (_selectBuscar.Equals("4"))
            {
                String _varStatus = _FrmCollection["VarStatus"];
                if (_varStatus.Equals("1"))
                {
                    ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino")
                        .Where(x => x.Estadofile==1)
                        .Include("Cliente")
                        .OrderByDescending(x => x.FechaSolicitud)
                        .ToListAsync();
                }
                else if (_varStatus.Equals("2"))
                {
                    ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino")
                        .Where(x => x.Estadofile == 2)
                        .Include("Cliente")
                        .OrderByDescending(x => x.FechaSolicitud)
                        .ToListAsync();
                }
                else if (_varStatus.Equals("3"))
                {
                    ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino")
                        .Where(x => x.Estadofile == 3)
                        .Include("Cliente")
                        .OrderByDescending(x => x.FechaSolicitud)
                        .ToListAsync();
                }
            }
            else
            {
                ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino")
                    .Include("Cliente")
                    .OrderByDescending(x=>x.FechaSolicitud)
                    .ToListAsync();

            }
            ViewData["Clientes"] = new SelectList(ctx.TblCliente, "Id", "Nombre");
            return View();
        }
        
        public String Numeracion()
        {
            var resultado = Convert.ToInt32(ctx.TblSolicitud.Count()) + 1;
            return resultado.ToString();
        }

        public async Task<ActionResult> Index()
        {
            ViewBag.Solicitudes = await ctx.TblSolicitud.Include("Destino").ToListAsync();
            return View();
        }

        public async Task<ActionResult> Nuevo(int? id)
        {

            if (!ModelState.IsValid)
            {
                return Redirect("/Solicitudes/");
            }
            //DestinoId- Solicitu
            ViewData["Galleta"] = new SelectList(ctx.TblDestino, "Id", "Nombre");
            ViewData["SolicitudInicio"] = await ctx.TblSolicitud.Include("Cliente").FirstOrDefaultAsync(i => i.Id == id);
            ViewBag.Solicitud = await ctx.TblSolicitud.Include("Cliente").FirstOrDefaultAsync(i => i.Id == id);
            Tuple<TblSolicitud> tupla
            = new Tuple<TblSolicitud>
            (ViewBag.Solicitud);
            //new TblReservasDetalle() al final de la tupla va esto antes
            return View(tupla);
        }

        [BindProperty]
        public TblSolicitud Solicitud { get; set; }
        public async Task<ActionResult> Guardar()
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/Solicitudes/");
            }
            ctx.Add(Solicitud);
            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> VerDetalle(int id)
        {
            ViewBag.DetalleSolicitud = await ctx.TblSolicitud.Include("Destino").Include("Cliente").FirstOrDefaultAsync(i => i.Id == id);
            //ViewBag.DetallePasaj = await ctx.TblPasajeros.Include("TipoPasajero").Include("TipoDocumentoIdentidad").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleCoti = await ctx.TblCotizacion.Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleReserva = await ctx.TblReservasDetalle.Include("TipoServicio").Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleLiCliente = await ctx.TblLiquiCliente.Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleLiProveedor = await ctx.TblLiquiProve.Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleFact = await ctx.TblFactura.Where(i => i.SolicitudId == id).ToListAsync();

            //SUMA RESERVAS
            ViewBag.Sumreserva = await ctx.TblReservasDetalle
                .Where(i => i.SolicitudId == id)
                .Select(x => x.Monto).SumAsync();

            //SUMA LIQUIDACION CLIENTE
            ViewBag.Sumlicli = await ctx.TblLiquiCliente
                .Where(i => i.SolicitudId == id)
                .Select(x => x.Monto).SumAsync();

            //SUMA LIQUIDACION PROVEEDOR
            ViewBag.Sumlipro = await ctx.TblLiquiProve
                .Where(i => i.SolicitudId == id)
                .Select(x => x.Monto).SumAsync();

            //Vista Status Html
            var statsolicitud = (TblSolicitud)ViewBag.DetalleSolicitud;
            String htmltipoestado = "";
            htmltipoestado = Metodos.TipoEstadoSolicitud(htmltipoestado, statsolicitud);
            ViewBag.TipoEstaSolicitud = htmltipoestado;

            if (ViewBag.DetalleSolicitud == null)
            {
                return Redirect("/Solicitudes/");
            }

            Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura> tuplita
            = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
            (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());

            return View("~/Views/Solicitudes/Detalle.cshtml", tuplita);
        }

        public async Task<ActionResult> Editar(int id, String tipo, int Ideditvista)
        {
            // var usersContext = _context.TblCliente.FromSqlRaw("SELECT * FROM Tbl_Cliente").Include(t => t.TipoCliente).Include(t => t.TipoDocumentoIdentidad).ToListAsync();

            if (String.IsNullOrEmpty(tipo))
            {
                tipo = "";
            }

            //Detalle solo
            ViewBag.DetalleSolicitud = await ctx.TblSolicitud.Include("Destino").Include("Cliente").FirstOrDefaultAsync(i => i.Id == id);

            //Conjunto de detalles
            //ViewBag.DetallePasaj = await ctx.TblPasajeros.Include("TipoPasajero").Include("TipoDocumentoIdentidad").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleCoti = await ctx.TblCotizacion.Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleReserva = await ctx.TblReservasDetalle.Include("TipoServicio").Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleLiCliente = await ctx.TblLiquiCliente.Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleLiProveedor = await ctx.TblLiquiProve.Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleFact = await ctx.TblFactura.Where(i => i.SolicitudId == id).ToListAsync();

            //SUMA RESERVAS
            ViewBag.Sumreserva = await ctx.TblReservasDetalle
                .Where(i => i.SolicitudId == id)
                .Select(x => x.Monto).SumAsync();

            //SUMA LIQUIDACION CLIENTE
            ViewBag.Sumlicli = await ctx.TblLiquiCliente
                .Where(i => i.SolicitudId == id)
                .Select(x => x.Monto).SumAsync();

            //SUMA LIQUIDACION PROVEEDOR
            ViewBag.Sumlipro = await ctx.TblLiquiProve
                .Where(i => i.SolicitudId == id)
                .Select(x => x.Monto).SumAsync();

            //Habilitar pasajero principal con CantidadPersonasPrincipales
            ViewBag.PasaPrincipal = Metodos.NumePrincipales((List<TblPasajeros>)ViewBag.DetallePasaj);

            //EditDatoSolo
            ViewBag.EditarPasa = null;
            ViewBag.EditarRese = null; //Editar_reserva
            ViewBag.Editarlicli = null;
            ViewBag.Editarlipro = null;

            //HtmlMostrar
            ViewBag.navlista = null;
            ViewBag.datosclas = null;
            ViewBag.pasajclas = null;
            ViewBag.reserclas = null;
            ViewBag.licliclas = null;
            ViewBag.liproclas = null;

            //SelectDatos
            ViewData["Destinos"] = new SelectList(ctx.TblDestino, "Id", "Nombre");
            ViewData["Galleta"] = new SelectList(ctx.TblTipoServicio, "Id", "Descripcion");
            ViewData["Morocha"] = new SelectList(ctx.TblProveedor, "Id", "Nombre");
            ViewData["Documentos"] = new SelectList(ctx.TblTipoDocumentoIdentidad, "Id", "Descripcion");
            ViewData["TPasaj"] = new SelectList(ctx.TblTipoPasajero, "Id", "Descripcion");
            ViewData["Provee"] = new SelectList(ctx.TblProveedor, "Id", "Nombre");

            //Status de la solicitud
            var statsolicitud = (TblSolicitud)ViewBag.DetalleSolicitud;
            ViewBag.stasoli = null;
            ViewBag.stapasa = null;
            ViewBag.starese = null;
            ViewBag.stalicli = null;
            ViewBag.Liquicli = null;
            ViewBag.Liquipro = null;

            //Vista Status Html
            String htmltipoestado = "";
            htmltipoestado = Metodos.TipoEstadoSolicitud(htmltipoestado, statsolicitud);
            ViewBag.TipoEstaSolicitud = htmltipoestado;


            if (ViewBag.DetalleSolicitud == null)
            {
                return Redirect("/Solicitudes/");
            }


            //Validación y envio de la vista dependiendo del estado de la solicitud

            if (statsolicitud.Cancelaestado is true)
            {
                String vistaDatos = "";
                Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                ViewBag.visoli = vistaDatos;

                String vistaPasa = "";
                Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                ViewBag.vipasa = vistaPasa;

                String vistaCoti = "";
                Metodos.VistaCotizacion(ref vistaCoti, (List<TblCotizacion>)ViewBag.DetalleCoti);
                ViewBag.vicoti = vistaCoti;

                String vistaRese = "";
                Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                ViewBag.virese = vistaRese;

                String vistaLiCli = "";
                Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                ViewBag.vilicli = vistaLiCli;

                String vistaLiPro = "";
                Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                ViewBag.vilipro = vistaLiPro;

                String vistaFact = "";
                Metodos.VistaFacturacion(ref vistaFact, (List<TblFactura>)ViewBag.DetalleFact);
                ViewBag.vifac = vistaFact;
            }
            else if (statsolicitud.Cancelaestado is false)
            {
                if (statsolicitud.Estadofile == 1)
                {
                    ViewBag.stasoli = true;

                    String vistaPasa = "";
                    Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    ViewBag.vipasa = vistaPasa;

                    String vistaCoti = "";
                    Metodos.VistaCotizacion(ref vistaCoti, (List<TblCotizacion>)ViewBag.DetalleCoti);
                    ViewBag.vicoti = vistaCoti;

                    String vistaRese = "";
                    Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                    ViewBag.virese = vistaRese;

                    String vistaLiCli = "";
                    Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                    ViewBag.vilicli = vistaLiCli;

                    String vistaLiPro = "";
                    Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                    ViewBag.vilipro = vistaLiPro;

                    String vistaFact = "";
                    Metodos.VistaFacturacion(ref vistaFact, (List<TblFactura>)ViewBag.DetalleFact);
                    ViewBag.vifac = vistaFact;
                }
                else if (statsolicitud.Estadofile == 2)
                {
                    ViewBag.stapasa = true;

                    String vistaDatos = "";
                    Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    ViewBag.visoli = vistaDatos;

                    String vistaCoti = "";
                    Metodos.VistaCotizacion(ref vistaCoti, (List<TblCotizacion>)ViewBag.DetalleCoti);
                    ViewBag.vicoti = vistaCoti;

                    String vistaRese = "";
                    Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                    ViewBag.virese = vistaRese;

                    String vistaLiCli = "";
                    Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                    ViewBag.vilicli = vistaLiCli;

                    String vistaLiPro = "";
                    Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                    ViewBag.vilipro = vistaLiPro;

                    String vistaFact = "";
                    Metodos.VistaFacturacion(ref vistaFact, (List<TblFactura>)ViewBag.DetalleFact);
                    ViewBag.vifac = vistaFact;
                }
                else if (statsolicitud.Estadofile == 3)
                {
                    ViewBag.stacoti = true;

                    String vistaDatos = "";
                    Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    ViewBag.visoli = vistaDatos;

                    String vistaPasa = "";
                    Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    ViewBag.vipasa = vistaPasa;

                    String vistaRese = "";
                    Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                    ViewBag.virese = vistaRese;

                    String vistaLiCli = "";
                    Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                    ViewBag.vilicli = vistaLiCli;

                    String vistaLiPro = "";
                    Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                    ViewBag.vilipro = vistaLiPro;

                    String vistaFact = "";
                    Metodos.VistaFacturacion(ref vistaFact, (List<TblFactura>)ViewBag.DetalleFact);
                    ViewBag.vifac = vistaFact;

                }
                else if (statsolicitud.Estadofile == 4)
                {
                    ViewBag.starese = true;

                    String vistaDatos = "";
                    Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    ViewBag.visoli = vistaDatos;

                    String vistaPasa = "";
                    Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    ViewBag.vipasa = vistaPasa;

                    String vistaCoti = "";
                    Metodos.VistaCotizacion(ref vistaCoti, (List<TblCotizacion>)ViewBag.DetalleCoti);
                    ViewBag.vicoti = vistaCoti;

                    String vistaLiCli = "";
                    Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                    ViewBag.vilicli = vistaLiCli;

                    String vistaLiPro = "";
                    Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                    ViewBag.vilipro = vistaLiPro;

                    String vistaFact = "";
                    Metodos.VistaFacturacion(ref vistaFact, (List<TblFactura>)ViewBag.DetalleFact);
                    ViewBag.vifac = vistaFact;
                }
                else if (statsolicitud.Estadofile == 5)
                {
                    ViewBag.Liquicli = true;

                    String vistaDatos = "";
                    Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    ViewBag.visoli = vistaDatos;

                    String vistaPasa = "";
                    Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    ViewBag.vipasa = vistaPasa;

                    String vistaCoti = "";
                    Metodos.VistaCotizacion(ref vistaCoti, (List<TblCotizacion>)ViewBag.DetalleCoti);
                    ViewBag.vicoti = vistaCoti;

                    String vistaRese = "";
                    Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                    ViewBag.virese = vistaRese;

                    String vistaLiPro = "";
                    Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                    ViewBag.vilipro = vistaLiPro;

                    String vistaFact = "";
                    Metodos.VistaFacturacion(ref vistaFact, (List<TblFactura>)ViewBag.DetalleFact);
                    ViewBag.vifac = vistaFact;
                }
                else if (statsolicitud.Estadofile == 6)
                {
                    ViewBag.Liquipro = true;

                    String vistaDatos = "";
                    Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    ViewBag.visoli = vistaDatos;

                    String vistaPasa = "";
                    Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    ViewBag.vipasa = vistaPasa;

                    String vistaCoti = "";
                    Metodos.VistaCotizacion(ref vistaCoti, (List<TblCotizacion>)ViewBag.DetalleCoti);
                    ViewBag.vicoti = vistaCoti;

                    String vistaRese = "";
                    Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                    ViewBag.virese = vistaRese;

                    String vistaLiCli = "";
                    Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                    ViewBag.vilicli = vistaLiCli;

                    String vistaFact = "";
                    Metodos.VistaFacturacion(ref vistaFact, (List<TblFactura>)ViewBag.DetalleFact);
                    ViewBag.vifac = vistaFact;
                }
                else if (statsolicitud.Estadofile == 7)
                {
                    ViewBag.stafact = true;
                    String vistaDatos = "";
                    Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    ViewBag.visoli = vistaDatos;

                    String vistaPasa = "";
                    Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    ViewBag.vipasa = vistaPasa;

                    String vistaCoti = "";
                    Metodos.VistaCotizacion(ref vistaCoti, (List<TblCotizacion>)ViewBag.DetalleCoti);
                    ViewBag.vicoti = vistaCoti;

                    String vistaRese = "";
                    Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                    ViewBag.virese = vistaRese;

                    String vistaLiCli = "";
                    Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                    ViewBag.vilicli = vistaLiCli;

                    String vistaLiPro = "";
                    Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                    ViewBag.vilipro = vistaLiPro;
                }
                else if (statsolicitud.Estadofile == 8)
                {

                    ///////crear variables y vistas para fatantes esta fase representa la vista del file completado;


                    //String vistaDatos = "";
                    //Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    //ViewBag.visoli = vistaDatos;

                    //String vistaPasa = "";
                    //Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    //ViewBag.vipasa = vistaPasa;


                    //String vistaRese = "";
                    //Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (int)ViewBag.Sumreserva);
                    //ViewBag.virese = vistaRese;

                    //String vistaLiCli = "";
                    //Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (int)ViewBag.Sumlicli);
                    //ViewBag.vilicli = vistaLiCli;

                    //String vistaLiPro = "";
                    //Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (int)ViewBag.Sumlipro);
                    //ViewBag.vilipro = vistaLiPro;
                }
                else if (statsolicitud.Estadofile == 9)
                {

                    ///////crear variables y vistas para fatantes esta fase representa la vista del file Caido;


                    String vistaDatos = "";
                    Metodos.VistaSolicitud(ref vistaDatos, (TblSolicitud)ViewBag.DetalleSolicitud);
                    ViewBag.visoli = vistaDatos;

                    String vistaPasa = "";
                    Metodos.VistaPasajero(ref vistaPasa, (List<TblPasajeros>)ViewBag.DetallePasaj);
                    ViewBag.vipasa = vistaPasa;


                    String vistaRese = "";
                    Metodos.VistaReserva(ref vistaRese, (List<TblReservasDetalle>)ViewBag.DetalleReserva, (decimal)ViewBag.Sumreserva);
                    ViewBag.virese = vistaRese;

                    String vistaLiCli = "";
                    Metodos.VistaLiCliente(ref vistaLiCli, (List<TblLiquiCliente>)ViewBag.DetalleLiCliente, (decimal)ViewBag.Sumlicli);
                    ViewBag.vilicli = vistaLiCli;

                    String vistaLiPro = "";
                    Metodos.VistaLiProveedor(ref vistaLiPro, (List<TblLiquiProve>)ViewBag.DetalleLiProveedor, (decimal)ViewBag.Sumlipro);
                    ViewBag.vilipro = vistaLiPro;
                }
            }

            //Posicionamiento Por accion en pestaña

            Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura> tuplita = null;

            String boton = "";
            String nav = "";
            //LO DE ABAJO CREO QUE NO SE EMPLEARA MAS
            String Opestado = "";
            if (tipo != "")
            {
                Opestado = tipo.Substring(0, 2);
            }
            //

            if (tipo.StartsWith("P"))
            {
                ViewBag.pasajclas = "in active";
                //ViewBag.navlista = Metodos.VistasInicio("P");
                Metodos.VistasInicio("P", statsolicitud, out nav, out boton);
                if (tipo.Equals("Pasa_edit"))
                {
                    ViewBag.EditarPasa = await ctx.TblPasajeros.Include("TipoPasajero").Include("TipoDocumentoIdentidad")
                        .FirstOrDefaultAsync(i => i.Id == Ideditvista);
                    //Habilitar el campo pasajero principal

                    ViewBag.PasaPrincipal = Metodos.ValidarPasaPrincipal((TblPasajeros)ViewBag.EditarPasa, (bool)ViewBag.PasaPrincipal);

                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                    (ViewBag.DetalleSolicitud, ViewBag.EditarPasa, new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
                }
                else
                {
                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                     (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
                }
            }
            else if (tipo.StartsWith("R"))
            {
                ViewBag.reserclas = "in active";
                //ViewBag.navlista = Metodos.VistasInicio("R");
                Metodos.VistasInicio("R", statsolicitud, out nav, out boton);
                if (tipo.Equals("Rese_edit"))
                {
                    ViewBag.EditarRese = await ctx.TblReservasDetalle.Include("TipoServicio").Include("Proveedor")
                        .FirstOrDefaultAsync(i => i.Id == Ideditvista);

                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                    (ViewBag.DetalleSolicitud, new TblPasajeros(), ViewBag.EditarRese, new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
                }
                else
                {
                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                    (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());

                }
            }
            else if (tipo.StartsWith("Coti"))
            {
                ViewBag.coticlas = "in active";
                Metodos.VistasInicio("Cti", statsolicitud, out nav, out boton);

                if (tipo.Equals("Coti_edit"))
                {
                    if (TempData["CotiEdit"] != null)
                    {
                        ViewBag.EditarCoti = JsonConvert.DeserializeObject<VCotizacion>((String)TempData["CotiEdit"]); //Aqui va la vistaModel
                        tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                        (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), ViewBag.EditarCoti, new VLicliente(), new VLiprovee(), new VFactura());
                    }
                }
                else
                {
                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                    (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
                }
            }
            else if (tipo.StartsWith("LC"))
            {
                ViewBag.Licliclas = "in active";
                Metodos.VistasInicio("LC", statsolicitud, out nav, out boton);
                if (tipo.Equals("LC_Edit"))
                {
                    if (TempData["LclEdit"] != null)
                    {
                        ViewBag.Editarlicli = JsonConvert.DeserializeObject<VLicliente>((String)TempData["LclEdit"]);
                        tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                        (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), ViewBag.Editarlicli, new VLiprovee(), new VFactura());
                    }
                }
                else
                {
                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                    (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
                }
            }
            else if (tipo.StartsWith("LP"))
            {
                ViewBag.Liproclas = "in active";
                Metodos.VistasInicio("LP", statsolicitud, out nav, out boton);
                if (tipo.Equals("LP_Edit"))
                {
                    if (TempData["LprEdit"] != null)
                    {
                        ViewBag.Editarlipro = JsonConvert.DeserializeObject<VLiprovee>((String)TempData["LprEdit"]);
                        tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                        (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), ViewBag.Editarlipro, new VFactura());
                    }
                }
                else
                {
                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                    (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
                }
            }
            else if (tipo.StartsWith("Fa"))
            {
                ViewBag.factclas = "in active";
                Metodos.VistasInicio("Fc", statsolicitud, out nav, out boton);
                if (tipo.Equals("Fac_edit"))
                {
                    if (TempData["FacEdit"] != null)
                    {
                        ViewBag.EditarFact = JsonConvert.DeserializeObject<VFactura>((String)TempData["FacEdit"]); //Aqui va la vistaModel
                        tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                        (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), ViewBag.EditarFact);
                    }
                }
                else
                {
                    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                    (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
                }
            }
            //else if (tipo.Equals("Cancelado"))
            //{
            //    ViewBag.datosclas = "in active";
            //    Metodos.VistasInicio(Opestado, statsolicitud, out nav, out boton);
            //    tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
            //    (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());
            //}
            else
            {
                ViewBag.datosclas = "in active";
                //ViewBag.navlista = Metodos.VistasInicio("D");
                Metodos.VistasInicio("D", statsolicitud, out nav, out boton);

                tuplita = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, VCotizacion, VLicliente, VLiprovee, VFactura>
                (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new VCotizacion(), new VLicliente(), new VLiprovee(), new VFactura());

            }

            ViewBag.navlista = nav; //MENU <LI>
            ViewBag.boton = boton; //ACTUALIZAR

            //ViewBag.Caramelo = "Solicitud 000X";
            return View("~/Views/Solicitudes/Editar.cshtml", tuplita);
        }

        public async Task<ActionResult> Actsoli([Bind(Prefix = "solicitud")] TblSolicitud Solictd)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("/Solicitudes/");
            }

            //Solictd.Estadofile = 2; ESTO NO 
            ctx.TblSolicitud.Attach(Solictd);
            ctx.Entry(Solictd).State = EntityState.Modified;
            await ctx.SaveChangesAsync();
            if (Solictd.Estadofile == 0)
            {
                return RedirectToAction("CamEstado", new { id = Solictd.Id });
            }
            else
            {
                return RedirectToAction("Editar2", new { id = Solictd.Id });
            }

        }

        //public async Task<ActionResult> NuevoPasajero2(String Tipoaccion, int Pasajeroid, [Bind(Prefix = "Item2")] TblPasajeros DePasajero)
        //{
        //    String accion = Tipoaccion;

        //    if (accion.Equals("Datapasa"))
        //    { //Se Maneja en el section
        //        DePasajero = await ctx.TblPasajeros.FirstOrDefaultAsync(i => i.Id == Pasajeroid);
        //        return RedirectToAction("Editar", new { id = DePasajero.SolicitudId, tipo = "Pasa_edit", Ideditvista = Pasajeroid });
        //    }
        //    else if (accion.Equals("Crearpasa"))
        //    { //Se maneja en el form
        //        if (!ModelState.IsValid)
        //        {
        //            return Redirect("/Solicitudes/");
        //        }
        //        ctx.TblPasajeros.Add(DePasajero);
        //    }
        //    else if (accion.Equals("Actupasa"))
        //    { //Se naneja en el formulario
        //        ctx.TblPasajeros.Attach(DePasajero);
        //        ctx.Entry(DePasajero).State = EntityState.Modified;
        //    }
        //    else if (accion.Equals("Elimipasa"))
        //    { //Se maneja en el section
        //        DePasajero = await ctx.TblPasajeros.FirstOrDefaultAsync(i => i.Id == Pasajeroid);
        //        ctx.TblPasajeros.Remove(DePasajero);
        //    }
        //    await ctx.SaveChangesAsync();
        //    return RedirectToAction("Editar", new { id = DePasajero.SolicitudId, tipo = "Pasa" });
        //}

        public async Task<ActionResult> NuevaCotiza2(String Tipoaccion, int Cotid, [Bind(Prefix = "Item4")] VCotizacion coti)
        {
            String accion = Tipoaccion;

            if (accion.Equals("Datacot"))
            {
                var DCotiz = await ctx.TblCotizacion.FirstOrDefaultAsync(i => i.Id == Cotid); //Detalles de la base de datos cotizacion
                var VCotiz = new VCotizacion(DCotiz); //Vista empleada para actualizar/editar
                TempData["CotiEdit"] = JsonConvert.SerializeObject(VCotiz); //Empleamos el temporal para enviar un objeto
                return RedirectToAction("Editar", new { id = DCotiz.SolicitudId, tipo = "Coti_edit" });
            }
            else if (accion.Equals("Crearcot"))
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("/Solicitudes/");
                }
                var SCoti = new TblCotizacion(coti); //Save/Guardar cotizacion
                if (coti.DocCoti != null)
                {
                    SCoti.Documentonombre = coti.DocCoti.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    SCoti.Documento = Metodos.GetByteArray(coti.DocCoti); //Pasamos el iform a varchar y guardamos
                }
                ctx.TblCotizacion.Add(SCoti);
            }
            else if (accion.Equals("Actucot"))
            {
                var TblCoti = await ctx.TblCotizacion.FindAsync(coti.Id); //Variable para actualizar el modelo de BD
                coti.SolicitudId = TblCoti.SolicitudId; //PUESTO QUE ESTAS VISTAS SON DIFERENTES, SOLAMENTE LE PASAMOS LA PROPIEDAD PARA QUE PUEDA SER ENVIADA EN EL RETURN

                TblCoti.FechaCotizacion = coti.Fecha; //En adelante capturamos los datos de la vista para act el modelo
                TblCoti.NumeCotizacion = coti.NumeCotizacion;
                TblCoti.Monto = coti.Monto;
                if (coti.DocCoti != null)
                {
                    TblCoti.Documentonombre = coti.DocCoti.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    TblCoti.Documento = Metodos.GetByteArray(coti.DocCoti); //Pasamos el iform a varchar y guardamos
                }
            }
            else if (accion.Equals("Elimicot"))
            {
                var FCoti = await ctx.TblCotizacion.FirstOrDefaultAsync(i => i.Id == Cotid); //File Cotizacion
                coti.SolicitudId = FCoti.SolicitudId; //ESTO ES PARA EL RETURN DE LO CONTRARIO ERROR, LOS MISMO QUE EN ACTU

                ctx.TblCotizacion.Remove(FCoti);
            }
            await ctx.SaveChangesAsync();
            return RedirectToAction("Editar", new { id = coti.SolicitudId, tipo = "Coti" });
        }

        public async Task<ActionResult> NuevaDreserva(String Tipoaccion, int Reservaid, [Bind(Prefix = "Item3")] TblReservasDetalle DeReserva)
        {
            String accion = Tipoaccion;

            if (accion.Equals("Datarese"))
            {
                DeReserva = await ctx.TblReservasDetalle.FirstOrDefaultAsync(i => i.Id == Reservaid);
                return RedirectToAction("Editar", new { id = DeReserva.SolicitudId, tipo = "Rese_edit", Ideditvista = Reservaid });
            }
            else if (accion.Equals("Crearrese"))
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("/Solicitudes/");
                }
                ctx.TblReservasDetalle.Add(DeReserva);
            }
            else if (accion.Equals("Acturese")) {
                ctx.TblReservasDetalle.Attach(DeReserva);
                ctx.Entry(DeReserva).State = EntityState.Modified;
            }
            else if (accion.Equals("Elimirese")) {
                DeReserva = await ctx.TblReservasDetalle.FirstOrDefaultAsync(i => i.Id == Reservaid);
                ctx.TblReservasDetalle.Remove(DeReserva);
            }
            await ctx.SaveChangesAsync();
            return RedirectToAction("Editar", new { id = DeReserva.SolicitudId, tipo = "Rese" });
        }

        public async Task<ActionResult> NuevaLCliente2(String Tipoaccion, int Lclid, [Bind(Prefix = "Item5")] VLicliente LiCliente)
        {
            String accion = Tipoaccion;

            if (accion.Equals("Datalicli"))
            {
                var DLic = await ctx.TblLiquiCliente.FirstOrDefaultAsync(i => i.Id == Lclid); 
                var VLic = new VLicliente(DLic); 
                TempData["LclEdit"] = JsonConvert.SerializeObject(VLic); 
                return RedirectToAction("Editar", new { id = DLic.SolicitudId, tipo = "LC_Edit" });
            }
            else if (accion.Equals("Crealicli"))
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("/Solicitudes/");
                }
                var SLic = new TblLiquiCliente(LiCliente); 
                if (LiCliente.Doc != null)
                {
                    SLic.Documentonombre = LiCliente.Doc.FileName; 
                    SLic.Documento = Metodos.GetByteArray(LiCliente.Doc); 
                }
                ctx.TblLiquiCliente.Add(SLic);
            }
            else if (accion.Equals("Actulicli"))
            {
                var TblLic = await ctx.TblLiquiCliente.FindAsync(LiCliente.Id); 
                LiCliente.SolicitudId = TblLic.SolicitudId;

                TblLic.FechaLcliente = LiCliente.FechaLcliente;
                TblLic.FormaPago = LiCliente.FormaPago;
                TblLic.Banco = LiCliente.Banco;
                TblLic.Titular = LiCliente.Titular;
                TblLic.NumeTarjeta = LiCliente.NumeTarjeta;
                TblLic.NumeVouch = LiCliente.NumeVouch;
                TblLic.Monto = LiCliente.Monto;
                if (LiCliente.Doc != null)
                {
                    TblLic.Documentonombre = LiCliente.Doc.FileName;
                    TblLic.Documento = Metodos.GetByteArray(LiCliente.Doc);
                }
            }
            else if (accion.Equals("Elimilicli"))
            {
                var FLic = await ctx.TblLiquiCliente.FirstOrDefaultAsync(i => i.Id == Lclid);
                LiCliente.SolicitudId = FLic.SolicitudId; 

                ctx.TblLiquiCliente.Remove(FLic);
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Editar", new { id = LiCliente.SolicitudId, tipo = "LC_Vista" });
        }

        public async Task<ActionResult> NuevaLProveedor2(String Tipoaccion, int Lprid, [Bind(Prefix = "Item6")] VLiprovee LiProveedor)
        {
            String accion = Tipoaccion;

            if (accion.Equals("Datalipro"))
            {
                var DLip = await ctx.TblLiquiProve.FirstOrDefaultAsync(i => i.Id == Lprid);
                var VLip = new VLiprovee(DLip);
                TempData["LprEdit"] = JsonConvert.SerializeObject(VLip);
                return RedirectToAction("Editar", new { id = DLip.SolicitudId, tipo = "LP_Edit" });
            }
            else if (accion.Equals("Crealipro"))
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("/Solicitudes/");
                }
                var SLip = new TblLiquiProve(LiProveedor);
                if (LiProveedor.Doc != null)
                {
                    SLip.Documentonombre = LiProveedor.Doc.FileName;
                    SLip.Documento = Metodos.GetByteArray(LiProveedor.Doc);
                }
                ctx.TblLiquiProve.Add(SLip);
            }
            else if (accion.Equals("Actulipro"))
            {
                var TblLip = await ctx.TblLiquiProve.FindAsync(LiProveedor.Id);
                LiProveedor.SolicitudId = TblLip.SolicitudId;

                TblLip.ProveedorId = LiProveedor.ProveedorId;
                TblLip.FechaLprove = LiProveedor.FechaLprove;
                TblLip.FormaPago = LiProveedor.FormaPago;
                TblLip.Banco = LiProveedor.Banco;
                TblLip.Titular = LiProveedor.Titular;
                TblLip.NumeVaucher = LiProveedor.NumeVaucher;
                TblLip.NumeTarjeta = LiProveedor.NumeTarjeta;
                TblLip.PagoCliente = LiProveedor.PagoCliente;
                TblLip.Monto = LiProveedor.Monto;

                if (LiProveedor.Doc != null)
                {
                    TblLip.Documentonombre = LiProveedor.Doc.FileName;
                    TblLip.Documento = Metodos.GetByteArray(LiProveedor.Doc);
                }
            }
            else if (accion.Equals("Elimilipro"))
            {
                var FLip = await ctx.TblLiquiProve.FirstOrDefaultAsync(i => i.Id == Lprid);
                LiProveedor.SolicitudId = FLip.SolicitudId;

                ctx.TblLiquiProve.Remove(FLip);
            }
            await ctx.SaveChangesAsync();
            return RedirectToAction("Editar", new { id = LiProveedor.SolicitudId, tipo = "LP_Vista" });
        }

        public async Task<ActionResult> NuevaFac2(String Tipoaccion, int Facid, [Bind(Prefix = "Item7")] VFactura fac)
        {
            String accion = Tipoaccion;

            if (accion.Equals("Datafac"))
            {
                var DFac = await ctx.TblFactura.FirstOrDefaultAsync(i => i.Id == Facid); //Detalles de la base de datos factura
                var VFac = new VFactura(DFac); //Vista factura empleada para actualizar/editar
                TempData["FacEdit"] = JsonConvert.SerializeObject(VFac); //Empleamos el temporal para enviar un objeto
                return RedirectToAction("Editar", new { id = DFac.SolicitudId, tipo = "Fac_edit" });
            }
            else if (accion.Equals("Crearfac"))
            {
                if (!ModelState.IsValid)
                {
                    return Redirect("/Solicitudes/");
                }
                var SFac = new TblFactura(fac); //Save/Guardar cotizacion
                if (fac.DocFact != null)
                {
                    SFac.Documentonombre = fac.DocFact.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    SFac.Documento = Metodos.GetByteArray(fac.DocFact); //Pasamos el iform a varchar y guardamos
                }
                ctx.TblFactura.Add(SFac);
            }
            else if (accion.Equals("Actufac"))
            {
                var TblFac = await ctx.TblFactura.FindAsync(fac.Id); //Variable para actualizar el modelo de BD
                fac.SolicitudId = TblFac.SolicitudId; //PUESTO QUE ESTAS VISTAS SON DIFERENTES, SOLAMENTE LE PASAMOS LA PROPIEDAD PARA QUE PUEDA SER ENVIADA EN EL RETURN


                TblFac.FechaFactu = fac.Fecha; //En adelante capturamos los datos de la vista para act el modelo
                TblFac.NumeFactu = fac.NumeFactu;
                TblFac.Monto = fac.Monto;
                if (fac.DocFact != null)
                {
                    TblFac.Documentonombre = fac.DocFact.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    TblFac.Documento = Metodos.GetByteArray(fac.DocFact); //Pasamos el iform a varchar y guardamos
                }
            }
            else if (accion.Equals("Elimifac"))
            {
                var FFact = await ctx.TblFactura.FirstOrDefaultAsync(i => i.Id == Facid); //File Facturacion
                fac.SolicitudId = FFact.SolicitudId; //PUESTO QUE ESTAS VISTAS SON DIFERENTES, SOLAMENTE LE PASAMOS LA PROPIEDAD PARA QUE PUEDA SER ENVIADA EN EL RETURN

                ctx.TblFactura.Remove(FFact);
            }
            await ctx.SaveChangesAsync();
            return RedirectToAction("Editar", new { id = fac.SolicitudId, tipo = "Fac" });
        }

        public async Task<ActionResult> CancelarRecuperara(int Id) 
        {
            
            var soliestado = await ctx.TblSolicitud.FirstOrDefaultAsync(i => i.Id == Id);
            if (soliestado.Cancelaestado is false || soliestado.Cancelaestado is null) //SE CANCELA UN FILE
            {
                soliestado.Cancelaestado = true;
                soliestado.Fechacancelacion = DateTime.Now;
                soliestado.Reaperturaestado = false;
            }
            else if (soliestado.Cancelaestado is true) //SE APERTURA EL FILE
            {
                soliestado.Cancelaestado = false;
                //soliestado.Estadofile = null;
                soliestado.Reaperturaestado = true;
            } 
            //else if (soliestado.Cancelaestado is null) //se deja ESTO POR AHORA HACER QUE EL FILE SE CREE CON UN FALSE POR DEFECTO EN ESTA PROPIEDAD
            //{
            //    soliestado.Cancelaestado = false;
            //}
            await ctx.SaveChangesAsync();
            return RedirectToAction("Editar2", new { id = Id});
        }

        public async Task<ActionResult> ReaperturarSolicitud(int Id) //ESTO NO SE EMPLEA
        {
            var statsolicitud = await ctx.TblSolicitud.FirstOrDefaultAsync(i => i.Id == Id);
            statsolicitud.Cancelaestado = false;
            //statsolicitud.Estadofile = null;
            await ctx.SaveChangesAsync();
            return RedirectToAction("CamEstado", new { id = Id });
        }

        public async Task<ActionResult> CamEstado(int id)
        {
            //Status Solicitud
            var statsolicitud = await ctx.TblSolicitud.FirstOrDefaultAsync(i => i.Id == id);
            String tipovista = "";

            if (statsolicitud.Cancelaestado is true)
            {
                tipovista = "Cancelado";
            } 
            else if (statsolicitud.Cancelaestado is false)
            {
                if (statsolicitud.Estadofile == 0)
                {
                    statsolicitud.Estadofile = 2;//Solicitud vacia a llena pasa a pestaña pasajero
                }
                else if (statsolicitud.Estadofile == 1)
                {
                    statsolicitud.Estadofile = 2;//pasajero
                    tipovista = "Pasa";
                }
                else if (statsolicitud.Estadofile == 2)
                {
                    statsolicitud.Estadofile = 3;//cotizacion
                    tipovista = "Coti";
                }
                else if (statsolicitud.Estadofile == 3)
                {
                    statsolicitud.Estadofile = 4;//reserva
                    tipovista = "Rese";
                }
                else if (statsolicitud.Estadofile == 4)
                {
                    statsolicitud.Estadofile = 5;//Licliente
                    tipovista = "LC_Vista";
                }
                else if (statsolicitud.Estadofile == 5)
                {
                    statsolicitud.Estadofile = 6;//l´proveedor
                    tipovista = "LP_Vista";
                }
                else if (statsolicitud.Estadofile == 6)
                {
                    statsolicitud.Estadofile = 7;//factura
                    tipovista = "Fac";
                }
                else if (statsolicitud.Estadofile == 7)
                {
                    statsolicitud.Estadofile = 1;//Directo a solicitud
                }
                //else if (statsolicitud.Estadofile == 7)
                //{
                //    statsolicitud.Estadofile = null; //Finalizada
                //}
                //else if (statsolicitud.Estadofile is null)
                //{
                //    statsolicitud.Estadofile = 1;//datos
                //}
            }
            await ctx.SaveChangesAsync();
            //return RedirectToAction("Editar", new { id = statsolicitud.Id, tipo = tipovista });
            return RedirectToAction("Editar2", new { id = statsolicitud.Id });
        }

        public async Task<ActionResult> DownloadFile(int Id, String Tipoaccion)
        {
            String accion = Tipoaccion;
            if (accion.Equals("DCoti"))
            {
                var Docu = await ctx.TblCotizacion.FindAsync(Id);
                if (Docu.Documento != null)
                {
                    string fileName = Docu.Documentonombre; //AQUI PODEMOS CAMBIAR EL NOMBRE CON QUE SE DESCARGA
                    return File(Docu.Documento, "application/octet-Strem", fileName);

                    ////EN CASO DE QUE NO SE QUIERA DESCARGAR EL ARCHIVO COMO TAL Y QUE SALGA ESTE EN OTRA PESTAÑA REALIZAR ESTO
                    //return File(Docu.Documento, "application/pdf");
                }
            }
            else if (accion.Equals("DLic"))
            {
                var Docu = await ctx.TblLiquiCliente.FindAsync(Id);
                if (Docu.Documento != null)
                {
                    string fileName = Docu.Documentonombre;
                    return File(Docu.Documento, "application/octet-Strem", fileName);
                }
            }
            else if (accion.Equals("DLip"))
            {
                var Docu = await ctx.TblLiquiProve.FindAsync(Id);
                if (Docu.Documento != null)
                {
                    string fileName = Docu.Documentonombre;
                    return File(Docu.Documento, "application/octet-Strem", fileName);
                }
            }
            else if(accion.Equals("DFact"))
            {
                var Docu = await ctx.TblFactura.FindAsync(Id);
                if (Docu.Documento != null)
                {
                    string fileName = Docu.Documentonombre;
                    return File(Docu.Documento, "application/octet-Strem", fileName);
                }
            }
            return Redirect("/Solicitudes/");
        }

        //public async Task<ActionResult> VerDetalle2(int id)
        //{
        //    ViewBag.DetalleSolicitud = await ctx.TblSolicitud.Include("Destino").FirstOrDefaultAsync(i => i.Id == id);
        //    ViewBag.DetalleReserva = await ctx.TblReservasDetalle.Include("TipoServicio").Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();
        //    //ViewBag.DetallePasaj = await ctx.TblPasajeros.Include("TipoPasajero").Include("TipoDocumentoIdentidad").Where(i => i.SolicitudId == id).ToListAsync();
        //    ViewBag.DetalleLiCliente = await ctx.TblLiquiCliente.Where(i => i.SolicitudId == id).ToListAsync();
        //    ViewBag.DetalleLiProveedor = await ctx.TblLiquiProve.Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();

        //    //SUMA LIQUIDACION CLIENTE
        //    ViewBag.Sumlicli = await ctx.TblLiquiCliente
        //        .Where(i => i.SolicitudId == id)
        //        .Select(x => x.Monto).SumAsync();

        //    //SUMA LIQUIDACION PROVEEDOR
        //    ViewBag.Sumlipro = await ctx.TblLiquiProve
        //        .Where(i => i.SolicitudId == id)
        //        .Select(x => x.Monto).SumAsync();

        //    if (ViewBag.DetalleSolicitud == null)
        //    {
        //        return Redirect("/Solicitudes/");
        //    }


        //    Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, TblCotizacion, TblLiquiCliente, TblLiquiProve, TblFactura> tuplita
        //    = new Tuple<TblSolicitud, TblPasajeros, TblReservasDetalle, TblCotizacion, TblLiquiCliente, TblLiquiProve, TblFactura>
        //    (ViewBag.DetalleSolicitud, new TblPasajeros(), new TblReservasDetalle(), new TblCotizacion(), new TblLiquiCliente(), new TblLiquiProve(), new TblFactura());


        //    ViewBag.Caramelo = "Solicitud 000X";
        //    return View("~/Views/Solicitudes/Detalle2.cshtml", tuplita);
        //}

        public async Task<ActionResult> Editar2(int id) {

            var soli = await ctx.TblSolicitud.Include("Destino").Include("Cliente").FirstOrDefaultAsync(i => i.Id == id);
            //ViewBag.DetallePasaj = await ctx.TblPasajeros.Include("TipoPasajero").Include("TipoDocumentoIdentidad").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleCoti = await ctx.TblCotizacion.Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleReserva = await ctx.TblReservasDetalle.Include("TipoServicio").Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleLiCliente = await ctx.TblLiquiCliente.Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleLiProveedor = await ctx.TblLiquiProve.Include("Proveedor").Where(i => i.SolicitudId == id).ToListAsync();
            ViewBag.DetalleFact = await ctx.TblFactura.Where(i => i.SolicitudId == id).ToListAsync();

            ViewData["Destinos"] = new SelectList(ctx.TblDestino, "Id", "Nombre");

            //ViewData["TPasaj"] = new SelectList(ctx.TblTipoPasajero, "Id", "Descripcion");

            ViewData["TPasaj"] = Metodos.LlenarBagPasajero(soli.Cntadulto,soli.Cntnino,soli.Cntinfa,(List<TblPasajeros>)ViewBag.DetallePasaj);

            ViewData["Documentos"] = new SelectList(ctx.TblTipoDocumentoIdentidad, "Id", "Descripcion");
            ViewData["Servicios"] = new SelectList(ctx.TblTipoServicio, "Id", "Descripcion");
            ViewData["Proveedores"] = new SelectList(ctx.TblProveedor, "Id", "Nombre");

            //Habilitar pasajero principal
            ViewBag.PasaPrincipal = Metodos.NumePrincipales((List<TblPasajeros>)ViewBag.DetallePasaj);

            VmFile file = new VmFile
            {
                solicitud = soli,
                pasajero = new TblPasajeros(),
                coti = new VCotizacion(),
                Reser = new TblReservasDetalle(),
                Lcliente = new VLicliente(),
                Lprove = new VLiprovee(),
                Fact = new VFactura()
            };
            
            return View("~/Views/Solicitudes/Editar2.cshtml",file);
        }

        public async Task<JsonResult> NuevaDreserva2([Bind(Prefix = "Reser")] TblReservasDetalle DeReserva) {
            string msg = "";
            bool rpt;
            if (!ModelState.IsValid)
            {
                rpt = false;
                msg = "Rellene los campos solicitados Detalle reserva";
                return Json(
                new 
                {
                    rpt = rpt,
                    msg= msg
                });
            }

            if (DeReserva.Id == 0 ) //Registrar Reserva
            {
                ctx.TblReservasDetalle.Add(DeReserva);
                DeReserva.Proveedor = await ctx.TblProveedor.FindAsync(DeReserva.ProveedorId);//Datos para mostrar en la tabla
                DeReserva.TipoServicio = await ctx.TblTipoServicio.FindAsync(DeReserva.TipoServicioId);//**
            }
            else //Actualizar Reserva
            {
                ctx.TblReservasDetalle.Attach(DeReserva);
                ctx.Entry(DeReserva).State = EntityState.Modified;
                DeReserva.Proveedor = await ctx.TblProveedor.FindAsync(DeReserva.ProveedorId);//Datos para mostrar en la tabla
                DeReserva.TipoServicio = await ctx.TblTipoServicio.FindAsync(DeReserva.TipoServicioId);//**
            }
            await ctx.SaveChangesAsync();

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = DeReserva
            });

        }

        public async Task<JsonResult> GetDetalleReserva(int id, string tipo)
        {
            string msg = "";
            bool rpt;
            TblReservasDetalle DeReserva = null;

            try
            {
                if (tipo.Equals("edit"))
                {
                    DeReserva = await ctx.TblReservasDetalle.FirstOrDefaultAsync(i => i.Id == id);
                    //DeReserva = await ctx.TblReservasDetalle.Include("TipoServicio").Include("Proveedor").FirstOrDefaultAsync(i => i.Id == id);
                    HttpContext.Session.SetObjectAsJson("DeReserva", DeReserva); //EL METODO SETOBJECASJSON ES DE UNA CLASE QUE LO EXTIENDE que yo cree
                }
                else if (tipo.Equals("del"))
                {
                    DeReserva = await ctx.TblReservasDetalle.FirstOrDefaultAsync(i => i.Id == id);
                    ctx.TblReservasDetalle.Remove(DeReserva);
                    ctx.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                return Json(
                new
                {
                    rpt = false,
                    msg = ex.Message,
                });
            }

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = DeReserva
            });

        }

        public IActionResult RfReserva()//LLamado de la vista para actualizar los datos
        {

            TblReservasDetalle reserva = HttpContext.Session.GetObjectFromJson<TblReservasDetalle>("DeReserva");
            VmFile file = new VmFile { Reser = reserva };
            //ViewBag.DetalleSolicitud = ViewBag.DetalleSolicitud =  ctx.TblSolicitud.Include("Destino").Include("Cliente").FirstOrDefault(i => i.Id == valor);
            ViewData["Servicios"] = new SelectList(ctx.TblTipoServicio, "Id", "Descripcion");
            ViewData["Proveedores"] = new SelectList(ctx.TblProveedor, "Id", "Nombre");
            HttpContext.Session.Remove("DeReserva");

            return View("~/Views/Solicitudes/File/Reservas.cshtml", file);
        }

        public async Task<JsonResult> NuevaCotiza([Bind(Prefix = "coti")] VCotizacion coti)
        {
            string msg = "";
            bool rpt;
            TblCotizacion tblcoti;
            if (!ModelState.IsValid)
            {
                rpt = false;
                msg = "Rellene los campos solicitados de la cotizacion";
                return Json(
                new
                {
                    rpt = rpt,
                    msg = msg
                });
            }

            if (coti.Id == 0) //Registrar Reserva
            {
                 tblcoti = new TblCotizacion(coti); //Save/Guardar cotizacion
                if (coti.DocCoti != null)
                {
                    tblcoti.Documentonombre = coti.DocCoti.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tblcoti.Documento = Metodos.GetByteArray(coti.DocCoti); //Pasamos el iform a varchar y guardamos
                }
                ctx.TblCotizacion.Add(tblcoti);
            }
            else //Actualizar Reserva
            {
                tblcoti = await ctx.TblCotizacion.FindAsync(coti.Id); //Variable para actualizar el modelo de BD
                coti.SolicitudId = tblcoti.SolicitudId; //PUESTO QUE ESTAS VISTAS SON DIFERENTES, SOLAMENTE LE PASAMOS LA PROPIEDAD PARA QUE PUEDA SER ENVIADA EN EL RETURN

                tblcoti.FechaCotizacion = coti.Fecha; //En adelante capturamos los datos de la vista para act el modelo
                tblcoti.NumeCotizacion = coti.NumeCotizacion;
                tblcoti.Monto = coti.Monto;
                if (coti.DocCoti != null)
                {
                    tblcoti.Documentonombre = coti.DocCoti.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tblcoti.Documento = Metodos.GetByteArray(coti.DocCoti); //Pasamos el iform a varchar y guardamos
                }
            }
            await ctx.SaveChangesAsync();

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = tblcoti
            });

        }

        public async Task<JsonResult> GetDetalleCoti(int id, string tipo)
        {
            string msg = "";
            bool rpt;
            TblCotizacion Decoti = null;
            VCotizacion Vcoti = null;
            object ob;

            try
            {
                if (tipo.Equals("edit"))
                {
                    var DCotiz = await ctx.TblCotizacion.FirstOrDefaultAsync(i => i.Id == id); //Detalles de la base de datos cotizacion
                    Vcoti = new VCotizacion(DCotiz); //Vista empleada para actualizar/editar

                    HttpContext.Session.SetObjectAsJson("DeCoti", Vcoti); //EL METODO SETOBJECASJSON ES DE UNA CLASE QUE LO EXTIENDE que yo cree
                }
                else if (tipo.Equals("del"))
                {
                    Decoti = await ctx.TblCotizacion.FirstOrDefaultAsync(i => i.Id == id); //File Cotizacion
                    ctx.TblCotizacion.Remove(Decoti);
                    ctx.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                return Json(
                new
                {
                    rpt = false,
                    msg = ex.Message,
                });
            }

            if (Decoti != null)
            {
                ob = Decoti;
            }
            else
            {
                ob = Vcoti;
            }

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = ob
            });

        }

        public IActionResult RfCoti()//LLamado de la vista para actualizar los datos
        {

            VCotizacion vcoti = HttpContext.Session.GetObjectFromJson<VCotizacion>("DeCoti");
            VmFile file = new VmFile { coti = vcoti };

            HttpContext.Session.Remove("DeCoti");

            return View("~/Views/Solicitudes/File/Cotizacion.cshtml", file);
        }

        public async Task<JsonResult> NuevoPasajero([Bind(Prefix = "pasajero")] TblPasajeros DePasajero)
        {
            string msg = "";
            bool rpt;
            if (!ModelState.IsValid)
            {
                rpt = false;
                msg = "Rellene los campos solicitados Detalle pasajero";
                return Json(
                new
                {
                    rpt = rpt,
                    msg = msg
                });
            }

            if (DePasajero.Id == 0) //Registrar Pasjro
            {
                ctx.TblPasajeros.Add(DePasajero);
                DePasajero.TipoPasajero = await ctx.TblTipoPasajero.FindAsync(DePasajero.TipoPasajeroId);//Datos para mostrar en la tabla
                DePasajero.TipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.FindAsync(DePasajero.TipoDocumentoIdentidadId);//**
            }
            else //Actualizar Pasjro
            {
                ctx.TblPasajeros.Attach(DePasajero);
                ctx.Entry(DePasajero).State = EntityState.Modified;
                DePasajero.TipoPasajero = await ctx.TblTipoPasajero.FindAsync(DePasajero.TipoPasajeroId);//Datos para mostrar en la tabla
                DePasajero.TipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.FindAsync(DePasajero.TipoDocumentoIdentidadId);//**
            }
            //guardar en sesion dato tanto despues de guardar como despues de actualizar un registro existente
            //Se guarda el id de la solicitud por que la vista pasajeros requiere la propiedad id del modelo dato
            //HttpContext.Session.SetInt32("IdSol", (int)DePasajero.SolicitudId); //SE DESACTIVA LO DEL PASAJERO POR QUE YA NO ES EL ID

            await ctx.SaveChangesAsync();

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = DePasajero
            });

        }

        public async Task<JsonResult> GetDetallePasa(int id, string tipo)
        {
            string msg = "";
            bool rpt;
            TblPasajeros DePasajero = null;

            try
            {
                if (tipo.Equals("edit"))
                {
                    DePasajero = await ctx.TblPasajeros.FirstOrDefaultAsync(i => i.Id == id);
                    HttpContext.Session.SetObjectAsJson("DePasa", DePasajero); //EL METODO SETOBJECASJSON ES DE UNA CLASE QUE LO EXTIENDE que yo cree
                }
                else if (tipo.Equals("del"))
                {
                    DePasajero = await ctx.TblPasajeros.FirstOrDefaultAsync(i => i.Id == id);
                    ctx.TblPasajeros.Remove(DePasajero);
                    //HttpContext.Session.SetInt32("IdSol", (int)DePasajero.SolicitudId); //CARGAMOS NUEVAMENTE EL ID SOLICITUD EN SESION
                    ctx.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                return Json(
                new
                {
                    rpt = false,
                    msg = ex.Message,
                });
            }

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = DePasajero
            });

        }

        public async Task<IActionResult> RfPasajero(string tipo)//LLamado de la vista para actualizar los datos
        {
            TblSolicitud soli ;
            List<TblPasajeros> dpasajeros;
            ViewData["Documentos"] = new SelectList(ctx.TblTipoDocumentoIdentidad, "Id", "Descripcion");

            if (tipo.Equals("new"))
            {
                var id = HttpContext.Session.GetInt32("IdSol");
                soli = await ctx.TblSolicitud.Include("Destino").Include("Cliente").FirstOrDefaultAsync(i => i.Id == id);
                //dpasajeros = await ctx.TblPasajeros.Include("TipoPasajero").Include("TipoDocumentoIdentidad").Where(i => i.SolicitudId == id).ToListAsync(); //SE DESACTIVA

                VmFile file = new VmFile { solicitud = soli , pasajero = new TblPasajeros() };

                //ViewBag.PasaPrincipal = Metodos.NumePrincipales(dpasajeros);

                //ViewData["TPasaj"] = Metodos.LlenarBagPasajero(soli.Cntadulto, soli.Cntnino, soli.Cntinfa, dpasajeros);
                HttpContext.Session.Remove("IdSol");

                return View("~/Views/Solicitudes/File/Pasajeros.cshtml", file);
            }
            else 
            {
                TblPasajeros pasa = HttpContext.Session.GetObjectFromJson<TblPasajeros>("DePasa");
                VmFile file = new VmFile { pasajero = pasa };

                //SE DESACTIVAN
                //soli = await ctx.TblSolicitud.Include("Destino").Include("Cliente").FirstOrDefaultAsync(i => i.Id == pasa.SolicitudId);
                //dpasajeros = await ctx.TblPasajeros.Include("TipoPasajero").Include("TipoDocumentoIdentidad").Where(i => i.SolicitudId == pasa.SolicitudId).ToListAsync();

                //ViewBag.PasaPrincipal = Metodos.NumePrincipales(dpasajeros);
                ViewBag.PasaPrincipal = Metodos.ValidarPasaPrincipal(pasa, (bool)ViewBag.PasaPrincipal);

                ViewData["TPasaj"] = Metodos.LlenarBagPasajeroSpecific((int)pasa.TipoPasajeroId);
                HttpContext.Session.Remove("DePasa");

                return View("~/Views/Solicitudes/File/Pasajeros.cshtml", file);
            }
            
        }

        public async Task<JsonResult> NuevaLicli([Bind(Prefix = "Lcliente")] VLicliente LiCliente)
        {
            string msg = "";
            bool rpt;
            TblLiquiCliente tbllicli;
            if (!ModelState.IsValid)
            {
                rpt = false;
                msg = "Rellene los campos solicitados de la cotizacion";
                return Json(
                new
                {
                    rpt = rpt,
                    msg = msg
                });
            }

            if (LiCliente.Id == 0) //Registrar Licli
            {
                tbllicli = new TblLiquiCliente(LiCliente); //Save/Guardar Licli
                if (LiCliente.Doc != null)
                {
                    tbllicli.Documentonombre = LiCliente.Doc.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tbllicli.Documento = Metodos.GetByteArray(LiCliente.Doc); //Pasamos el iform a varchar y guardamos
                }
                ctx.TblLiquiCliente.Add(tbllicli);
            }
            else //Actualizar Reserva
            {
                tbllicli = await ctx.TblLiquiCliente.FindAsync(LiCliente.Id); //Variable para actualizar el modelo de BD

                tbllicli.FechaLcliente = LiCliente.FechaLcliente;
                tbllicli.FormaPago = LiCliente.FormaPago;
                tbllicli.Banco = LiCliente.Banco;
                tbllicli.Titular = LiCliente.Titular;
                tbllicli.NumeTarjeta = LiCliente.NumeTarjeta;
                tbllicli.NumeVouch = LiCliente.NumeVouch;
                tbllicli.Monto = LiCliente.Monto;

                if (LiCliente.Doc != null)
                {
                    tbllicli.Documentonombre = LiCliente.Doc.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tbllicli.Documento = Metodos.GetByteArray(LiCliente.Doc); //Pasamos el iform a varchar y guardamos
                }
            }
            await ctx.SaveChangesAsync();

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = tbllicli
            });

        }

        public async Task<JsonResult> GetDetalleLicli(int id, string tipo)
        {
            string msg = "";
            bool rpt;
            TblLiquiCliente Delicli = null;
            VLicliente Vlicli = null;
            object ob;

            try
            {
                if (tipo.Equals("edit"))
                {
                    Delicli = await ctx.TblLiquiCliente.FirstOrDefaultAsync(i => i.Id == id); //Detalles de la base de datos liqui
                    Vlicli = new VLicliente(Delicli); //Vista empleada para actualizar/editar

                    HttpContext.Session.SetObjectAsJson("DeLicli", Vlicli); //EL METODO SETOBJECASJSON ES DE UNA CLASE QUE LO EXTIENDE que yo cree
                }
                else if (tipo.Equals("del"))
                {
                    Delicli = await ctx.TblLiquiCliente.FirstOrDefaultAsync(i => i.Id == id); //File Cotizacion
                    ctx.TblLiquiCliente.Remove(Delicli);
                    ctx.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                return Json(
                new
                {
                    rpt = false,
                    msg = ex.Message,
                });
            }

            if (Delicli != null)
            {
                ob = Delicli;
            }
            else
            {
                ob = Vlicli;
            }

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = ob
            });

        }

        public IActionResult RfLicli()//LLamado de la vista para actualizar los datos
        {

            VLicliente vlicli = HttpContext.Session.GetObjectFromJson<VLicliente>("DeLicli");
            VmFile file = new VmFile { Lcliente = vlicli };

            HttpContext.Session.Remove("DeLicli");

            return View("~/Views/Solicitudes/File/LiCliente.cshtml", file);
        }

        public async Task<JsonResult> NuevaLipro([Bind(Prefix = "Lprove")] VLiprovee LiProveedor)
        {
            string msg = "";
            bool rpt;
            TblLiquiProve tbllipro;
            if (!ModelState.IsValid)
            {
                rpt = false;
                msg = "Rellene los campos solicitados de la cotizacion";
                return Json(
                new
                {
                    rpt = rpt,
                    msg = msg
                });
            }

            if (LiProveedor.Id == 0) //Registrar Reserva
            {
                tbllipro = new TblLiquiProve(LiProveedor); //Save/Guardar cotizacion
                if (LiProveedor.Doc != null)
                {
                    tbllipro.Documentonombre = LiProveedor.Doc.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tbllipro.Documento = Metodos.GetByteArray(LiProveedor.Doc); //Pasamos el iform a varchar y guardamos
                }
                ctx.TblLiquiProve.Add(tbllipro);
                tbllipro.Proveedor = await ctx.TblProveedor.FindAsync(LiProveedor.ProveedorId);//Datos para mostrar en la tabla
            }
            else //Actualizar Reserva
            {
                tbllipro = await ctx.TblLiquiProve.FindAsync(LiProveedor.Id); //Variable para actualizar el modelo de BD

                tbllipro.ProveedorId = LiProveedor.ProveedorId;
                tbllipro.FechaLprove = LiProveedor.FechaLprove;
                tbllipro.FormaPago = LiProveedor.FormaPago;
                tbllipro.Banco = LiProveedor.Banco;
                tbllipro.Titular = LiProveedor.Titular;
                tbllipro.NumeVaucher = LiProveedor.NumeVaucher;
                tbllipro.NumeTarjeta = LiProveedor.NumeTarjeta;
                tbllipro.PagoCliente = LiProveedor.PagoCliente;
                tbllipro.Monto = LiProveedor.Monto;

                if (LiProveedor.Doc != null)
                {
                    tbllipro.Documentonombre = LiProveedor.Doc.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tbllipro.Documento = Metodos.GetByteArray(LiProveedor.Doc); //Pasamos el iform a varchar y guardamos
                }
                tbllipro.Proveedor = await ctx.TblProveedor.FindAsync(LiProveedor.ProveedorId);//Datos para mostrar en la tabla
            }
            await ctx.SaveChangesAsync();

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = tbllipro
            });

        }

        public async Task<JsonResult> GetDetalleLipro(int id, string tipo)
        {
            string msg = "";
            bool rpt;
            TblLiquiProve Delipro = null;
            VLiprovee Vlipro = null;
            object ob;

            try
            {
                if (tipo.Equals("edit"))
                {
                    Delipro = await ctx.TblLiquiProve.FirstOrDefaultAsync(i => i.Id == id); //Detalles de la base de datos cotizacion
                    Vlipro = new VLiprovee(Delipro); //Vista empleada para actualizar/editar

                    HttpContext.Session.SetObjectAsJson("DeLipro", Vlipro); //EL METODO SETOBJECASJSON ES DE UNA CLASE QUE LO EXTIENDE que yo cree
                }
                else if (tipo.Equals("del"))
                {
                    Delipro = await ctx.TblLiquiProve.FirstOrDefaultAsync(i => i.Id == id); //File Cotizacion
                    ctx.TblLiquiProve.Remove(Delipro);
                    ctx.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                return Json(
                new
                {
                    rpt = false,
                    msg = ex.Message,
                });
            }

            if (Delipro != null)
            {
                ob = Delipro;
            }
            else
            {
                ob = Vlipro;
            }

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = ob
            });

        }

        public IActionResult RfLipro()//LLamado de la vista para actualizar los datos
        {

            VLiprovee vlipro = HttpContext.Session.GetObjectFromJson<VLiprovee>("DeLipro");
            VmFile file = new VmFile { Lprove = vlipro };
            ViewData["Proveedores"] = new SelectList(ctx.TblProveedor, "Id", "Nombre");
            HttpContext.Session.Remove("DeLipro");

            return View("~/Views/Solicitudes/File/LiProveedor.cshtml", file);
        }

        public async Task<JsonResult> NuevaFact([Bind(Prefix = "Fact")] VFactura fac)
        {
            string msg = "";
            bool rpt;
            TblFactura tblfact;
            if (!ModelState.IsValid)
            {
                rpt = false;
                msg = "Rellene los campos solicitados de la cotizacion";
                return Json(
                new
                {
                    rpt = rpt,
                    msg = msg
                });
            }

            if (fac.Id == 0) //Registrar Reserva
            {
                tblfact = new TblFactura(fac); //Save/Guardar cotizacion
                if (fac.DocFact != null)
                {
                    tblfact.Documentonombre = fac.DocFact.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tblfact.Documento = Metodos.GetByteArray(fac.DocFact); //Pasamos el iform a varchar y guardamos
                }
                ctx.TblFactura.Add(tblfact);
            }
            else //Actualizar Reserva
            {
                tblfact = await ctx.TblFactura.FindAsync(fac.Id); //Variable para actualizar el modelo de BD

                tblfact.FechaFactu = fac.Fecha; //En adelante capturamos los datos de la vista para act el modelo
                tblfact.NumeFactu = fac.NumeFactu;
                tblfact.Monto = fac.Monto;

                if (fac.DocFact != null)
                {
                    tblfact.Documentonombre = fac.DocFact.FileName; //Aqui se le asigna el nombre al doc remplar si gustas con otro field
                    tblfact.Documento = Metodos.GetByteArray(fac.DocFact); //Pasamos el iform a varchar y guardamos
                }
            }
            await ctx.SaveChangesAsync();

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = tblfact
            });

        }

        public async Task<JsonResult> GetDetalleFact(int id, string tipo)
        {
            string msg = "";
            bool rpt;
            TblFactura Defact = null;
            VFactura Vfact = null;
            object ob;

            try
            {
                if (tipo.Equals("edit"))
                {
                    Defact = await ctx.TblFactura.FirstOrDefaultAsync(i => i.Id == id); //Detalles de la base de datos cotizacion
                    Vfact = new VFactura(Defact); //Vista empleada para actualizar/editar

                    HttpContext.Session.SetObjectAsJson("DeFact", Vfact); //EL METODO SETOBJECASJSON ES DE UNA CLASE QUE LO EXTIENDE que yo cree
                }
                else if (tipo.Equals("del"))
                {
                    Defact = await ctx.TblFactura.FirstOrDefaultAsync(i => i.Id == id); //File Cotizacion
                    ctx.TblFactura.Remove(Defact);
                    ctx.SaveChanges();
                }

            }
            catch (Exception ex)
            {

                return Json(
                new
                {
                    rpt = false,
                    msg = ex.Message,
                });
            }

            if (Defact != null)
            {
                ob = Defact;
            }
            else
            {
                ob = Vfact;
            }

            return Json(
            new
            {
                rpt = true,
                msg = "Paso Correctamente",
                data = ob
            });

        }

        public IActionResult RfFact()//LLamado de la vista para actualizar los datos
        {

            VFactura vfact = HttpContext.Session.GetObjectFromJson<VFactura>("DeFact");
            VmFile file = new VmFile { Fact = vfact };

            HttpContext.Session.Remove("DeFact");

            return View("~/Views/Solicitudes/File/Facturacion.cshtml", file);
        }

    }
}
