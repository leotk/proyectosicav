﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class ClientesController : Controller
    {
        UsersContext ctx;

        public ClientesController(UsersContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<IActionResult> Index()
        {
            List<TblCliente> Clientes = await ctx.TblCliente.Include("TipoCliente").ToListAsync();
            Clientes = await ctx.TblCliente.Include("TipoDocumentoIdentidad").ToListAsync();
            TblCliente Cli = new TblCliente();
            ViewBag.Cliente = Clientes;
            ViewBag.TblTipoCliente = await ctx.TblTipoCliente.OrderBy(x => x.Descripcion).ToListAsync();
            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();

            return View(Cli);
        }


        [BindProperty]
        public TblCliente Cliente { get; set; }
        public async Task<IActionResult> GuardarCliente()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var _Cliente = await ctx.TblCliente.Where(x => x.Id == Cliente.Id).AnyAsync();

            if (!_Cliente)
            {
                ctx.TblCliente.Add(Cliente);
            }
            else
            {
                ctx.TblCliente.Attach(Cliente);
                ctx.Entry(Cliente).State = EntityState.Modified;
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");

        }


  

        public async Task<IActionResult> Modificar(int id)
        {
            var Cliente = await ctx.TblCliente.FindAsync(id);

            if (Cliente == null)
            {
                return RedirectToAction("Index");
            }


            ViewBag.TblTipoCliente = await ctx.TblTipoCliente.OrderBy(x => x.Descripcion).ToListAsync();
            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();

            ViewBag.Cliente =  await ctx.TblCliente.Include("TipoCliente").ToListAsync();


            return View(Cliente);

        }

        public async Task<IActionResult> Eliminar(int id)
        {
            var _Cliente = await ctx.TblCliente.FindAsync(id);

            if (_Cliente == null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ctx.TblCliente.Remove(_Cliente);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        } 


    }
}