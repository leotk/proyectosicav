﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class AjaxController : Controller
    {
        private long fileSizeLimit = 2097152;

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public string ValidarArchivo()
        //{
        //    Dictionary<string, string> valores = new Dictionary<string, string>();
        //    String valor, span = "";

        //    try
        //    {
        //        IFormFile file = Request.Form.Files[0];
        //        if (file.Length != 0)
        //        {
        //            if (!Metodos.PermitedExten(file))
        //            {
        //                span = "<span style=\"color:red\">Este tipo de archivo no es permitido</span>";
        //                valores.Add("span", span);
        //                valores.Add("bool", "false");
        //            }
        //            else if (!Metodos.FileSize(file, fileSizeLimit))
        //            {
        //                //var megaby = fileSizeLimit / 1048576;
        //                //span = "<span style=\"color:red\">El archivo excede el maximo de 2MB por: "+ megaby + "MB</span>";
        //                span = "<span style=\"color:red\">El archivo excede el maximo de 2MB </span>";
        //                valores.Add("span", span);
        //                valores.Add("bool", "false");
        //            }
        //            else
        //            {
        //                span = "<span style=\"color:green\">El archivo se cargo correctamente</span>";
        //                valores.Add("span", span);
        //                valores.Add("bool", "true");
        //            }
        //        }
        //        else
        //        {
        //            span = "<span style=\"color:red\">Error no se puede cargar un archivo vacio</span>";
        //            valores.Add("span", span);
        //            valores.Add("bool", "false");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        span = "<span style=\"color:red\">Error no se cargo ningun archivo </span>";
        //        valores.Add("span", span);
        //        valores.Add("bool", "false");
        //    }

        //    valor = JsonConvert.SerializeObject(valores, Formatting.Indented);

        //    return valor;
        //}

        public JsonResult  ValidarArchivo2()
        {
            //bool rpt;
            string msg = "";
            bool value=false; //REPRESENTA SI PASA O NO EL ARCHIVO CON ESTO COMPARAMOS EN EL AJX DEL HTML
            string span="";

            try
            {
                var request = Request.Form.Files;
                IFormFile file = Request.Form.Files[0];
                if (file.Length != 0)
                {
                    if (!Metodos.PermitedExten(file))
                    {
                        span = "<span style=\"color:red\">Este tipo de archivo no es permitido</span>";
                    }
                    else if (!Metodos.FileSize(file, fileSizeLimit))
                    {
                        span = "<span style=\"color:red\">El archivo excede el maximo de 2MB </span>";
                    }
                    else
                    {
                        span = "<span style=\"color:green\">El archivo se cargo correctamente</span>";
                        value = true;
                    }
                }
                else
                {
                    span = "<span style=\"color:red\">Error no se puede cargar un archivo vacio</span>";
                }
            }
            catch (Exception ex)
            {
                span = "<span style=\"color:red\">Error no se cargo ningun archivo </span>";
                return Json(
                new
                {
                    //rpt = false,
                    msg = "ERROR: "+ex.Message,
                    value = value,
                    span = span
                });
            }

            return Json(
            new
            {
                //rpt = true,
                msg = "Se ejecuto",
                value = value,
                span = span
            });
        }

    }
}