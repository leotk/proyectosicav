﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using ProyectoWsicav.Models;
using ProyectoWsicav.ViewModel;

namespace ProyectoWsicav.Controllers
{
    public class BibliaController : Controller
    {
        private readonly UsersContext _context;

        public BibliaController(UsersContext context)
        {
            _context = context;
        }

        // GET: Biblia
        public async Task<IActionResult> Index()
        {

            // var c = _context.TblDestino.Include(t => t.TblCiudad);

            List<VmBiblia> list = new List<VmBiblia>();
            var v = (from a in _context.viw_Biblia
                     join b in _context.TblGuia on a.GuiaId equals b.Id
                     join c in _context.TblVehiculo on a.VehiculoId equals c.Id
                     select new VmBiblia
                     {
                         Id = a.Id,
                         NombresPasajeros = a.NombresPasajeros,
                         Files = a.Files,
                         nPasajeros = a.nPasajeros,
                         idioma = a.idioma,
                         Agencia = a.Agencia,
                         hora = a.hora,
                         Servicios = a.Servicios,
                         TipoServicio = a.TipoServicio,
                         Hotel = a.Hotel,
                         GuiaId = a.GuiaId,
                         VehiculoId = a.VehiculoId,
                         Observaciones = a.Observaciones,
                         FechaIn = a.FechaIn,
                         Guia = b.Nombres,
                         Movilidad = c.Marca,
                         ReservaDetalleId = a.ReservaDetalleId

                     }); ;
            list = await v.ToListAsync();
            return View(list);


        }
        [HttpPost]
        public async Task<IActionResult> Index(string Desde, string Hasta)
        {
            if (Desde == null || Desde == "")
            {
                Desde = "02/02/2000";



            }
            if (Hasta == null || Hasta == "")
            {
                Hasta = "02/02/3000";
                


            }
            // var 
            // var c = _context.TblDestino.Include(t => t.TblCiudad);

            List<VmBiblia> list = new List<VmBiblia>();
            var v = (from a in _context.viw_Biblia
                     join b in _context.TblGuia on a.GuiaId equals b.Id
                     join c in _context.TblVehiculo on a.VehiculoId equals c.Id
                     select new VmBiblia
                     {
                         Id = a.Id,
                         NombresPasajeros = a.NombresPasajeros,
                         Files = a.Files,
                         nPasajeros = a.nPasajeros,
                         idioma = a.idioma,
                         Agencia = a.Agencia,
                         hora = a.hora,
                         Servicios = a.Servicios,
                         TipoServicio = a.TipoServicio,
                         Hotel = a.Hotel,
                         GuiaId = a.GuiaId,
                         VehiculoId = a.VehiculoId,
                         Observaciones = a.Observaciones,
                         FechaIn = a.FechaIn,
                         Guia = b.Nombres,
                         Movilidad = c.Marca,
                         ReservaDetalleId = a.ReservaDetalleId

                     }); ;
            DateTime desde = Convert.ToDateTime(Desde);
            DateTime hasta = Convert.ToDateTime(Hasta);
            list = await v.Where(a=>a.FechaIn>=desde&&a.FechaIn<=hasta).ToListAsync();
            return View(list);


        }

        [HttpPost]
        public ActionResult saveuser(int id, string propertyName, string value)
        {
            var status = false;
            var message = "";

            //Update data to database 

            var user = _context.TblReservasDetalle.Find(id);

            object updateValue = value;
            bool isValid = true;

            if (propertyName == "GuiaId")
            {
                int newRoleID = 0;
                if (int.TryParse(value, out newRoleID))
                {
                    updateValue = newRoleID;
                    //Update value field
                    value = _context.TblGuia.Where(a => a.Id == newRoleID).First().Nombres;
                }
                else
                {
                    isValid = false;
                }

            }
            if (propertyName == "VehiculoId")
            {
                int newRoleID = 0;
                if (int.TryParse(value, out newRoleID))
                {
                    updateValue = newRoleID;
                    //Update value field
                    value = _context.TblVehiculo.Where(a => a.Id == newRoleID).First().Marca;
                }
                else
                {
                    isValid = false;
                }

            }
            //else if (propertyName == "DOB")
            //{
            //    DateTime dob;
            //    if (DateTime.TryParseExact(value, "dd-MM-yyyy", new CultureInfo("en-US"), DateTimeStyles.None, out dob))
            //    {
            //        updateValue = dob;
            //    }
            //    else
            //    {
            //        isValid = false;
            //    }
            //}

            if (user != null && isValid)
            {
                _context.Entry(user).Property(propertyName).CurrentValue = updateValue;
                _context.SaveChanges();
                status = true;
            }
            else
            {
                message = "Error!";
            }


            var response = new { value = value, status = status, message = message };
            JObject o = JObject.FromObject(response);
            return Content(o.ToString());
        }

        public ActionResult GetGuia(int id)
        {
            //{'E':'Letter E','F':'Letter F','G':'Letter G', 'selected':'F'}
            int selectedRoleID = 0;
            StringBuilder sb = new StringBuilder();

            var roles = _context.TblGuia.OrderBy(a => a.Nombres).ToList();
            foreach (var item in roles)
            {
                sb.Append(string.Format("'{0}':'{1}',", item.Id, item.Nombres));
            }

            selectedRoleID = _context.TblGuia.Where(a => a.Id == id).First().Id;



            sb.Append(string.Format("'selected': '{0}'", selectedRoleID));
            return Content("{" + sb.ToString() + "}");
        }

        public ActionResult GetVehiculo(int id)
        {
            //{'E':'Letter E','F':'Letter F','G':'Letter G', 'selected':'F'}
            int selectedRoleID = 0;
            StringBuilder sb = new StringBuilder();

            var roles = _context.TblVehiculo.OrderBy(a => a.Marca).ToList();
            foreach (var item in roles)
            {
                sb.Append(string.Format("'{0}':'{1}',", item.Id, item.Marca));
            }

            selectedRoleID = _context.TblVehiculo.Where(a => a.Id == id).First().Id;



            sb.Append(string.Format("'selected': '{0}'", selectedRoleID));
            return Content("{" + sb.ToString() + "}");
        }



        // GET: Biblia/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viw_Biblia = await _context.viw_Biblia
                .FirstOrDefaultAsync(m => m.Id == id);
            if (viw_Biblia == null)
            {
                return NotFound();
            }

            return View(viw_Biblia);
        }

        // GET: Biblia/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Biblia/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NombresPasajeros,Files,nPasajeros,idioma,Agencia,hora,Servicios,Hotel,Guia,Movilidad,Observaciones,FechaIn")] viw_Biblia viw_Biblia)
        {
            if (ModelState.IsValid)
            {
                _context.Add(viw_Biblia);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(viw_Biblia);
        }

        // GET: Biblia/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viw_Biblia = await _context.viw_Biblia.FindAsync(id);
            if (viw_Biblia == null)
            {
                return NotFound();
            }
            return View(viw_Biblia);
        }

        // POST: Biblia/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NombresPasajeros,Files,nPasajeros,idioma,Agencia,hora,Servicios,Hotel,Guia,Movilidad,Observaciones,FechaIn")] viw_Biblia viw_Biblia)
        {
            if (id != viw_Biblia.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(viw_Biblia);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!viw_BibliaExists(viw_Biblia.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(viw_Biblia);
        }

        // GET: Biblia/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var viw_Biblia = await _context.viw_Biblia
                .FirstOrDefaultAsync(m => m.Id == id);
            if (viw_Biblia == null)
            {
                return NotFound();
            }

            return View(viw_Biblia);
        }

        // POST: Biblia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var viw_Biblia = await _context.viw_Biblia.FindAsync(id);
            _context.viw_Biblia.Remove(viw_Biblia);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool viw_BibliaExists(int id)
        {
            return _context.viw_Biblia.Any(e => e.Id == id);
        }
    }
}
