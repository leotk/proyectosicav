
using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class GuiasController : Controller
    {
        UsersContext ctx;

        public GuiasController(UsersContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<IActionResult> Index()
        {
            List<TblGuia> Guias = await ctx.TblGuia.Include("TipoDocumentoIdentidad").ToListAsync();
            TblGuia Gui = new TblGuia();
            ViewBag.Guia = Guias;
            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();

            return View(Gui);
        }

        [BindProperty]
        public TblGuia Guia { get; set; }
        public async Task<IActionResult> GuardarGuia()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var _Guia = await ctx.TblGuia.Where(x => x.Id == Guia.Id).AnyAsync();

            if (!_Guia)
            {
                ctx.TblGuia.Add(Guia);
            }
            else
            {
                ctx.TblGuia.Attach(Guia);
                ctx.Entry(Guia).State = EntityState.Modified;
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        public async Task<IActionResult> Modificar(int id)
        {
            var Guia = await ctx.TblGuia.FindAsync(id);

            if (Guia == null)
            {
                return RedirectToAction("Index");
            }


            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();

            ViewBag.Guia = await ctx.TblGuia.Include("TipoDocumentoIdentidad").ToListAsync();


            return View(Guia);

        }

        public async Task<IActionResult> Eliminar(int id)
        {
            var _Guia = await ctx.TblGuia.FindAsync(id);

            if (_Guia == null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ctx.TblGuia.Remove(_Guia);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        }

    }
}