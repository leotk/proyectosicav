﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    /*
    public class TblSolicitudsController : Controller
    {
        private readonly UsersContext _context;

        public TblSolicitudsController(UsersContext context)
        {
            _context = context;
        }

        // GET: TblSolicituds
        public async Task<IActionResult> Index()
        {
            var usersContext = _context.TblSolicitud.Include(t => t.Cliente).Include(t => t.Destino).Include(t => t.Estatus).Include(t => t.Usuario).Include(t => t.Vendedor);
            return View(await usersContext.ToListAsync());
        }

        // GET: TblSolicituds/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblSolicitud = await _context.TblSolicitud
                .Include(t => t.Cliente)
                .Include(t => t.Destino)
                .Include(t => t.Estatus)
                .Include(t => t.Usuario)
                .Include(t => t.Vendedor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblSolicitud == null)
            {
                return NotFound();
            }

            return View(tblSolicitud);
        }

        // GET: TblSolicituds/Create
        public IActionResult Create()
        {
            ViewData["ClienteId"] = new SelectList(_context.TblCliente, "Id", "Codigo");
            ViewData["DestinoId"] = new SelectList(_context.TblDestino, "Id", "Id");
            ViewData["EstatusId"] = new SelectList(_context.TblEstatus, "Id", "Color");
            ViewData["UsuarioId"] = new SelectList(_context.TblUsuario, "Id", "Apellido");
            ViewData["VendedorId"] = new SelectList(_context.TblVendedor, "Id", "Id");
            return View();
        }

        // POST: TblSolicituds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NumeroSolicitud,FechaSolicitud,UsuarioId,DestinoId,BoletoAereo,Hotel,Traslado,Excursion,Guias,SeguroViajes,AlquilerCarro,FechaIn,FechaOut,HoraVueloIda,HoraVueloRegreso,PersonaContacto,TelefonoContacto,CorreoContacto,ClienteId,EstatusId,Descripcion,VendedorId")] TblSolicitud tblSolicitud)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblSolicitud);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClienteId"] = new SelectList(_context.TblCliente, "Id", "Codigo", tblSolicitud.ClienteId);
            ViewData["DestinoId"] = new SelectList(_context.TblDestino, "Id", "Id", tblSolicitud.DestinoId);
            ViewData["EstatusId"] = new SelectList(_context.TblEstatus, "Id", "Color", tblSolicitud.EstatusId);
            ViewData["UsuarioId"] = new SelectList(_context.TblUsuario, "Id", "Apellido", tblSolicitud.UsuarioId);
            ViewData["VendedorId"] = new SelectList(_context.TblVendedor, "Id", "Id", tblSolicitud.VendedorId);
            return View(tblSolicitud);
        }

        // GET: TblSolicituds/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblSolicitud = await _context.TblSolicitud.FindAsync(id);
            if (tblSolicitud == null)
            {
                return NotFound();
            }
            ViewData["ClienteId"] = new SelectList(_context.TblCliente, "Id", "Codigo", tblSolicitud.ClienteId);
            ViewData["DestinoId"] = new SelectList(_context.TblDestino, "Id", "Id", tblSolicitud.DestinoId);
            ViewData["EstatusId"] = new SelectList(_context.TblEstatus, "Id", "Color", tblSolicitud.EstatusId);
            ViewData["UsuarioId"] = new SelectList(_context.TblUsuario, "Id", "Apellido", tblSolicitud.UsuarioId);
            ViewData["VendedorId"] = new SelectList(_context.TblVendedor, "Id", "Id", tblSolicitud.VendedorId);
            return View(tblSolicitud);
        }

        // POST: TblSolicituds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NumeroSolicitud,FechaSolicitud,UsuarioId,DestinoId,BoletoAereo,Hotel,Traslado,Excursion,Guias,SeguroViajes,AlquilerCarro,FechaIn,FechaOut,HoraVueloIda,HoraVueloRegreso,PersonaContacto,TelefonoContacto,CorreoContacto,ClienteId,EstatusId,Descripcion,VendedorId")] TblSolicitud tblSolicitud)
        {
            if (id != tblSolicitud.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblSolicitud);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblSolicitudExists(tblSolicitud.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClienteId"] = new SelectList(_context.TblCliente, "Id", "Codigo", tblSolicitud.ClienteId);
            ViewData["DestinoId"] = new SelectList(_context.TblDestino, "Id", "Id", tblSolicitud.DestinoId);
            ViewData["EstatusId"] = new SelectList(_context.TblEstatus, "Id", "Color", tblSolicitud.EstatusId);
            ViewData["UsuarioId"] = new SelectList(_context.TblUsuario, "Id", "Apellido", tblSolicitud.UsuarioId);
            ViewData["VendedorId"] = new SelectList(_context.TblVendedor, "Id", "Id", tblSolicitud.VendedorId);
            return View(tblSolicitud);
        }

        // GET: TblSolicituds/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblSolicitud = await _context.TblSolicitud
                .Include(t => t.Cliente)
                .Include(t => t.Destino)
                .Include(t => t.Estatus)
                .Include(t => t.Usuario)
                .Include(t => t.Vendedor)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblSolicitud == null)
            {
                return NotFound();
            }

            return View(tblSolicitud);
        }

        // POST: TblSolicituds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblSolicitud = await _context.TblSolicitud.FindAsync(id);
            _context.TblSolicitud.Remove(tblSolicitud);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblSolicitudExists(int id)
        {
            return _context.TblSolicitud.Any(e => e.Id == id);
        }
    } 

    */
}
