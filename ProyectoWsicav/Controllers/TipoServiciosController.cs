﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class TipoServiciosController : Controller
    {
        UsersContext ctx;

        public TipoServiciosController(UsersContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<IActionResult> Index()
        {
            List<TblTipoServicio > TipoServicios = await ctx.TblTipoServicio.Include("TipoServicioEstado").ToListAsync();
            TblTipoServicio TipoServi = new TblTipoServicio();
            ViewBag.TipoServicio = TipoServicios;

            //ViewBag.TblTipoServicioEstado = await ctx.TblTipoServicioEstado.OrderBy(x => x.Estado).ToListAsync();

            return View(TipoServi);
        }

        [BindProperty]
        public TblTipoServicio TipoServicio { get; set; }
        public async Task<IActionResult> GuardarServicio()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var _TipoServicio = await ctx.TblTipoServicio.Where(x => x.Id == TipoServicio.Id).AnyAsync();

            if (!_TipoServicio)
            {
                ctx.TblTipoServicio.Add(TipoServicio);
            }
            else
            {
                ctx.TblTipoServicio.Attach(TipoServicio);
                ctx.Entry(TipoServicio).State = EntityState.Modified;
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        public async Task<IActionResult> Modificar(int id)
        {
            var TipoServicio = await ctx.TblTipoServicio.FindAsync(id);

            if (TipoServicio == null)
            {
                return RedirectToAction("Index");
            }


            //ViewBag.TblTipoServicioEstado = await ctx.TblTipoServicioEstado.OrderBy(x => x.Estado).ToListAsync();

            ViewBag.TipoServicio = await ctx.TblTipoServicio.Include("TipoServicioEstado").ToListAsync();


            return View(TipoServicio);

        }

        public async Task<IActionResult> Eliminar(int id)
        {
            var _TipoServicio = await ctx.TblTipoServicio.FindAsync(id);

            if (_TipoServicio == null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ctx.TblTipoServicio.Remove(_TipoServicio);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        }

    }
}