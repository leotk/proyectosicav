
using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProyectoWsicav.Models;

namespace ProyectoWsicav.Controllers
{
    public class ConductoresController : Controller
    {
        UsersContext ctx;

        public ConductoresController(UsersContext _ctx)
        {
            ctx = _ctx;
        }

        public async Task<IActionResult> Index()
        {
            List<TblConductor> Conductores = await ctx.TblConductor.Include("TipoDocumentoIdentidad").ToListAsync();
            Conductores = await ctx.TblConductor.Include("TipoLicencia").ToListAsync();
            TblConductor Cond = new TblConductor();
            ViewBag.Conductor = Conductores;
            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();
            ViewBag.TblTipoLicencia = await ctx.TblTipoLicencia.OrderBy(x => x.Licencia).ToListAsync();


            return View(Cond);
        }

        [BindProperty]
        public TblConductor Conductor { get; set; }
        public async Task<IActionResult> GuardarConductor()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            var _Conductor = await ctx.TblConductor.Where(x => x.Id == Conductor.Id).AnyAsync();

            if (!_Conductor)
            {
                ctx.TblConductor.Add(Conductor);
            }
            else
            {
                ctx.TblConductor.Attach(Conductor);
                ctx.Entry(Conductor).State = EntityState.Modified;
            }

            await ctx.SaveChangesAsync();
            return RedirectToAction("Index");

        }

        public async Task<IActionResult> Modificar(int id)
        {
            var Conductor = await ctx.TblConductor.FindAsync(id);

            if (Conductor == null)
            {
                return RedirectToAction("Index");
            }


            ViewBag.TblTipoDocumentoIdentidad = await ctx.TblTipoDocumentoIdentidad.OrderBy(x => x.Descripcion).ToListAsync();
            ViewBag.TblTipoLicencia = await ctx.TblTipoLicencia.OrderBy(x => x.Licencia).ToListAsync();


            ViewBag.Conductor = await ctx.TblConductor.Include("TipoDocumentoIdentidad").ToListAsync();


            return View(Conductor);

        }

        public async Task<IActionResult> Eliminar(int id)
        {
            var _Conductor = await ctx.TblConductor.FindAsync(id);

            if (_Conductor == null)
            {
                return RedirectToAction("Index");

            }
            else
            {
                ctx.TblConductor.Remove(_Conductor);
                ctx.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}